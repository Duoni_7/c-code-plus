#define  _CRT_SECURE_NO_WARNINGS 1

//最近公共祖先
//class Solution {
//public:
//    TreeNode* lowestCommonAncestor(TreeNode* root, TreeNode* p, TreeNode* q)
//    {
//        if (!root || root == p || root == q) return root;//若root为空，或找到q\p点，则直接返回root
//        TreeNode* left = lowestCommonAncestor(root->left, p, q);//查找左树
//        TreeNode* right = lowestCommonAncestor(root->right, p, q);//查找右树
//        if (left && right) return root;//若左右树都不为空，则返回root
//        else if (!left && !right) return nullptr;//若左右树都为空，则返回空，因为树中不存在两个节点
//        else return left == nullptr ? right : left;//返回不为空的节点
//    }
//};


//搜索二叉树的最近公共祖先
//TreeNode* find(TreeNode* root, int p, int q)
//{
//    if (!root || root->val == p || root->val == q) return root;//如果找到该点，就返回
//    if (root->val > p && root->val > q)//判断该向左树还是右数查找
//    {
//        return find(root->left, p, q);
//    }
//    if (root->val < p && root->val < q)
//    {
//        return find(root->right, p, q);
//    }
//    return root;//
//}
//
//int lowestCommonAncestor(TreeNode* root, int p, int q)
//{
//    return find(root, p, q)->val;
//}
//};

//合并二叉树
//TreeNode* mergeTrees(TreeNode*& t1, TreeNode*& t2)
//{
//    //以t1作为主树
//    if (!t1 && !t2) return nullptr;//若同时为空，返回空
//    if (t1 && t2) //若同时不为空，相加
//    {
//        t1->val += t2->val;
//    }
//    else if (!t1 && t2)//若t1为空t2不为空，则链接t2节点
//    {
//        return t1 = t2;
//    }
//    else//若t1不为空，t2为空，则不处理返回
//    {
//        return nullptr;
//    }
//    mergeTrees(t1->left, t2->left);
//    mergeTrees(t1->right, t2->right);
//    return t1;
//}
//};