#define  _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
using namespace std;



class Person {
public:
	virtual  Person* BuyTicket(int) { cout << "买票-全价" << endl; return this; }
};

class student :public Person
{
public:
	virtual student* BuyTicket(int) { cout << "买票-半价" << endl; return this;}
};

class Soldier :public Person
{
public:
	virtual Soldier* BuyTicket(int) { cout << "买票-免排队" << endl; return this; }
};

void priant(Person* p)
{
	p->BuyTicket(1);
}

int main()
{
	Person a;
	student b;
	Soldier c;
	priant(&a);
	priant(&b);
	priant(&c);
	return 0;
}

//class a
//{
//public:
//	virtual void print()
//	{
//
//	}
//	virtual void print2()
//	{
//
//	}
//	virtual void print3()
//	{
//
//	}
//	virtual void print4()
//	{
//
//	}
//	int _p;
//};
//
//int main()
//{
//	a o;
//	cout << sizeof(o);
//	return 0;
//}

//动态规划思想
//class Solution {
//public:
//	int Fibonacci(int n)
//	{
//		if (n == 0) return 0;
//		vector<int>out({ 0,1 });
//		while (--n)
//		{
//			out.push_back(out[out.size() - 1] + out[out.size() - 2]);
//		}
//		return out[out.size() - 1];
//	}
//};