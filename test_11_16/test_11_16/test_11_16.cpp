#define  _CRT_SECURE_NO_WARNINGS 1

class Solution {
public:
    vector<vector<int>> generate(int numRows)
    {
        vector<vector<int>>y_Num(numRows, vector<int>(numRows, 1));//创建一个行和列为numRows的二维数组
        for (size_t i = 0; i < numRows; i++)//行
        {
            y_Num[i].resize(i + 1);//对每一列进行裁剪
            for (size_t j = 0; j <= i; j++)//列
            {
                if (j != 0 && j != i)//保持两条边角线值为：1
                {
                    y_Num[i][j] = y_Num[i - 1][j] + y_Num[i - 1][j - 1]; //上行与上行左值相加
                }
            }
        }
        return y_Num;
    }
};


class Solution {
public:
    int removeDuplicates(vector<int>& nums)
    {
        if (nums.size() <= 1)//若长度为0或1，则返回
        {
            return nums.size();
        }
        for (vector<int>::iterator it = nums.begin() + 1; it != nums.end();)//快慢检查
        {
            if (*it == *(it - 1))//若快方与慢方相等，则删除快方
            {
                it = nums.erase(it);//快方迭代器接收删除后的下一个位置，防止迭代器异常
            }
            else
            {
                ++it;//若不相等则快方迭代器++
            }
        }
        return nums.size();
    }
};


class Solution {
public:
    //计数排序
    int MoreThanHalfNum_Solution(vector<int> numbers)
    {
        vector<int>has_(10000);
        for (size_t i = 0; i < numbers.size(); i++)
        {
            has_[numbers[i]]++;
        }
        for (size_t i = 0; i < numbers.size(); i++)
        {
            if (has_[numbers[i]] > numbers.size() / 2)
            {
                return numbers[i];
            }
        }
        return 0;
    }
};

class Solution {
public:
    int singleNumber(vector<int>& nums)
    {
        if (nums.size() < 3)//若长度小于3，则返回首元素
        {
            return nums[0];
        }
        sort(nums.begin(), nums.end());//排序->升序
        for (size_t i = 2; i < nums.size(); i += 2)//排除特殊情况，数组长度最小为4：三个重复元素，一个不重复元素
        {
            //条件给出：三个重复元素，所以每次走三步，再回头检查一次
            if (nums[i - 2] == nums[i])//若是三个重复的数，则进入下一组检查是否重复
            {
                i++;
            }
            else//如果不重复，则返回三个中的第一个元素
            {
                return nums[i - 2];
            }
            if (i == nums.size() - 1 && nums[i] != nums[i - 2])//捕捉不重复元素在数组最后一位出现的情况
            {
                return nums[i];
            }
        }
        return 0;
    }
};

