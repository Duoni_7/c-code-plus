#define  _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<set>
#include<vector>
#include<queue>
using namespace std;

// 最长连续子序
//class Solution {
//public:
//    int maxPower(string s)
//    {
//        int max = INT_MIN;
//        int count = 0;
//        int slow = 0;
//        int fast = 1;
//        while (fast <= s.size())
//        {
//            if (s[slow] != s[fast])
//            {
//                count++;
//                max = max < count ? count : max;
//                count = 0;
//            }
//            else
//            {
//                count++;
//                max = max < count ? count : max;
//            }
//            slow = fast;
//            fast++;
//        }
//        return max;
//    }
//};

//合并有序链表
//class Solution {
//public:
//    ListNode* mergeTwoLists(ListNode* l1, ListNode* l2)
//    {
//        if (!l1 && !l2) return l1;
//        if (!l1 || !l2) return l1 == nullptr ? l2 : l1;
//        ListNode* cur = l1->val <= l2->val ? l1 : l2;
//        ListNode* out = cur;
//        if (cur == l1) l1 = l1->next;
//        if (cur == l2) l2 = l2->next;
//        while (l1 && l2)
//        {
//            if (l1->val <= l2->val)
//            {
//                cur->next = l1;
//                l1 = l1->next;
//                cur = cur->next;
//            }
//            else
//            {
//                cur->next = l2;
//                l2 = l2->next;
//                cur = cur->next;
//            }
//        }
//        if (l1)
//        {
//            cur->next = l1;
//        }
//        if (l2)
//        {
//            cur->next = l2;
//        }
//        return out;
//    }
//};

//链表的相交节点
//class Solution {
//public:
//    ListNode* getIntersectionNode(ListNode* headA, ListNode* headB)
//    {
//        ListNode* cur_1 = headA;
//        ListNode* cur_2 = headB;
//        int count_A = 0;
//        int count_B = 0;
//        while (cur_1)
//        {
//            count_A++;
//            cur_1 = cur_1->next;
//        }
//        while (cur_2)
//        {
//            count_B++;
//            cur_2 = cur_2->next;
//        }
//        int op = 0;
//        if (count_A > count_B)
//        {
//            op = count_A - count_B;
//            while (op--)
//            {
//                headA = headA->next;
//            }
//        }
//        if (count_B > count_A)
//        {
//            op = count_B - count_A;
//            while (op--)
//            {
//                headB = headB->next;
//            }
//        }
//        while (headA && headB)
//        {
//            if (headA == headB)
//            {
//                return headA;
//            }
//            headA = headA->next;
//            headB = headB->next;
//        }
//        return nullptr;
//    }
//};


//奇数前置
//class Solution {
//public:
//    vector<int> exchange(vector<int>& nums)
//    {
//        int pos = 0;
//        for (size_t i = 1; i < nums.size(); i++)
//        {
//            if (nums[pos] % 2 == 0 && nums[i] % 2 != 0)
//            {
//                swap(nums[pos], nums[i]);
//                pos++;
//            }
//            if (nums[pos] % 2 != 0)
//            {
//                pos++;
//            }
//        }
//        return nums;
//    }
//};


//数组中的两数之和
//class Solution {
//public:
//    vector<int> twoSum(vector<int>& nums, int target)
//    {
//        vector<int>out;
//        int start = 0;
//        int end = nums.size() - 1;
//        while (start < end)
//        {
//            if (nums[start] + nums[end] < target)
//            {
//                start++;
//            }
//            else if (nums[start] + nums[end] > target)
//            {
//                end--;
//            }
//            else
//            {
//                out.push_back(nums[start]);
//                out.push_back(nums[end]);
//                break;
//            }
//        }
//        return out;
//    }
//};

//int main()
//{
//	priority_queue<int,vector<int>,greater<int>>qu;
//	qu.push(9);
//	qu.push(5);
//	qu.push(3);
//	qu.push(7);
//	qu.push(1);
//	qu.push(4);
//	int cur = qu.size();
//	for (size_t i = 0; i < cur; i++)
//	{
//		cout << qu.top() << " ";
//		qu.pop();
//	}
//	return 0;
//}

int main()
{
	vector<int>ot({ 1,6,0,0,7 ,1});
	set<int,greater<int>>op(ot.begin(),ot.end());
	for (auto e : op)
	{
		cout << e << " ";
	}
	cout << endl;
	op.insert(10);
	for (auto e : op)
	{
		cout << e << " ";
	}
	cout << endl;
	auto r = op.find(1);
	cout << *r;
	cout << endl;
	cout << op.count(0);
	cout << endl;
	cout << op.size();
	return 0;
}