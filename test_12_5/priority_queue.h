#pragma once
#include<iostream>
#include<vector>
#include<list>
#include<stack>
#include<deque>
#include <queue>
#include<functional>
using namespace std;

void test_priority_queue()
{
	priority_queue<int,vector<int>,greater<int>>qu;//默认插入时按大堆排序
	qu.push(1);
	qu.push(2);
	qu.push(3);
	qu.push(4);
	qu.push(5);
	qu.push(6); 
	qu.push(7);
	qu.push(8);
	qu.push(9);
	while (!qu.empty())
	{
		cout << qu.top() << " ";
		qu.pop();
	}
}