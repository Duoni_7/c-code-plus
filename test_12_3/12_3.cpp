#define  _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include<stack>
#include<string>
#include<vector>
using namespace std;


//逆波兰求值
//int evalRPN(vector<string>& tokens)
//{
//    stack<int>st;//存放数据
//    for (size_t i = 0; i < tokens.size(); i++)
//    {
//        if (tokens[i] != "+" && tokens[i] != "-"
//            && tokens[i] != "*" && tokens[i] != "/")
//        {
//            st.push(stoi(tokens[i]));//若数据为0-9则入栈
//        }
//        else
//        {
//            //若数据为“+、-、*、/”之一，就出栈两个数据
//            int val_2 = st.top(); st.pop();
//            int val_1 = st.top(); st.pop();
//            if (tokens[i] == "+")
//            {
//                st.push(val_1 + val_2);//再将计算好的数据重新入栈
//            }
//            else if (tokens[i] == "-")
//            {
//                st.push(val_1 - val_2);
//            }
//            else if (tokens[i] == "*")
//            {
//                st.push(val_1 * val_2);
//            }
//            else
//            {
//                st.push(val_1 / val_2);
//            }
//        }
//    }
//    return st.top();//栈顶元素就是解
//}

//栈的插入与弹出
//class Solution {
//public:
//    bool IsPopOrder(vector<int> pushV, vector<int> popV)
//    {
//        stack<int>st;
//        int cur = 0;
//        for (size_t i = 0; i < popV.size(); i++)
//        {
//            //当栈不为空时，只有两种可能停止循环：
//            //1、数据已经取到尾
//            //2、取到的数据与栈顶相等
//            while (cur < popV.size() && (st.empty() || st.top() != popV[i]))
//            {
//                st.push(pushV[cur++]);
//            }
//            if (st.top() == popV[i])//捕捉情景2
//            {
//                st.pop();
//            }
//            else//捕捉情景1
//            {
//                return false;
//            }
//        }
//        return true;//若数据遍历到尾，且没有遇到情景1并栈顶数据与popV[i]数据不同的情况下正确
//    }
//};

//最小栈
//class MinStack {
//public:
//    MinStack() {
//        minst.push(INT_MAX);//最小栈中填入一个较大值作对对比值
//    }
//    void push(int val) {
//        st.push(val);//正常栈持续插入
//        if (val <= minst.top())//最小栈只有当数据小于最小栈顶值时进行插入
//        {
//            minst.push(val);
//        }
//    }
//    void pop()
//    {
//        if (st.top() == minst.top())//先对比二者栈顶元素是否相同，若相同，最小栈必须及时更新，防止栈内数据重复
//        {
//            minst.pop();
//        }
//        st.pop();//一定要先对比后，再删除正常栈栈顶
//    }
//    int top() {
//        return st.top();//返回正常栈栈顶
//    }
//    int getMin() {
//        return minst.top();//返回最小栈栈顶
//    }
//private:
//    stack<int>st;
//    stack<int>minst;
//};

//中缀表达式转化为后缀表达式
//#include <iostream>
//#include<stack>
//#include<string>
//#include<vector>
//using namespace std;
//
//int main()
//{
//    stack<char>st;//符号容器
//    string put;//用例容器
//    cin >> put;
//    vector<int>letNum(50);//建立优先级关系
//    letNum['*'] = 1;
//    letNum['/'] = 1;
//    letNum['+'] = 2;
//    letNum['-'] = 2;
//    for (size_t i = 0; i < put.size(); i++)
//    {
//        //若是操作数直接输出
//        if (put[i] != '+' && put[i] != '-'
//            && put[i] != '*' && put[i] != '/')
//        {
//            cout << put[i];
//        }
//        //若是符号进行处理
//        else
//        {
//            //保证栈中不为空，至少有一个符号
//            if (st.empty())
//            {
//                st.push(put[i]);
//                continue;
//            }
//            //分两种情况处理
//            //1、若栈顶符号优先级低于用例符号，则用例符号入栈
//            //2、若栈顶符号优先级高于用例符号，则出栈顶元素，继续对比栈顶符号
//            //直到栈顶符号低于用例符号才停止出栈，再将用例符号入栈
//            while (!st.empty() && letNum[put[i]] >= letNum[st.top()])
//            {
//                cout << st.top();
//                st.pop();
//            }
//            st.push(put[i]);
//        }
//    }
//    //若栈内还有符号，则出栈
//    while (!st.empty())
//    {
//        cout << st.top();
//        st.pop();
//    }
//    return 0;
//}


//牛牛与后缀表达式
//class Solution {
//public:
//    long long legalExp(string str)
//    {
//        int pos = 0;
//        stack<long long>st;
//        str += '#';//尾部加上‘#’作为结束符号
//        for (size_t i = 0; i < str.size(); i++)
//        {
//            if (i + 1 < str.size() && (str[i] == '+' || str[i] == '-' || str[i] == '*'))
//            {
//                if (str[i + 1] != '#')//有的用例比较恶心，把符号和操作数粘起来没有结束符
//                {
//                    str.insert(str.begin() + i + 1, '#');
//                }
//            }
//            if (str[i] == '#')
//            {
//                string cur = str.substr(pos, i - pos);
//                pos = i + 1;
//                if (cur == "+" || cur == "-" || cur == "*")
//                {
//                    long long right = st.top(); st.pop();
//                    long long left = st.top(); st.pop();
//                    if (cur == "+")
//                    {
//                        st.push(left + right);
//                    }
//                    if (cur == "-")
//                    {
//                        st.push(left - right);
//                    }
//                    if (cur == "*")
//                    {
//                        st.push(left * right);
//                    }
//                }
//                else
//                {
//                    st.push(stoll(cur));
//                }
//            }
//        }
//        return st.top();
//    }
//};



//滑动窗口的最大值
//class Solution {
//public:
//    vector<int> maxInWindows(const vector<int>& num, unsigned int size)
//    {
//        //若滑动值大于数组大小或滑动大小等于0，则返回空数组
//        if (num.size() < size || size == 0) return {};
//        vector<int>out(10000);
//        int pos = 0;//pos用于记录实际最大值的数量
//        for (size_t i = 0; i < num.size(); i++)//遍历
//        {
//            int max_ = INT_MIN;//比较最大值
//            //计算能够对数组进行滑动的次数，这也意味着最后的返回数组实际数量有多少个
//            if (i <= num.size() - size)
//            {
//                int len = size;//每次滑动size次
//                int cur = i;//记录i的下标位置，滑动i+size次
//                while (len--)//进行size次的往后移动
//                {
//                    if (max_ < num[cur])//对比最大值
//                    {
//                        max_ = num[cur];
//                        out[pos] = num[cur];//记录最大值
//                    }
//                    cur++;//向后移动数据
//                }
//                pos++;//每进入一次，pos++；
//            }
//        }
//        out.resize(pos);//调整返回长度
//        return out;
//    }
//};

//包含mini函数的栈
class Solution {
//public:
//    Solution()
//    {
//        minst.push(INT_MAX);
//    }
//    void push(int value)
//    {
//        st.push(value);
//        if (minst.top() >= value)
//        {
//            minst.push(value);
//        }
//    }
//    void pop()
//    {
//        if (minst.top() == st.top())
//        {
//            minst.pop();
//        }
//        st.pop();
//    }
//    int top()
//    {
//        return st.top();
//    }
//    int min()
//    {
//        return minst.top();
//    }
//
//private:
//    stack<int>st;
//    stack<int>minst;
//};