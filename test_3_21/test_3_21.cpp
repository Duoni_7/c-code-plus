#define  _CRT_SECURE_NO_WARNINGS 1


//蘑菇阵
//#include<iostream>
//#include<vector>
//using namespace std;
//
//int main() {
//	int n, m, k;
//	while (cin >> n >> m >> k)
//	{
//		vector<vector<int>> table((n + 1), vector<int>(m + 1));
//		vector<vector<double>> P((n + 1), vector<double>(m + 1));
//		int x, y;
//		for (int i = 0; i < k; i++)
//		{
//			scanf("%d%d", &x, &y);
//			table[x][y] = 1;
//		}
//		P[1][1] = 1;      //起点概率为1
//		for (int i = 1; i <= n; i++)
//		{
//			for (int j = 1; j <= m; j++)
//			{
//				if (!(i == 1 && j == 1))
//				{      //跳过起点
//					P[i][j] = P[i - 1][j] * (j == m ? 1 : 0.5) + P[i][j - 1] * (i == n ? 1 : 0.5);   //边界的时候，概率为1
//					if (table[i][j] == 1) P[i][j] = 0;        //如果该点有蘑菇，概率置为0
//				}
//			}
//		}
//		printf("%.2lf\n", P[n][m]);
//	}
//}

//红与黑
//#include<iostream>
//#include<vector>
//using namespace std;
//
//int count = 0;
//void backticking(vector<vector<char>>& vate, int x, int y)
//{
//    if (x < 0 || x >= vate.size() || y >= vate[0].size() || vate[x][y] == '#')
//    {
//        return;
//    }
//    count++;
//    vate[x][y] = '#';
//    backticking(vate, x - 1, y);
//    backticking(vate, x + 1, y);
//    backticking(vate, x, y - 1);
//    backticking(vate, x, y + 1);
//}
//
//int main()
//{
//    int m, n;
//    while (cin >> m >> n)
//    {
//        int x = 0, y = 0;
//        vector<vector<char>>vate(m, vector<char>(n, '\0'));
//        for (int i = 0; i < m; i++)
//        {
//            for (int j = 0; j < n; j++)
//            {
//                cin >> vate[i][j];
//                if (vate[i][j] == '@')
//                {
//                    x = i;
//                    y = j;
//                }
//            }
//        }
//        backticking(vate, x, y);
//        cout << count << endl;
//        count = 0;
//    }
//    return 0;
//}

//链表
//#include <iostream>
//#include <string>
//using namespace std;
//
//struct List
//{
//    List(int x = -1)
//        :val(x)
//        , next(nullptr)
//    {}
//    int val;
//    struct List* next;
//};
//
//List* head = new List();
//List* ret = head;
//
//int main()
//{
//    int play_num = 0;
//    cin >> play_num;
//    while (play_num--)
//    {
//        string str;
//        cin >> str;
//        if (str == "insert")
//        {
//            int x, y;
//            cin >> x >> y;
//            List* cur = head;
//            while (cur->next && cur->next->val != x)
//            {
//                cur = cur->next;
//            }
//            if (!cur)
//            {
//                head->next = new List(y);
//                continue;
//            }
//            List* node = new List(y);
//            node->next = cur->next;
//            cur->next = node;
//        }
//        if (str == "delete")
//        {
//            int del;
//            cin >> del;
//            if (ret->next)
//            {
//                List* cur = head;
//                while (cur->next->val != del)
//                {
//                    cur = cur->next;
//                }
//                cur->next = cur->next->next;
//            }
//        }
//    }
//    List* op = ret->next;
//    if (!op)
//    {
//        cout << "NULL" << endl;
//    }
//    while (op)
//    {
//        cout << op->val << " ";
//        op = op->next;
//    }
//    return 0;
//}

//#include <iostream>
//#include<vector>
//using namespace std;
//
//
//
//void backticking(vector<vector<char>>& vate, int sx, int sy, int ex, int ey,int count)
//{
//    if (sx < 0 || sx >= vate.size() || sy >= vate[0].size() || sx < 0 || vate[sx][sy] == '*' || (sx == ex && sy == ey))
//    {
//        return;
//    }
//    count++;
//    backticking(vate, sx - 1, sy, ex, ey,count);
//    backticking(vate, sx + 1, sy, ex, ey,count);
//    backticking(vate, sx, sy - 1, ex, ey,count);
//    backticking(vate, sx, sy + 1, ex, ey,count);
//}
//
//
//int main()
//{
//    int count = 0;
//    int n, m;
//    int sx, sy, ex, ey;
//    cin >> n >> m;
//    cin >> sx >> sy >> ex >> ey;
//    vector<vector<char>>vate(n, vector<char>(m, '\0'));
//    for (int i = 0; i < m; i++)
//    {
//        for (int j = 0; j < n; j++)
//        {
//            cin >> vate[i][j];
//        }
//    }
//    backticking(vate, sx - 1, sy - 1, ex - 1, ey - 1,count);
//    cout << count << endl;
//    return 0;
//}


//#include <iostream>
//using namespace std;
//
//int main()
//{
//    int dp[100000] = { 1 };
//    int vate[100000];
//    int num = 0;
//    int ret = 0;
//    cin >> num;
//    for (int i = 1; i <= num; i++)
//    {
//        cin >> vate[i];
//    }
//    for (int i = 1; i <= num; i++)
//    {
//        dp[i] = max(dp[i - 1] * vate[i], vate[i]);
//        ret = dp[i] >= 0 ? (ret + 1) : ret;
//    }
//    cout << ret << endl;
//    return 0;
//}

//#include <iostream>
//using namespace std;
//
//int main()
//{
//    int dp[100000] = { 1 };
//    int vate[100000];
//    int num = 0;
//    int ret = 0;
//    int ans = 0;
//    cin >> num;
//    for (int i = 1; i <= num; i++)
//    {
//        cin >> vate[i];
//    }
//    for (int i = 1; i <= num; i++)
//    {
//        dp[i] = dp[i - 1] * vate[i];
//        if (dp[i] < 0)
//        {
//            dp[i] = 1;
//        }
//        ret = dp[i] > 0 ? (ret + 1) : ret;
//    }
//    cout << ret << endl;
//    return 0;
//}
