#define  _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
using namespace std;



//对称二叉树
//class Solution {
//public:
//    //中序遍历
//    bool isSameTree(TreeNode* p, TreeNode* q)
//    {
//        if (p == nullptr && q == nullptr)//若两个根节点都为空，则对称
//        {
//            return true;
//        }
//        stack<TreeNode*>p_;//建两个栈用于对比节点的值是否相等，是否对称
//        stack<TreeNode*>q_;
//        while ((p || q) || (!p_.empty() || !q_.empty()))//若有一方节点不为空，或一方栈中不为空则继续
//        {
//            if (p && q)//如果两个节点都不为空
//            {
//                p_.push(p);//若双方值相等，则都入栈
//                q_.push(q);
//                p = p->left;//向左节点走
//                q = q->left;
//            }
//            else if (!q && !p)//若双方都为空
//            {
//                p = p_.top();
//                q = q_.top();
//                p_.pop();
//                q_.pop();//则都出栈
//                if (p->val != q->val)//若双方节点的值不同则返回错误
//                {
//                    return false;
//                }
//                p = p->right;//若相等则继续向右节点走
//                q = q->right;
//            }
//            else//若一方为空，一方不为空，则返回错误
//            {
//                return false;
//            }
//        }
//        return true;//正常走完，则对称
//    }
//};

//层序遍历
//class Solution {
//public:
//    vector<vector<int>> levelOrder(TreeNode* root)
//    {
//        if (!root) return {};//若根节点为空，返回空
//        vector<vector<int>>out;//建立二维数组
//        queue<TreeNode*>q;//队列，用于分层
//        q.push(root);//插入头节点
//        while (!q.empty())//当队列为空时停止循环
//        {
//            vector<int> cur;//保存层节点数据
//            int size = q.size();//记录单层数据个数
//            while (size--)
//            {
//                TreeNode* node = q.front();
//                q.pop();
//                cur.push_back(node->val);//保存数据
//                if (node->left) q.push(node->left);//保存节点左右孩子
//                if (node->right) q.push(node->right);
//            }
//            out.push_back(cur);//插入单层的数据
//        }
//        return out;
//    }
//};


//二叉树的右视图
//class Solution {
//public:
//    vector<int> rightSideView(TreeNode* root)
//    {
//        if (!root)//如果根节点为空，则返回空
//        {
//            return {};
//        }
//        vector<int>out;
//        queue<TreeNode*>q;
//        q.push(root);//若不为空，根节点入队列
//        while (!q.empty())//若队列为空停止
//        {
//            int size = q.size();
//            out.push_back(q.front()->val);//取右节点值
//            while (size--)
//            {
//                TreeNode* node = q.front();
//                q.pop();
//                if (node->right)//入队列顺序：层序遍历逆置：右-》左
//                {
//                    q.push(node->right);
//                }
//                if (node->left)
//                {
//                    q.push(node->left);
//                }
//            }
//        }
//        return out;
//    }
//};


//二叉树的层平均值
//class Solution {
//public:
//    vector<double> averageOfLevels(TreeNode* root)
//    {
//        if (!root)//若根节点为空，返回空
//        {
//            return {};
//        }
//        vector<double>out;
//        queue<TreeNode*>q;
//        q.push(root);//若不为空，则入队
//        while (!q.empty())//当队列为空时停止
//        {
//            double cur = 0;//总数
//            int count = 0;//计数
//            int size = q.size();//队列数据数
//            while (size--)
//            {
//                ++count;//计数
//                TreeNode* node = q.front();
//                q.pop();
//                cur += node->val;//累加
//                if (node->left)//入队
//                {
//                    q.push(node->left);
//                }
//                if (node->right)
//                {
//                    q.push(node->right);
//                }
//            }
//            out.push_back(cur / count);//总和/计数
//        }
//        return out;
//    }
//};


//N叉树的层序遍历
//class Solution {
//public:
//    vector<vector<int>> levelOrder(Node* root)
//    {
//        if (!root) return {};
//        vector<vector<int>>out;
//        queue<Node*>q;
//        q.push(root);
//        while (!q.empty())
//        {
//            vector<int>cur;
//            int size = q.size();
//            while (size--)
//            {
//                Node* node = q.front();
//                q.pop();
//                cur.push_back(node->val);
//                //注意这里将左右孩子封装成了一个数组，必须将每个节点下的孩子节点数组都入队
//                for (int i = 0; i < node->children.size(); i++)
//                {
//                    if (node->children[i])
//                    {
//                        q.push(node->children[i]);
//                    }
//                }
//            }
//            out.push_back(cur);
//        }
//        return out;
//    }
//};

//int main()
//{
//	cout << INT_MIN;
//	return 0;
//}

//在树行中寻找最大值
//class Solution {
//public:
//    vector<int> largestValues(TreeNode* root)
//    {
//        if (!root) return {};
//        vector<int>out;
//        queue<TreeNode*>q;
//        q.push(root);
//        while (!q.empty())
//        {
//            int max = INT_MIN;//设置最小值
//            int size = q.size();
//            while (size--)
//            {
//                TreeNode* node = q.front();
//                q.pop();
//                if (node->val > max)//若节点值大于最小值，则赋值
//                {
//                    max = node->val;
//                }
//                if (node->left)
//                {
//                    q.push(node->left);
//                }
//                if (node->right)
//                {
//                    q.push(node->right);
//                }
//            }
//            out.push_back(max);
//        }
//        return out;
//    }
//};