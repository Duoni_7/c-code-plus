#define  _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<queue>
using namespace std;

//冒泡排序
//void bubbl_sort(int* vate, int size)
//{
//	for (size_t i = 0; i < size; i++)//总共要比较size次
//	{
//		for (size_t j = 0; j < size - i-1; j++)//总共比较size-1次
//		{
//			if (vate[j] > vate[j+1])
//			{
//				int tep = vate[j];
//				vate[j] = vate[j+1];
//				vate[j+1] = tep;
//			}
//		}
//	}
//}


//插入排序：到达指定条件点时，向后排序
//void insert_sort(int* vate, int size)
//{
//	int date = 0;
//	int cur = 0;
//	for (size_t i = 1; i < size; i++)
//	{
//		cur = i - 1;
//		date = vate[i];
//		while (cur >= 0 && vate[cur] > date)
//		{
//			vate[cur + 1] = vate[cur];
//			cur--;
//		}
//		vate[cur + 1] = date;
//	}
//}

//选择排序:不断的找最小值插入到升序位
//void choose_sort(int* vate, int size)
//{
//	int cur = 0; 
//	while (cur < size)
//	{
//		int max_v = cur;
//		for (size_t i = 1 + cur; i < size; i++)
//		{
//			if (vate[max_v] > vate[i])
//			{
//				max_v = i;
//			}
//		}
//		int tep = vate[max_v];
//		vate[max_v] = vate[cur];
//		vate[cur] = tep;
//		cur++;
//	}
//}

//希尔排序:加了一个预排序，最后一次是插入排序
//void hill_sort(int* vate, int size)
//{
//	int gap = size;
//	while (gap > 1)
//	{
//		gap = gap / 3 + 1;
//		for (size_t i = 0; i < size - gap; i++)
//		{
//			int cur = i;
//			int date = vate[cur + gap];
//			while (cur >= 0)
//			{
//				if (vate[cur] > date)
//				{
//					vate[cur + gap] = vate[cur];
//					cur -= gap;
//				}
//				else
//				{
//					break;
//				}
//			}
//			vate[cur + gap] = date;
//		}
//	}
//}

 //堆排序
//void heap_swap(int* vate, int left, int right)
//{
//	int tep = vate[left];
//	vate[left] = vate[right];
//	vate[right] = tep;
//}
//
//void heap_sort(int* vate, int size, int parent)//向下调整
//{
//	int child = parent * 2 + 1;
//	while (child < size)
//	{
//		if (child + 1 < size && vate[child + 1] < vate[child])
//		{
//			child++;
//		}
//		if (vate[child] < vate[parent])
//		{
//			heap_swap(vate, child, parent);
//			parent = child;
//			int child = parent * 2 + 1;
//		}
//		else
//		{
//			break;
//		}
//	}
//}
//
//int main()
//{
//	int vate[] = { 9,8,7,6,5,4,3,2,1 };
//	int size = sizeof(vate) / sizeof(vate[0]);
//	//bubbl_sort(vate, sizeof(vate) / sizeof(vate[0]));
//	//insert_sort(vate, sizeof(vate) / sizeof(vate[0]));
//	//choose_sort(vate, sizeof(vate) / sizeof(vate[0]));
//	//hill_sort(vate, sizeof(vate) / sizeof(vate[0]));
//	for (int i = (size - 1 -1) / 2; i >= 0; i--)//从最后一个父亲节点调大堆
//	{
//		heap_sort(vate, size, i);
//	}
//	size--;
//	while (size >= 0)
//	{
//		heap_swap(vate, 0, size);//大元素向后调，size--
//		heap_sort(vate, size, 0);//调堆
//		size--;
//	}
//	for (int i = 0; i < sizeof(vate) / sizeof(vate[0]); i++)//打印
//	{
//		printf("%d ", vate[i]);
//	}
//	return 0;
//}


//基数排序
#define K 7
#define COUNT 10

queue<int>qu[COUNT];

int get_num(int val, int k)
{
	int key = 0;
	while(k >= 0)
	{
		key = val % 10;
		val /= 10;
		k--;
	}
	return key;
}

void push_val(int* vate, int size, int k)
{
	for (int i = 0; i < size; i++)
	{
		int cur = get_num(vate[i], k);
		qu[cur].push(vate[i]);
	}
}

void out_val(int* vate, int size)
{
	int cur = 0;
	for (int i = 0; i < COUNT; i++)
	{
		while (!qu[i].empty())
		{
			vate[cur++] = qu[i].front();
			qu[i].pop();
		}
	}
}

void cardinal_sort(int* vate, int size, int k)
{
	for (int i = 0; i<k; i++)//最高位优先
	{
		push_val(vate,size, i);
		out_val(vate,size);
	}
}

int main()
{
	int vate[] = { 65235,98664,93345,563248,13,996454,256,6666,987546,3999999 };
	int size = sizeof(vate) / sizeof(vate[0]);
	cardinal_sort(vate, size, K);
	for (int i = 0; i < size; i++)
	{
		cout << vate[i] << " ";
	}
	return 0;
}