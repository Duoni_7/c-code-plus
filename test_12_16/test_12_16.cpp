#define  _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
using namespace std;

//class A
//{
//public:
//	virtual void foo_1()
//	{
//		cout << "foo_1" << endl;
//	}
//	virtual void foo()
//	{
//		cout << "foo_3" << endl;
//	}
//	int _a;
//};
//
//class B
//{
//public:
//	virtual void foo_2()
//	{
//		cout << "foo_2" << endl;
//	}
//};
//
//class C
//{
//public:
//	virtual void foo_3()
//	{
//		cout << "foo_3" << endl;
//	}
//}; 
//
//class D
//{
//public:
//	virtual void foo_4()
//	{
//		cout << "foo_4" << endl;
//	}
//};
//
//class E :public A, public B, public C, public D
//{
//	virtual void foo_1()
//	{
//		cout << "foo_3" << endl;
//	}
//	virtual void foo_2()
//	{
//		cout << "foo_3" << endl;
//	}
//	virtual void foo_3()
//	{
//		cout << "foo_3" << endl;
//	}
//	virtual void foo_4()
//	{
//		cout << "foo_3" << endl;
//	}
//	virtual void Z()
//	{
//		cout << "foo_3" << endl;
//	}
//};
//
//int main()
//{
//	E op;
//	return 0;
//}


//class Person {
//public:
//	virtual void BuyTicket() { cout << "��Ʊ-ȫ��" << endl; }
//	int _a;
//};
//class Student : public Person {
//public:
//	virtual void BuyTicket() { cout << "��Ʊ-���" << endl; }
//	int _b;
//};
//void Func(Person& p)
//{
//	p.BuyTicket();
//}
//int main()
//{
//	Person Mike;
//	Func(Mike);
//	Student Johnson;
//	Func(Johnson);
//	return 0;
//}


//class Person 
//{
//public:
//	virtual ~Person(){ cout << "~Person()" << endl; }
//};
//class Student:public Person
//{
//public: 
//	~Student()override { cout << "~Student()" << endl; }
//};
//
//int main()
//{
//	/*Person* pte = new Person;
//	delete pte;*/
//
//	Person* ptr = new Student;
//	delete ptr;
//	return 0; 
//}

//class A
//{
//public:
//	int a=10;
//};
//
//class B:public A
//{
//public:
//	int a=5;
//};
//
//int main()
//{
//	B op;
//	cout << op.A::a;
//	return 0;
//}

class A
{
public:
	virtual void test(int i = 5) = 0;
};

class B :public A
{
public:
	virtual void test(int i = 0) 
	{ 
		cout << i << endl;
		cout << "��д1" << endl;
	}
};

class C :public A
{
public:
	virtual void test(int i = 0)
	{
		cout << i << endl;
		cout << "��д2" << endl;
	}
};

int main()
{
	A* ptr = new B;
	ptr->test();

	ptr = new C;
	ptr->test();
	return 0;
}
