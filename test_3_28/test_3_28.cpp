#define  _CRT_SECURE_NO_WARNINGS 1

//电话号码组合+回溯
//class Solution {
//public:
//    string cur;
//    vector<string>out;
//    vector<string>muau{ {""},{""},{"abc"},{"def"},{"ghi"},{"jkl"},{"mno"},{"pqrs"},{"tuv"},{"wxyz"} };
//    void backtracking(vector<string>& vate, int index, int size)
//    {
//        if (cur.size() == size)//若是块数相等则保存
//        {
//            out.push_back(cur);
//            return;
//        }
//        string op = vate[index];//取每一组号码进行遍历
//        for (int i = 0; i < op.size(); i++)
//        {
//            cur += op[i];
//            backtracking(vate, index + 1, size);//index+1的意思是取下一组的数据进行组合
//            cur.pop_back();
//        }
//    }
//    vector<string> letterCombinations(string digits)
//    {
//        if (digits.size() == 0) return{};
//        vector<string>vate;
//        int dig_size = digits.size();
//        for (int i = 0; i < dig_size; i++)
//        {
//            vate.push_back(muau[digits[i] - '0']);//将对应号码对应的数据进行保存
//        }
//        backtracking(vate, 0, dig_size);
//        return out;
//    }
//};


//集合总和+回溯
//class Solution {
//public:
//    vector<int>path;
//    vector<vector<int>>out;
//    void backtracking(vector<int>& vate, int target, int sum, int index)
//    {
//        if (sum > target)return;//若不限制，会发生数组越界情况
//        if (sum == target)
//        {
//            out.push_back(path);
//            return;
//        }
//        for (int i = index; i < vate.size() && sum + vate[i] <= target; i++)//若和加上本次元素大于目标值，则跳过
//        {
//            path.push_back(vate[i]);
//            backtracking(vate, target, sum + vate[i], i);
//            //为避免相似集合的产生，所以需要将本次的位置用于下一次递归的使用
//            //既保证了可以使用自己，也避免了重头开始所造成的相似集合的产生
//            path.pop_back();
//
//        }
//    }
//    vector<vector<int>> combinationSum(vector<int>& candidates, int target)
//    {
//        sort(candidates.begin(), candidates.end());
//        backtracking(candidates, target, 0, 0);
//        return out;
//    }
//};


//组合总和（去重）
//class Solution {
//public:
//    vector<int>path;
//    vector<vector<int>>out;
//    void backtracking(vector<int>& vate, int target, int sum, int index, vector<bool>& used)
//    {
//        //if(sum>target)return;//若不限制，会发生数组越界情况
//        if (sum == target)
//        {
//            if (find(out.begin(), out.end(), path) == out.end())
//            {
//                out.push_back(path);
//            }
//            return;
//        }
//        //当used[i-1]==true时，意味着不同数据的同数被使用，树枝重合
          //当used[i-1]==false时，意味着同一数值被重复使用，树层重合
//        for (int i = index; i < vate.size() && sum + vate[i] <= target; i++)//若和加上本次元素大于目标值，则跳过
//        {
//            if (i > index && vate[i] == vate[i - 1] && used[i - 1] == false)continue;
//            path.push_back(vate[i]);
//            used[i] = true;
//            backtracking(vate, target, sum + vate[i], i + 1, used);
//            //为避免相似集合的产生，所以需要将本次的位置用于下一次递归的使用
//            //既保证了可以使用自己，也避免了重头开始所造成的相似集合的产生
//            used[i] = false;
//            path.pop_back();
//
//        }
//    }
//    vector<vector<int>> combinationSum2(vector<int>& candidates, int target)
//    {
//        vector<bool>used(candidates.size(), false);
//        sort(candidates.begin(), candidates.end());
//        backtracking(candidates, target, 0, 0, used);
//        return out;
//    }
//};


//切割回文串
//class Solution {
//public:
//    vector<string>cur;
//    vector<vector<string>>out;
//
//    //判断是否是回文串
//    bool ispalin(string& str, int start, int end)
//    {
//        string op;
//        for (int i = start; i <= end; i++)
//        {
//            op += str[i];
//        }
//        string ot(op);
//        reverse(ot.begin(), ot.end());
//        return op == ot;
//    }
//
//    void backtracking(string& str, int startindex)
//    {
//        if (startindex == str.size())
//        {
//            out.push_back(cur);
//            return;
//        }
//        //用startindex作为起点，i作为终点，作为一条切割线
//        for (int i = startindex; i < str.size(); i++)
//        {
//            if (ispalin(str, startindex, i))//判断这个字串是否为回文串
//            {
//                //如果是回文串，则收集
//                cur.push_back(str.substr(startindex, i - startindex + 1));
//            }
//            else//若不是则跳过
//            {
//                continue;
//            }
//            backtracking(str, i + 1);//递归
//            cur.pop_back();//回溯
//        }
//    }
//    vector<vector<string>> partition(string s)
//    {
//        backtracking(s, 0);
//        return out;
//    }
//};