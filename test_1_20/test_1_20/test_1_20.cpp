#define  _CRT_SECURE_NO_WARNINGS 1


//707. 设计链表
//class MyLinkedList {
//public:
//    struct Node
//    {
//        int _val;
//        Node* next;
//        Node(int x)
//            :_val(x)
//            , next(nullptr)
//        {}
//    };
//    MyLinkedList()
//        :size(0)
//    {
//        op = new Node(0);
//    }
//
//    int get(int index)
//    {
//        if (index > (size - 1) || (index < 0))
//        {
//            return -1;
//        }
//        Node* cur = op->next;
//        while (index--)
//        {
//            cur = cur->next;
//        }
//        return cur->_val;
//    }
//
//    void addAtHead(int val)
//    {
//        Node* nenode = new Node(val);
//        nenode->next = op->next;
//        op->next = nenode;
//        size++;
//    }
//
//    void addAtTail(int val)
//    {
//        Node* nenode = new Node(val);
//        Node* cur = op;
//        while (cur->next)
//        {
//            cur = cur->next;
//        }
//        cur->next = nenode;
//        size++;
//    }
//
//    void addAtIndex(int index, int val)
//    {
//        if (index > size)
//        {
//            return;
//        }
//        Node* nenode = new Node(val);
//        Node* cur = op;
//        while (index--)
//        {
//            cur = cur->next;
//        }
//        nenode->next = cur->next;
//        cur->next = nenode;
//        size++;
//    }
//
//    void deleteAtIndex(int index)
//    {
//        if (index >= size || index < 0) return;
//        Node* cur = op;
//        while (index--)
//        {
//            cur = cur->next;
//        }
//        Node* ot = cur->next;
//        cur->next = cur->next->next;
//        size--;
//        delete ot;
//    }
//private:
//    int size;
//    Node* op;
//};


//19. 删除链表的倒数第 N 个结点
//public:
//    ListNode* removeNthFromEnd(ListNode* head, int n)
//    {
//        n += 1;
//        ListNode* op = new ListNode(0);
//        op->next = head;
//        ListNode* slow = op;
//        ListNode* first = op;
//        while (n--)
//        {
//            first = first->next;
//        }
//        while (first)
//        {
//            first = first->next;
//            slow = slow->next;
//        }
//        slow->next = slow->next->next;
//        return op->next;
//    }
//};