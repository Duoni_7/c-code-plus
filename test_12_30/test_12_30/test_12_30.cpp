#define  _CRT_SECURE_NO_WARNINGS 1

//搜索二叉树递归删除
//class Solution {
//public:
//    TreeNode* deleteNode(TreeNode* root, int key)
//    {
//        if (!root) return nullptr;//如果root为空，则说明不存在该删除数据
//        if (root->val == key)//找到删除点
//        {
//            if (!root->left)//若删除点没有左孩子
//            {
//                return root->right;//则返回删除点的右孩子，给上层父节点链接
//            }
//            else if (!root->right)
//            {
//                return root->left;//则返回删除点的左孩子，给上层父节点链接
//            }
//            else//若存在左右孩子，则内部处理
//            {
//                TreeNode* cur = root->right;
//                TreeNode* pre = root;
//                while (cur->left)
//                {
//                    pre = cur;
//                    cur = cur->left;
//                }
//                root->val = cur->val;
//                if (pre->left == cur)
//                {
//                    pre->left = cur->right;
//                }
//                else
//                {
//                    pre->right = cur->right;
//                }
//            }
//        }
//        if (root->val > key)//如果key小于根节点值，根节点向左查找
//        {
//
//            root->left = deleteNode(root->left, key);
//        }
//        else
//        {
//            root->right = deleteNode(root->right, key);
//        }
//        return root;
//    }
//};

//构建二叉搜索树
//class Solution {
//public:
//
//    TreeNode* set_node(TreeNode* root, int key)
//    {
//        if (!root)
//        {
//            return new TreeNode(key);
//        }
//        if (root->val > key)
//        {
//            root->left = set_node(root->left, key);
//        }
//        else
//        {
//            root->right = set_node(root->right, key);
//        }
//        return root;
//    }
//    TreeNode* bstFromPreorder(vector<int>& preorder)
//    {
//        TreeNode* root = new TreeNode(preorder[0]);
//        for (size_t i = 1; i < preorder.size(); i++)
//        {
//            set_node(root, preorder[i]);
//        }
//        return root;
//    }
//};

//返回第k小的数
//class Solution {
//public:
//    void f(TreeNode* root, vector<int>& out)
//    {
//        if (!root) return;
//        f(root->left, out);
//        out.push_back(root->val);
//        f(root->right, out);
//    }
//    int kthSmallest(TreeNode* root, int k)
//    {
//        vector<int>out;
//        f(root, out);
//        return out[k - 1];
//    }
//};