#pragma once
#include<iostream>
#include<vector>
#include<list>
#include<stack>
#include<deque>
using namespace std;

namespace XLJ
{
	template<class T, class Container = deque<T>>
	class queue
	{
	public:

		//插入
		void push(const T& val)
		{
			_con.push_back(val);
		}

		//头删除
		void pop()
		{
			_con.pop_front();
		}

		//返回队头别名
		T& front()
		{
			return _con.front();
		}

		//返回队尾别名
		T& back()
		{
			return _con.back();
		}

		//判空
		bool empty()const
		{
			return _con.empty();
		}

		//返回元素个数
		size_t size()const
		{
			return _con.size();
		}

	private:
		Container _con;
		//vector<T>_con;
	};
}

void test_queue_1()
{
	XLJ::queue<int> st;
	st.push(1);
	st.push(2);
	st.push(3);
	st.push(4);
	while (!st.empty())
	{
		cout << st.front() << " ";
		st.pop();
	}
	cout << endl;
	//cout << st.top() << endl;
}

