#define  _CRT_SECURE_NO_WARNINGS 1
#include<vector>
#include<list>
#include<array>
#include<queue>
#include<iostream>
using namespace std;
//
//template<class t, t i = 'p'>
//class test_1
//{
//public:
//	test_1(t a = 0, t b = i)
//		:_a(a)
//		,_b(b)
//	{}
//	void print() 
//	{
//		cout << _a << "  " << _b << endl;
//	}
//private:
//	t _a;
//	t _b;
//};
//
//int main()
//{
//	test_1<char, 'x'>op;
//	op.print(); 
//	/*test_1<int> *op = new test_1<int>;
//	op->print();*/
//	//test_1<int>* ot = (test_1<int>*)malloc(sizeof(test_1<int>));
//	//ot = new(ot)test_1<int>(6, 9);
//	//ot->print();
//	return 0;
//}


//
//#include <iostream>
//#include<string>
//#include<stack>
//#include<vector>
//#include<algorithm>
//using namespace std;
//
//int main()
//{
//    string letNum;
//    stack<char>st;
//    vector<char>ve;
//    cin >> letNum;
//    for (size_t i = 0; i < letNum.size(); i++)
//    {
//        if (!st.empty() && st.top() == letNum[i])
//        {
//            st.pop();
//        }
//        else
//            st.push(letNum[i]);
//    }
//    if (st.empty())
//    {
//        cout << '0';
//        return 0;
//    }
//    while (!st.empty())
//    {
//        ve.push_back(st.top());
//        st.pop();
//    }
//    reverse(ve.begin(), ve.end());
//    for (auto e : ve)
//    {
//        cout << e;
//    }
//    return 0;
//}


//int main()
//{
//	array<int, 10>op;//类型与空间大小
//	op.fill(9);
//	for (auto e : op)
//	{
//		cout << e << " ";
//	}
//	return 0;
//}


struct Date
{
	Date(int year, int month, int day)
		:_year(year)
		,_month(month)
		,_day(day)
	{}

	bool operator<(const Date& d)const
	{
		if (_year < d._year
			|| (_year == d._year) && (_month < d._month)
			|| (_year == d._year) && (_month == d._month) && (_day < d._day))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	bool operator>(const Date& d)const
	{
		if (_year > d._year
			|| (_year == d._year) && (_month > d._month)
			|| (_year == d._year) && (_month == d._month) && (_day > d._day))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	int _year;
	int _month;
	int _day;
};

template<class T>
bool Get(T left, T right)
{
	return left > right;
}

//特化：对特殊类型特殊处理
template<>
bool Get<Date*>(Date* left, Date* right)
{
	return *left > *right;
}

namespace XLJ
{
	template<class T>
	struct less
	{
		bool operator()(const T& l, const T& r)
		{
			return l<r;
		}
	};

	//类特化
	template<>
	struct less<Date*>
	{
		bool operator()(const Date* l, const Date* r)
		{
			return *l < *r;//将指针进行解引用，对象比较  
		}
	};
}

int main()
{
	cout << Get(1, 2) << endl;

	Date d1(2022, 7, 7 );
	Date d2(2022, 7, 8 );
	cout << Get(d1, d2)<<endl;

	Date* p1 = &d1;
	Date* p2 = &d2;
	cout << Get(p1, p2) << endl;

	XLJ::less<Date>lp;
	cout << lp(d1, d2) << endl;

	XLJ::less<Date*>ls;
	cout << ls(p1, p2)<<endl;
	
	priority_queue<Date,vector<Date>,XLJ::less<Date>>q1;
	priority_queue<Date*, vector<Date*>, XLJ::less<Date*>>q2;

	q2.push(new Date(2000, 3, 6));
	q2.push(new Date(2000, 4, 99));
	q2.push(new Date(2000, 5, 2));
	q2.push(new Date(2000, 6, 555));
	q2.push(new Date(2000, 7, 77));
	return 0;
}