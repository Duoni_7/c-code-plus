#define  _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<string>
#include<assert.h>
using namespace std;
//#include"vector.h"

//	template<class T>
//	class date
//	{
//	public:
//		date(T a, T b)
//			:_a(a)
//			,_b(b)
//		{}
//
//		void print();
//	private:
//		T _a;
//		T _b;
//	};
//
//	template<class T>
//	typename void date<T>::print()//类外定义需要指定类域
//	{
//		cout << _a << " " << _b << endl;
//	}
//
//int main()
//{
//	date<int>op(9, 99);
//	op.print();
//	return 0;
//}

//int main()
//{
//	my_vector::vector<int>a;
//	a.push_back(1);
//	a.push_back(2);
//	a.push_back(3);
//	for (size_t i = 0; i < a.size(); i++)
//	{
//		cout << a[i] << " ";
//	}
//	cout <<endl;
//	return 0;
//}

//class Person//子类（公有）
//{
//public:
//	void print()
//	{
//		cout << "name:" << _name << endl;
//		cout<< "age:" << _age << endl;
//	}
//	string _name = "peter";//名字
//	int _age = 18;//年龄
//protected:
//	string o = "保护";
//}; 
//
//
//
//class Student :public Person//学生
//{
//protected:
//	int _stuid;//学号
//};
//
//class Teacher :public Person//老师
//{
//public:
//	void p()//在类中才可以对保护限定符成员进行访问
//	{
//		cout << o << endl;
//	}
//protected:
//	int _jobid;//工号
//};
//
//int main()
//{
//	Student b;//对于基类的继承，是使用基类还是实例化一份给自己呢？
//	b._name = "肖小月";
//	b._age = 24;
//	b.print();
//
//	Teacher c;
//	c.p();
//	c._name = "肖老师";
//	c._age = 30;
//	c.print();
//
//	Person a;
//	a.print();
//	return 0;
//}

//class Person
//{
//public:
//	string _name = "小李";
//	int _age = 18;
//};
//
//class student :public Person
//{
//public:
//	void print()
//	{
//		cout << _name << endl;
//		cout << _age << endl;
//	}
//protected:
//	int _age = 99; 
//};
//
//int main()
//{
//	student s;
//	s.print();
//	return 0;
//}

template<typename Type>
Type Max(const Type& a, const Type& b)
{
	cout << "This is Max<Type>" << endl;
	return a > b ? a : b;
}

template<>
int Max<int>(const int& a, const int& b)
{
	cout << "This is Max<int>" << endl;
	return a > b ? a : b;
}

template<>
char Max<char>(const char& a, const char& b)
{
	cout << "This is Max<char>" << endl;
	return a > b ? a : b;
}

int Max(const int& a, const int& b)
{
	cout << "This is Max" << endl;
	return a > b ? a : b;
}

int main()
{
	Max(10, 20);
	Max(12.34, 23.45);
	Max('A', 'B');
	Max<int>(20, 30);
	return 0;
}