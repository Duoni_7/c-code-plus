//#pragma once
//#pragma once
//#include<iostream>
//#include<assert.h>
//using namespace std;
//
//
//namespace my_vector
//{
//	template<class t>
//	class vector
//	{
//	public:
//		typedef t* iterator;//迭代器的声明放在公有域
//		typedef const t* const_iterator;
//
//		//const迭代器_供const对象调用
//		const_iterator begin()const
//		{
//			return _start;
//		}
//
//		const_iterator end()const
//		{
//			return _finish;
//		}
//
//		//begin
//		iterator begin()//注意重载不能依据返回参数
//		{
//			return _start;
//		}
//
//		//end
//		iterator end()
//		{
//			return _finish;
//		}
//
//		vector()//构造函数
//			:_start(nullptr)
//			, _finish(nullptr)
//			, _end_of_storage(nullptr)
//		{}
//
//		//现代写法 拷贝构造
//		//设立模板兼容各种类型的迭代器
//		//拷贝构造不允许参数别名发生改变
//		vector(const vector<t>& cp)
//			:_start(nullptr)
//			, _finish(nullptr)
//			, _end_of_storage(nullptr)
//		{
//			vector<t>temp(cp.begin(), cp.end());//利用迭代器区间构造一个对象，
//			swap(temp);//交换二者的指针空间
//		}
//
//		//交换 不是拷贝
//		void swap(vector<t>& cp)
//		{
//			//内置类型交换使用全局库中的//会发生深拷贝
//			//自定义类型使用指定库中的
//			::swap(_start, cp._start);
//			::swap(_finish, cp._finish);
//			::swap(_end_of_storage, cp._end_of_storage);
//		}
//
//		//迭代器构造
//		template<class iterator>
//		vector(iterator fast, iterator end)
//			:_start(nullptr)
//			, _finish(nullptr)
//			, _end_of_storage(nullptr)
//		{
//			while (fast != end)
//			{
//				push_back(*fast);
//				fast++;
//			}
//		}
//
//		//赋值重载（现代写法）
//		vector<t>& operator=(vector<t> cp)//传值传参（引发拷贝构造）  修改形参不会改变实参
//		{
//			//vector<t> temp(cp.begin(), cp.end());
//			swap(cp);
//			return *this;
//		}
//
//
//		~vector()//析构函数
//		{
//			delete[]_start;
//			_start = _finish = _end_of_storage = nullptr;
//		}
//
//		//返回容量
//		size_t capecity()const
//		{
//			return _end_of_storage - _start;//指针间相减，得出的是二者之间的距离
//		}
//
//		//返回有效数位
//		size_t size()const
//		{
//			return _finish - _start;
//		}
//
//		//方括号 
//		t& operator[](size_t pos)const
//		{
//			assert(pos < size());
//			return _start[pos];
//		}
//
//		//扩容
//		void reserve(size_t len)
//		{
//			if (len > capecity())
//			{
//				size_t sz = size();//记录未扩容前finish在start中的位置
//				t* new_start = new t[len];
//				if (_start != nullptr)
//				{
//					memcpy(new_start, _start, sizeof(t) * sz);//按字节拷贝内存
//					delete[] _start;
//				}
//				_start = new_start;//头指针
//				_finish = _start + sz;//有效位指针
//				_end_of_storage = _start + len;//容器容量指针
//			}
//		}
//
//		//pop_back
//		void pop_back()
//		{
//			assert(_finish > _start);
//			_finish--;
//		}
//
//		//insert与erase的迭代器失效问题
//		//insert 返回当前插入点的迭代器
//		//解决insert时发生的pos点失效（迭代器失效问题）
//		//先记录pos到原本_start的距离，等到发生扩容时，将这一段距离与新空间的begin相加，就得到了新空间的pos点 
//		iterator insert(iterator pos, const t& x);
//
//		//尾插
//		void push_back(const t& x);
//
//	private:
//		iterator _start;
//		iterator _finish;
//		iterator _end_of_storage;
//	};
//}
//
//	//尾插
//	template<class t>
//	typename void my_vector::vector<t>::push_back(const t& x)
//	{
//		insert(end(), x);
//	}
//
//	//insert 返回当前插入点的迭代器
//	//解决insert时发生的pos点失效（迭代器失效问题）
//	//先记录pos到原本_start的距离，等到发生扩容时，将这一段距离与新空间的begin相加，就得到了新空间的pos点 
//	template<class t>
//	typename my_vector::vector<t>::iterator my_vector::vector<t>::insert(typename my_vector::vector<t>::iterator pos, const t& x)
//	{
//		assert(pos >= _start && pos <= _finish);//支持尾插 、头插
//		if (_finish == _end_of_storage)
//		{
//			size_t sz = pos - _start;//记录就空间中begin到pos的距离
//			reserve(capecity() == 0 ? 4 : capecity() * 2);
//			pos = _start + sz;//更新在新空间中pos点的位置
//		}
//		iterator cur = this->end() - 1;
//		iterator end = this->end();
//		while (cur >= pos)
//		{
//			*(end--) = *(cur--);//按需把控优先级
//		}
//		*pos = x;
//		_finish++;
//		return pos;
//	}
//
// 