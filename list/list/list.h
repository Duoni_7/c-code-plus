#pragma once
#include<iostream>
#include<string>
#include<algorithm>
#include<vector>
using namespace std;

namespace XLJ
{
	//节点类
	template<class T>
	class list_node
	{
	public:
		list_node(const T& x = T())
			:_date(x)
			,_next(nullptr)
			,_prev(nullptr)
		{}
		T _date;
		list_node<T>* _next;
		list_node<T>* _prev;
	};

	//迭代器类
	template<class T, class Ref, class Ptr>//通过模板实例化来确定返回值
	struct __list_iterator
	{
		//typedef __list_iterator<T,const T&,const T*> const_iterator;//重点！！！
		typedef __list_iterator<T,T&,T*> iterator;

		typedef list_node<T> Node; 
		Node* _node;//只是借用节点，并非开辟，不需要析构

		//使用节点来构造迭代器位置
		__list_iterator(Node* node)//构造
			:_node(node)
		{}

		//!=重载
		bool operator!=(const iterator& it)const
		{
			return _node != it._node;
		}

		//==重载
		bool operator==(const iterator& it)const
		{
			return _node == it._node;
		}

		//*解引用
		Ref operator*()
		{
			return _node->_date;
		}

		//前置++
		iterator& operator++()
		{
			_node = _node->_next;
			return *this;
		}

		//后置++
		iterator& operator++(int)
		{
			iterator cp = _node;
			_node = _node->_next;
			return cp;
		}

		//前置--
		iterator& operator--()
		{
			_node = _node->_prev;
			return *this;
		}

		//后置--解引用
		iterator& operator--(int)
		{
			iterator cp = _node;
			_node = _node->_prev;
			return cp;
		}

		//->重载
		Ptr operator->()
		{
			return &(operator*());//operator*()返回的是一个对象，对对象取地址后在进行返回，然后再由另一个隐藏的->对其进行成员变量访问 
		}

	};

	template<class T>
	class list 
	{
		typedef list_node<T> Node;
	public:

		typedef __list_iterator<T, const T&, const T*> const_iterator;
		typedef __list_iterator<T, T&, T*> iterator;

		//const begin
		const_iterator begin()const
		{
			return const_iterator(_head->_next); //调用迭代器构造匿名对象
		}

		//const end
		const_iterator end()const
		{
			return const_iterator(_head);
		}

		//begin
		iterator begin()
		{
			return iterator(_head->_next); //调用迭代器构造匿名对象
		}


		//end
		iterator end()
		{
			return iterator(_head);
		}

		list()//构造函数(构造头节点）
		{
			_head = new Node;
			_head->_prev = _head;
			_head->_next = _head;
		}

		void push_back(const T& x)//尾插
		{
			Node* end = _head->_prev;//第一步：找尾
			Node* node = new Node(x);//第二步：开辟节点//自定义类型：new一个节点会自动调用构造函数
			end->_next = node;//尾节点指针指向新节点
			node->_prev = end;//新节点的指针指向前方的尾节点
			node->_next = _head;//新节点的尾指针指向头指针
			_head->_prev = node;//头指针的前一位指向新尾节点
		}

	
	//private:
		Node* _head;//自定义类
	};
}


void tp(const XLJ::list<int>& a)
{
	XLJ::list<int>::const_iterator it = a.begin();
	while (it != a.end())
	{
		cout << *it << " ";
		it++;
	}
}
void test_lise_1()
{
	XLJ::list<int>a;
	a.push_back(8);
	a.push_back(7);
	a.push_back(7);
	a.push_back(7);
	a.push_back(7);
	a.push_back(7);
	//XLJ::list<int>* b = &a;
	//cout << b->begin()._node->_date << endl;
	//cout << b->_head->_next->_date<<endl;
	//XLJ::list<int>::const_iterator it = a.begin();
	//while (it != a.end())
	//{
	//	cout << *it << " ";
	//	it++;
	//}
	/*for (auto& e : a)
	{ 
		e *= 2;
		cout << e << " ";
	}*/
	//cout << a.begin()._node->_date;
	tp(a);
}

