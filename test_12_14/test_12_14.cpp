#define  _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
using namespace std;

class Person {
public:
	virtual Person* BuyTicket() { cout << "��Ʊ-ȫ��" << endl; }
};

class Student:public Person
{
public:
	virtual Person* BuyTicket() { cout << "��Ʊ-���" << endl; }
};

class Soldier :public Person
{
public:
	virtual  BuyTicket() { cout << "��Ʊ-���Ŷ�" << endl; }
};

void func(Person& p)
{
	p.BuyTicket();
}

int main()
{
	Person a;
	Student b;
	Soldier c;
	func(a);
	func(b);
	func(c);
	return 0;
}