#define  _CRT_SECURE_NO_WARNINGS 1

//25. K 个一组翻转链表
//class Solution {
//public:
//    ListNode* reverseKGroup(ListNode* head, int k)
//    {
//        int pre = 0;
//        vector<int>out;
//        ListNode* cur = head;
//        while (cur)
//        {
//            out.push_back(cur->val);
//            cur = cur->next;
//        }
//        cur = head;
//        for (size_t i = 0; i < out.size(); i += k)
//        {
//            if (i + k <= out.size())
//            {
//                reverse(out.begin() + i, out.begin() + k + i);
//            }
//        }
//        while (cur)
//        {
//            cur->val = out[pre++];
//            cur = cur->next;
//        }
//        return head;
//    }
//};

//剑指 Offer 18. 删除链表的节点
//class Solution {
//public:
//    ListNode* deleteNode(ListNode* head, int val)
//    {
//        if (head->val == val)
//        {
//            head = head->next;
//            return head;
//        }
//        ListNode* pre = nullptr;
//        ListNode* cur = head;
//        while (cur->val != val)
//        {
//            pre = cur;
//            cur = cur->next;
//        }
//        pre->next = cur->next;
//        return head;
//    }
//};
//
//剑指 Offer 27. 二叉树的镜像
//class Solution {
//public:
//    TreeNode* mirrorTree(TreeNode* root)
//    {
//        if (!root) return nullptr;
//        swap(root->left, root->right);
//        mirrorTree(root->left);
//        mirrorTree(root->right);
//        return root;
//    }
//};
//
//剑指 Offer 26. 树的子结构
//class Solution {
//public:
//    bool isSubtre(TreeNode* root, TreeNode* subRoot)
//    {
//        if (!subRoot) return true;
//        if (!root || !subRoot) return false;
//        if (root->val != subRoot->val) return false;
//        return isSubtre(root->left, subRoot->left) && isSubtre(root->right, subRoot->right);
//    }
//
//    bool isSubStructure(TreeNode* root, TreeNode* subRoot) //对每一颗子树进行对比
//    {
//        if (!root || !subRoot) return false;//root为空代表找不到这一棵子树
//
//        if (isSubtre(root, subRoot)) return true;
//        bool res = isSubStructure(root->left, subRoot);
//        bool ret = isSubStructure(root->right, subRoot);
//
//        return res || ret;//左右子树任意一个匹配就为真
//    }
//};

//236. 二叉树的最近公共祖先
//class Solution {
//public:
//    bool find_node(TreeNode* root, TreeNode* node, stack<TreeNode*>& st)
//    {
//        if (!root) return false;
//        st.push(root);
//        if (root == node) return true;
//        if (find_node(root->left, node, st)) return true;
//        if (find_node(root->right, node, st)) return true;
//        st.pop();
//        return false;
//    }
//    TreeNode* lowestCommonAncestor(TreeNode* root, TreeNode* p, TreeNode* q)
//    {
//        stack<TreeNode*>pst, qst;
//        find_node(root, p, pst);
//        find_node(root, q, qst);
//        int cue = pst.size() > qst.size() ? pst.size() - qst.size() : qst.size() - pst.size();
//        while (cue--)
//        {
//            if (qst.size() > pst.size()) qst.pop();
//            else pst.pop();
//        }
//        while (!pst.empty() && !qst.empty())
//        {
//            if (pst.top() == qst.top())
//            {
//                return qst.top();
//            }
//            pst.pop();
//            qst.pop();
//        }
//        return NULL;
//    }
//};