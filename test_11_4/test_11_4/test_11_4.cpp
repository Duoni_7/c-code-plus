#define  _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
using namespace std;

//int main()
//{
//	try
//	{
//		int* p = (int*)operator new(1024u * 1024u * 1024u * 2 - 1);
//	}
//	catch (const exception& e)
//	{
//		cout << e.what() << endl;
//	}
//	return 0;
//}

//class A
//{
//public:
//	A(int a=2)
//		:_a(a)
//	{}
//
////private:
//	int _a;
//};

//int main()
//{
//	A* p = new A[5]{1};
//	for (int i = 0; i < 5; i++)
//	{
//		cout << p[i]._a;
//	}
//	return 0;
//}

//内存池开辟 类的专属new delete
//struct A
//{
//	int _val;
//	A* p;
//	static allocator<A>alloc;
//
//	struct A(int val=5)
//		:_val(val)
//		,p(nullptr)
//	{}
//
//	void* operator new(size_t size)
//	{
//		cout << "new" << endl;
//		void* p = alloc.allocate(size);
//		return p;
//	}
//
//	void operator delete(void* p)
//	{
//		cout << "delete" << endl;
//		alloc.deallocate((A*)p,1);
//	}
//};
//
//allocator<A> A::alloc;
//
//int main()
//{
//	A* a = new A[5];
//	//delete a;
//	//cout << a->_val;
//	cout<<_CrtDumpMemoryLeaks();
//	return 0;
//}

//int main()
//{
//	int vate[20];
//	int* p = new int[20]{1};
//	for (int i = 0; i < 20; i++)
//	{
//		cout << p[i] << " ";
//	}
//	return 0;
//}


//class A
//{
//public:
//	A(int a)
//		:_a(a)
//	{}
//
//	int _a;
//};
//
//int main()
//{
//	A* p = new A[2]{ 1,2 };
//	cout << p[0]._a << " " << p[1]._a << endl;
//	A* o = (A*)malloc(sizeof(A));
//	new(o)A(0) ;
//	cout << o->_a << endl;
//	return 0;
//}


//void swp(int& a, int& b)
//{
//	int tep = a;
//	a = b; 
//	b = tep;
//}
//template<typename T>
//
////T add(T2& a, T& b)
////{
////	return a + b;
////}
//void swp(T& s1, T& s2)
//{
//	T cp = s1;
//	s1 = s2;
//	s2 = cp;
//}
//
////T Add(const T& a, const T& b)
////{
////	return a + b;
////}
//
////T* func(int a)
////{
////	T* p = new T[a];
////	return p;
////}
//int main()
//{
//	int a = 5;
//	int b = 10;
//	double c = 1.5;
//	double d = 2.5;
//	char o[] = "123";
//	char t[] = "456";
//	swp(a, b);
//	swp(c,d);
//	cout << a << " " << b << endl;
//	//func<int>(10);
//	/*cout << Add<double>(1, 1.1) << endl;*///同类型实参能够顺利实例化模板函数 
//	/*swap(o, t);
//	cout << o << endl;
//	cout << t << endl;*/
//	/*swap(c,d);
//	cout << a << " " << b << endl;*/
//	//cout << add(a, c) << endl;
//	/*swp(a, b);
//	swp(c, d);
//	cout << a << " " << b << endl;
//	cout << c << " " << d << endl;*/
//	return 0;
//}


//typedef int stackType;


template<typename T>
class Stack
{
public:
	Stack(size_t capecity = 4)
		:_Date(nullptr)
		,_top(0)
		,_capecity(0)
	{
		if (capecity>0)
		{
  			_capecity = capecity;
			_top = 0;
			_Date = new T[capecity];
		}
	}

	~Stack()
	{
		delete []_Date;
		_Date = nullptr;
		_capecity = 0;
		_top = 0;
	}

	void push(const T& x)
	{
		if (_top == _capecity)
		{
			_capecity = _capecity == 0 ? 4 : _capecity * 2;
			T* new_Date = new T[_capecity];
			if (_Date != nullptr)
			{
				memcpy(new_Date, _Date, sizeof(T)*_top);
				delete[]_Date;
			}
			_Date = new_Date;
		}
		_Date[_top++] = x;
	}

	void show()
	{
		for (int i = 0; i < _top; i++)
		{
			cout << _Date[i] << " ";
		}
		cout << endl;
	}

private:
	T* _Date;
	int _top;
	int _capecity;
};

int main()
{
	Stack<int>st_1(0);
	st_1.push(1);
	st_1.push(2);
	st_1.push(3);
	st_1.push(4);
	st_1.show();
	//Stack<char>st_2(2);
	return 0;  
}