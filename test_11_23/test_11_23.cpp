#define  _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
using namespace std;


//迭代法：后序遍历
//public:
//    vector<int> postorderTraversal(TreeNode* root)
//    {
//        if (root == nullptr)
//        {
//            return {};
//        }
//        stack<TreeNode*>st;
//        vector<int>out;
//        st.push(root);
//        while (!st.empty())
//        {
//            TreeNode* cur = st.top();
//            st.pop();
//            out.push_back(cur->val);
//            if (cur->left) st.push(cur->left);
//            if (cur->right) st.push(cur->right);
//        }
//        reverse(out.begin(), out.end());
//        return out;
//    }
//};



//迭代法：中序遍历
//class Solution {
//public:
//    vector<int> inorderTraversal(TreeNode* root)
//    {
//        if (root == nullptr)
//        {
//            return {};
//        }
//        stack<TreeNode*>st;
//        vector<int>out;
//        TreeNode* cur = root;
//        while (cur != nullptr || !st.empty())
//        {
//            if (cur != nullptr)
//            {
//                st.push(cur);
//                cur = cur->left;
//            }
//            else
//            {
//                cur = st.top();
//                st.pop();
//                out.push_back(cur->val);
//                cur = cur->right;
//            }
//        }
//        return out;
//    }
//};


//迭代法：前序遍历
//public:
//    vector<int> preorderTraversal(TreeNode* root)
//    {
//        if (root == nullptr)
//        {
//            return {};
//        }
//        stack<TreeNode*>st;
//        vector<int>out;
//        st.push(root);
//        while (!st.empty())
//        {
//            TreeNode* cur = st.top();
//            st.pop();
//            out.push_back(cur->val);
//            if (cur->right) st.push(cur->right);
//            if (cur->left) st.push(cur->left);
//        }
//        return out;
//    }
//};

