#define  _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<string>
#include<vector>>
using namespace std;


//KMP算法
//class Solution {
//public:
//    void getNext(vector<int>& next, string& needle)//构成字串的next表（前缀表）
//    {
//        int j = 0;
//        next[0] = 0;
//        for (int i = 1; i < needle.size(); i++)
//        {
//            while (j > 0 && needle[j] != needle[i])
//            {
//                j = next[j - 1];
//            }
//            if (needle[j] == needle[i])
//            {
//                j++;
//            }
//            next[i] = j;
//        }
//    }
//    int strStr(string haystack, string needle)
//    {
//        if (needle.size() == 0) return -1;
//        int ret = -1;
//        vector<int>next(needle.size());
//        getNext(next, needle);
//        int cur = 0;
//        for (int i = 0; i < haystack.size(); i++)
//        {
//            while (cur > 0 && haystack[i] != needle[cur])//不断判断回退位是否等于当前位
//            {
//                cur = next[cur - 1];//若此时字串位与父串位不同，则字串位返回next[cur-1]位的对应下标处
//            }
//            if (haystack[i] == needle[cur])
//            {
//                cur++;
//            }
//            if (cur == needle.size()) return (i - needle.size()) + 1;
//        }
//        return -1;
//    }
//};


//组合+回溯
//class Solution {
//public:
//    vector<int>cur;
//    vector<vector<int>>out;
//    void backtracking(int num, int start, int block)//参数：数值范围 起点 规定组合的大小
//    {
//        if (cur.size() == block)//当容器内的数值个数等于规定的块大小
//        {
//            out.push_back(cur);//保存数据
//            return;
//        }
//        for (int i = start; i <= num; i++)//数据集合的大小，也是递归的长度的体现
//        {
//            cur.push_back(i);//处理数据
//            backtracking(num, i + 1, block);//递归
//            cur.pop_back();//回溯
//        }
//    }
//    vector<vector<int>> combine(int n, int k)
//    {
//        backtracking(n, 1, k);
//        return out;
//    }
//};


//bool find_vate(string& a, string& b)
//{
//	bool flag = false;
//	int cur = 0;
//	for (int i = 0; i < a.size(); i++)
//	{
//		for (int j = 0; j < b.size(); j++)
//		{
//			if (i + j < a.size() && a[i + j] != b[j])
//			{
//				break;
//			}
//			if (i + j < a.size() && j == b.size() - 1)
//			{
//				return true;
//			}
//		}
//	}
//	return flag;
//}
//
//int main()
//{
//	string a{ "aabaacaabaaf" };
//	string b{ "aac" };
//	bool flag = find_vate(a, b);
//	cout << flag << endl;
//	return 0;
//}