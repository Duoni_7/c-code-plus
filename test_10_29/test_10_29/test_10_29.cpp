#define  _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
using namespace std;
#include<vector>

//int main()
//{
//	vector<int>a;
//	a.push_back(100);
//	a.push_back(300);
//	a.push_back(300);
//	a.push_back(300);
//	a.push_back(300);
//	a.push_back(500);
//	vector<int>::iterator itor;
//	for (itor = a.begin(); itor != a.end(); itor++)
//	{
//		if (*itor == 300)
//		{
//			itor = a.erase(itor);
//		}
//	}
//	for (itor = a.begin(); itor != a.end(); itor++)
//	{
//		cout << *itor << " ";
//	}
//	return 0;
//}

//字符出现的次数
//#include<iostream>
//#include<string>
//#include<vector>
//using namespace std;
//
//int main()
//{
//    string out, let_Num;//字符串与指定字符
//    getline(cin, out);//输入字符串
//    getline(cin, let_Num);//输入字符
//    if (let_Num[0] >= 'a' && let_Num[0] <= 'z')//将小写字母转化为大写
//    {
//        let_Num[0] = let_Num[0] - 32;
//    }
//    vector<int>let_Count(256);//下标映射
//    for (size_t i = 0; i < out.size(); i++)//计数
//    {
//        if (out[i] >= 'a' && out[i] <= 'z')//若出现小写，将它算为大写，并在对印下标计数
//        {
//            let_Count[out[i] - 32]++;
//            continue;
//        }
//        let_Count[out[i]]++;
//    }
//    cout << let_Count[let_Num[0]] << endl;//打印指定字符出现的次数
//    return 0;
//}

//#include<iostream>
//#include<string>
//#include<vector>
//using namespace std;
//
//int main()
//{
//    int col_Num = 0;
//    cin >> col_Num;
//    vector<vector<int>>dict(col_Num, vector<int>(1 + (2 * (col_Num - 1)), 0));
//    for (size_t i = 0; i < col_Num; i++)
//    {
//        for (size_t j = 0; j < dict[0].size(); j++)
//        {
//            cout << dict[i][j] << " ";
//        }
//        cout << endl;
//    }
//
//    return 0;
//}


//#include<iostream>
//#include<string>
//#include<vector>
//using namespace std;
//
//int main()
//{
//    int col_Num = 0;
//    int len_Num = 1;
//    int up = 0, left = 0, right = 0;
//    cin >> col_Num;
//    vector<vector<int>>dict(col_Num, vector<int>(1 + (2 * (col_Num - 1)), 0));
//    dict[0][0] = 1;
//    for (int i = 1; i < col_Num; i++)
//    {
//        len_Num += 2;
//        for (int j = 0; j < len_Num; j++)
//        {
//            if (j - 1 < 0)
//            {
//                right = 0;
//            }
//            else
//            {
//                right = dict[i - 1][j - 1];
//            }
//            if (j - 2 < 0)
//            {
//                left = 0;
//            }
//            else
//            {
//                left = dict[i - 1][j - 2];
//            }
//            dict[i][j] = dict[i - 1][j] + left + right;
//        }
//    }
//    for (size_t i = 0; i < col_Num; i++)
//    {
//        for (size_t j = 0; j < dict[0].size(); j++)
//        {
//            cout << dict[i][j] << " ";
//        }
//        cout << endl;
//    }
//    return 0;
//}

//杨辉三角的变形
#include<iostream>
#include<string>
#include<vector>
using namespace std;

int main()
{
    int col_Num = 0;
    int len_Num = 1;
    int up = 0, left = 0, right = 0;
    cin >> col_Num;
    vector<vector<int>>dict(col_Num, vector<int>(1 + 2 * col_Num, 0));
    dict[0][0] = 1;
    for (int i = 1; i < col_Num; i++)
    {
        len_Num += 2;
        for (int j = 0; j < len_Num; j++)
        {
            if (j - 1 < 0)
            {
                right = 0;
            }
            else
            {
                right = dict[i - 1][j - 1];
            }
            if (j - 2 < 0)
            {
                left = 0;
            }
            else
            {
                left = dict[i - 1][j - 2];
            }
            dict[i][j] = dict[i - 1][j] + left + right;
        }
    }
  /*      for (size_t i = 0; i < col_Num; i++)
    {
        for (size_t j = 0; j < dict[0].size(); j++)
        {
            cout << dict[i][j] << " ";
        }
        cout << endl;
    }*/
    for (size_t i = col_Num - 1; i < col_Num; i++)
    {
        for (size_t j = 0; j < dict[0].size(); j++)
        {
            if (dict[i][j] % 2 == 0)
            {
                cout << j + 1;
                return 0;
            }
        }
    }
    cout << -1;
    return 0;
}