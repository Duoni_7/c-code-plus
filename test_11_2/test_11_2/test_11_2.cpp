#define  _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
using namespace std;

//class A
//{
//public:
//	friend ostream& operator<<(ostream& out, const A& s);
//	friend istream& operator>>(istream& put,A& s);
//	void print()const
//	{
//		cout << s << endl;
//	}
//	A* operator&()
//	{
//		return this;
//	}
//	const A* operator&()const
//	{
//		return this;
//	}
//private:
//	int s = 3;
//};
//
//inline istream& operator>>(istream& put, A& p)
//{
//	put >> p.s;
//	return put;
//}
//
//inline ostream& operator<<(ostream& out, const A& p)
//{
//	out << p.s << endl;
//	return out;
//}
//
//int main()
//{
//	const A cp;
//	A y;
//	cout << (&cp)<<endl;
//	cout << (&y) << endl;
//	//cp.print();
//	//cin >> cp;
//	//cout << cp;
//	return 0;
//}

//class a
//{
//public:
//	a()
//	{
//		x++;
//		s += x;
//	}
//	void print()const
//	{
//		cout << s << endl;
//	}
//private:
//	static int x;
//	static int s;
//};
//
//int a::s = 0;
//int a::x = 0;
//
//int main()
//{
//	int x = 0;
//	cin >> x;
//	a* o = (new a [x]);
//	o->print();
//	delete []o;
//	return 0;
//}


//class Test
//{
//public:
//	Test(int op, int tp)
//	{
//		_a = op;
//		_b = tp;
//	}
//private:
//	int _a;
//	int _b;
//};
//
//class A
//{
//public:
//	A(int op = 1)
//	:_op(op)
//	,l(op,op)
//	,y(op)
//	,i(op)
//	{}
//private:
//	int _op;
//	int& y;
//	const int i;
//	Test l;
//};
//
//int main()
//{
//	A c;
//	return 0;
//}


//class a
//{
//public:
//	a(int u = 0)
//		:_a(u)
//	{
//	}
//	~a()
//	{
//		cout << "析构" << endl;
//	}
//	static int print()
//	{
//		cout << _b << endl;
//		return _b++;
//	}
//	int getint()
//	{
//		return _b ;
//	}
//private:
//	int _a;
//	static int _b;
//};
//
//int a::_b = 0;
//
//int main()
//{
//	//a p(10);
//	// a b = 10;
//	//a b = 10;
//	/*a d(10); 
//	a(10);*/
//	//a().print(); 
//	a::print();
//	a d(10);
//	d.print();
//	//a::_b;
//	//a::getint();
//	a().getint();
//	return 0;
//}


//class A
//{
//public:
//	friend void show(A& a);
//private:
//	int _a = 1;
//};
//
//class B
//{
//public:
//	friend void show(B& a);
//private:
//	int _a = 2;
//};
//
//inline void show(A& a)
//{
//	cout << "hello world" << a._a <<endl;
//}
//
//inline void show(B& a)
//{
//	cout << "hello world" << a._a<< endl;
//}
//
//int main()
//{
//	A a;
//	B b;
//	show(a);
//	show(b);
//	return 0;
//}



//class A
//{
//	friend class B;//声明即可，不能将类定义其中
//private:
//	int _a = 5;
//};
//
//class B
//{
//public:
//	void show(A& a)
//	{
//		cout << a._a << endl;
//	}
//};
//
//int main()
//{
//	B b;
//	A a;
//	b.show(a);
//	return 0;
//}


class A
{
public:
	class B
	{
	public:
		void show(const A& a)
		{
			cout << a._a << endl;
			cout << _b << endl;
		}
	};
	void pr()
	{
		B a[4];
	}
public: 
	int _a = 6;
	static int _b;
};

int A::_b = 8;

int main()
{
	A a;
	A::B b;
	b.show(a);
	return 0;
}