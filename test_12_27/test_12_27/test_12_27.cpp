#define  _CRT_SECURE_NO_WARNINGS 1

//剑指 Offer II 047. 二叉树剪枝
//class Solution {
//public:
//    TreeNode* pruneTree(TreeNode* root)
//    {
//        if (!root) return nullptr;
//        pruneTree(root->left);
//        pruneTree(root->right);
//        if (root->left && root->left->val == 0 && !root->left->left && !root->left->right)
//        {
//            root->left = nullptr;
//        }
//        if (root->right && root->right->val == 0 && !root->right->left && !root->right->right)
//        {
//            root->right = nullptr;
//        }
//        if (root->val == 0 && !root->left && !root->right) return nullptr;
//        return root;
//    }
//};


//剑指 Offer 28. 对称的二叉树
//class Solution {
//public:
//    bool judge(TreeNode* l, TreeNode* r)
//    {
//        if (!l && !r) return true;
//        if (!l && r) return false;
//        if (l && !r) return false;
//        if (l->val != r->val) return false;
//        return judge(l->left, r->right) && judge(l->right, r->left);
//    }
//    bool isSymmetric(TreeNode* root)
//    {
//        if (!root) return true;
//        if (root && !root->left && !root->right) return true;
//        return judge(root->left, root->right);
//    }
//};


//剑指 Offer 55 - I.二叉树的深度
//class Solution {
//public:
//    int maxDepth(TreeNode* root)
//    {
       /* int depth = 0;
        if (!root) return depth;
        queue<TreeNode*>qu;
        qu.push(root);
        while (!qu.empty())
        {
            int size = qu.size();
            depth++;
            while (size--)
            {
                TreeNode* cur = qu.front();
                qu.pop();
                if (cur->left) qu.push(cur->left);
                if (cur->right) qu.push(cur->right);
            }
        }
        return depth;*/
//    }
//};

//剑指 Offer 32 - I.从上到下打印二叉树
//class Solution {
//public:
//    vector<int> levelOrder(TreeNode* root)
//    {
//        if (!root) return {};
//        vector<int>out;
//        queue<TreeNode*>qu;
//        qu.push(root);
//        while (!qu.empty())
//        {
//            int size = qu.size();
//            while (size--)
//            {
//                TreeNode* cur = qu.front();
//                qu.pop();
//                out.push_back(cur->val);
//                if (cur->left) qu.push(cur->left);
//                if (cur->right) qu.push(cur->right);
//            }
//        }
//        return out;
//    }
//};

//单值二叉树
class Solution {
public:
    bool isUnivalTre(TreeNode* root, int val)
    {
        if (!root) return true;
        if (root->val != val) return false;
        return isUnivalTre(root->left, val) && isUnivalTre(root->right, val);
    }
    bool isUnivalTree(TreeNode* root)
    {
        if (!root) return true;
        return isUnivalTre(root, root->val);
    }
};