#pragma once
#include<iostream>
#include<vector>
#include<list>
#include<stack>
#include<deque>
#include <queue>
#include<functional>
using namespace std;

namespace XLJ
{
	template<class T, class Container = vector<T>, class Compare = std::less<T>>//此处的仿函数作用类似于bool（比较)
	class priority_queue
	{
	public:

		priority_queue()
		{}

		//迭代器区间构造
		template<class iterator>
		priority_queue(iterator first, iterator last)
		{
			while (first != last)
			{
				_con.push_back(*first);
				first++;
			}
			for(int i = (_con.size() - 1 - 1) / 2; i >= 0; i--)
			{
				Adjust_down(i);
			}
		}


		//向上调整O(logn)
		void Adjust_up(int child)//注意size_t的无符号问题
		{
			if (_con.size() == 1)
			{
				return;
			}
			Compare com;//仿函数
			int preant = (child - 1) / 2;
			while (child >= 0)
			{
				//if (_con[preant] < _con[child])
				//若 父亲节点元素 < 孩子节点元素 则仿函数返回：1 为真
				if (com(_con[preant] , _con[child]))//对象调用成员函数“operator()”
				{
					std::swap(_con[preant], _con[child]);
					child = preant;
					preant = (child - 1) / 2;
				}
				else
				{
					break;
				}
			}
		}
		//尾插
		void push(const T& x) 
		{
			_con.push_back(x);
			Adjust_up(_con.size() - 1);//堆的插入需要进行尾插后的向上调整,从最后节点元素下标开始调整
		}

		//向下调整O(N)
		void Adjust_down(int preant)
		{
			if (_con.size() == 1)
			{
				return;
			}
			Compare com;
			int child = preant * 2 + 1;//把在移动的第一方设为边界条件，防止越界
			while (child < _con.size())//边界
			{
				//if (child + 1 < _con.size() && _con[child + 1] > _con[child])//判断左右孩子谁大
				if (child + 1 < _con.size() && com(_con[child] , _con[child + 1]))//判断左右孩子谁大
				{ 
					child = child + 1;
				}
				//if (_con[child] > _con[preant])
				if (com(_con[preant] , _con[child]))
				{
					std::swap(_con[preant], _con[child]);
					preant = child;
					child = preant * 2 + 1;
				}
				else
				{
					break;
				}
			}
		}

		//尾删
		void pop()
		{
			std::swap(_con[0], _con[_con.size() - 1]);//方括号返回元素的引用
			_con.pop_back();
			Adjust_down(0);//向下调整
		}

		//取堆顶元素
		const T& front()
		{
			return _con[0];
		}

		//size
		size_t size()const
		{
			return _con.size();
		}

		//capecity
		size_t capecity()const
		{
			return _con.capecity();
		}

		//empty
		bool empty()const
		{
			return _con.empty();
		}

	private:
		Container _con;
	};
}

//仿函数
namespace Functional
{
	template<class T>
	class less
	{
	public:
		bool operator()(const T& l, const T& r)const
		{
			return l < r;
		}
	};

	template<class T>
	class greater
	{
	public:
		bool operator()(const T& l, const T& r)const
		{
			return l > r;
		}
	};
}

void test_priority_queue()
{
	vector<int>ap({ 1,9,7,3,4,6,8,2,5 });
	XLJ::priority_queue<int,vector<int>,Functional::less<int>>op(ap.begin(), ap.end());
	//XLJ::priority_queue<int, vector<int>, greater<int>()>ot(greater<int>());
	/*op.push(1);
	op.push(2);
	op.push(3);
	op.push(4);
	op.push(5);*/
	while (!op.empty())
	{
		cout << op.front() << endl;
		op.pop();
	}
} 
