#pragma once
#include<iostream>
#include<vector>
#include<assert.h>
using namespace std;


namespace my_vector
{
	template<class T>
	class vector
	{
	public:
		typedef T* iterator;//迭代器的声明放在公有域
		typedef const T* const_iterator;

		//const迭代器_供const对象调用
		const_iterator begin()const
		{
			return _start;
		}

		const_iterator end()const
		{
			return _finish;
		}

		//begin
		iterator begin()//注意重载不能依据返回参数
		{
			return _start;
		}

		//end
		iterator end()
		{
			return _finish;
		}

		vector()//构造函数
			:_start(nullptr)
			, _finish(nullptr)
			,_end_of_storage(nullptr)
		{}


		//拷贝构造(传统写法）
		/*vector(const vector<T>& cp)
		{
			_start = new T[cp.size()];
			memcpy(_start, cp._start, sizeof(T) * cp.size());
			_finish = _start + cp.size();
			_end_of_storage = _start + cp.size();
		}*/

		//拷贝构造（现代写法）负用
		//空对象（老对象）
		/*vector(const vector<T>& cp)
			:_start(nullptr)
			,_finish(nullptr)
			,_end_of_storage(nullptr)
		{
			reserve(cp.size());
			for (const auto& e : cp)
			{
				push_back(e);
			}
		}*/

		//现代写法 拷贝构造
		//设立模板兼容各种类型的迭代器
		//拷贝构造不允许参数别名发生改变
		vector(const vector<T>& cp)
			:_start(nullptr)
			, _finish(nullptr)
			, _end_of_storage(nullptr)
		{
			vector<T>temp(cp.begin(), cp.end());//利用迭代器区间构造一个对象，
			swap(temp);//交换二者的指针空间
		}
		 
		//交换 不是拷贝
		void swap(vector<T>& cp)
		{
			//内置类型交换使用全局库中的//会发生深拷贝
			//自定义类型使用指定库中的
			::swap(_start, cp._start);
			::swap(_finish, cp._finish);
			::swap(_end_of_storage, cp._end_of_storage);
		}

		//迭代器构造
		template<class Iterator>
		vector(Iterator fast, Iterator end)
			:_start(nullptr)
			, _finish(nullptr)
			, _end_of_storage(nullptr)
		{
			while (fast != end)
			{
				push_back(*fast);
				fast++;
			}
		}


		 
		//赋值重载 
		//老对象（老对象）注意数据覆盖问题(传统写法）
		/*vector<T>& operator=(const vector<T>& cp)
		{
			T* new_c = new T[sizeof(T) * cp.size()];
			delete[] _start;
			_start = new_c;
			memcpy(_start, cp._start, sizeof(T) * cp.size());
			_finish = _start + cp.size();
			_end_of_storage = _finish;
			return *this;
		}*/

		//赋值重载（现代写法）
		vector<T>& operator=(vector<T> cp)//传值传参（引发拷贝构造）  修改形参不会改变实参
		{
			//vector<T> temp(cp.begin(), cp.end());
			swap(cp);
			return *this;
		}


		~vector()//析构函数
		{
			delete[]_start;
			_start = _finish = _end_of_storage = nullptr;
		}

		//返回容量
		size_t capecity()const
		{
			return _end_of_storage - _start;//指针间相减，得出的是二者之间的距离
		}

		//返回有效数位
		size_t size()const
		{
			return _finish - _start;
		}

		//返回最后一个元素
		T& back()const
		{
			assert(size() > 0);
			return *(_finish-1);
		}

		//返回第一个元素
		T& front()const
		{
			assert(size() > 0);
			return *(_start);
		}


		//尾插
		void push_back(const T& x)
		{
			if (_finish == _end_of_storage)
			{
				reserve(capecity() == 0 ? 4 : 2 * capecity());
			}
			*_finish = x;
			_finish++;
		}

		//方括号 
		T& operator[](size_t pos)const
		{
			assert(pos < size());
			return _start[pos];
		}
		
		//扩容
		void reserve(size_t len)
		{
			if (len > capecity())
			{
				size_t sz = size();//记录未扩容前finish在start中的位置
				T* new_start = new T[len];
				if (_start != nullptr)
				{
					memcpy(new_start, _start, sizeof(T)*sz);//按字节拷贝内存
					delete[] _start;
				}
				_start = new_start;//头指针
				_finish = _start + sz;//有效位指针
				_end_of_storage = _start + len;//容器容量指针
			}
		}


		//resize
		void resize(size_t n, const T& val = T(0))//默认val的缺省值为0
		{
			if (n > capecity())//若n大于此时的总容量，则进行扩容，注意此时的_finish与_end_of_storage已经更新
			{
				reserve(n);
			}
			if (n > size())//若n大于有效数据，则需要给后面的新空间初始化成val值
			{
				while (_finish != _end_of_storage)
				{
					*_finish = val;
					_finish++;
				}
			}
			else//若n小于有效数据，则需要删除有效数据
			{
				_finish = _start + n;
			}
		}

		//pop_back
		void pop_back()
		{
			assert(_finish > _start);
			_finish--;
		}

		//insert与erase的迭代器失效问题

		//insert 返回当前插入点的迭代器
		//解决insert时发生的pos点失效（迭代器失效问题）
		//先记录pos到原本_start的距离，等到发生扩容时，将这一段距离与新空间的begin相加，就得到了新空间的pos点 
		iterator insert(iterator pos, const T& x)
		{
			assert(pos >= _start && pos <= _finish);//支持尾插 、头插
			if (_finish == _end_of_storage)
			{
				size_t sz = pos - _start;//记录就空间中begin到pos的距离
				reserve(capecity() == 0 ? 4 : capecity() * 2);
				pos = _start + sz;//更新在新空间中pos点的位置
			}
			iterator cur = this->end()-1;
			iterator end = this->end();
			while (cur >= pos)
			{
				*(end--) = *(cur--);//按需把控优先级
			}
			*pos = x;
			_finish++;
			return pos;
		}

		//erase 删除指定迭代器后，返回被删元素的下一个迭代器
		//erase发生的迭代器错误大致情景为：删除某一点后，没有接收erase所返回的被删元素的下一个迭代器，而是自行++，导致发生迭代器错误
		iterator erase(iterator pos)
		{
			assert(pos >= _start && pos < _finish);
			if (pos == this->end() - 1)
			{
				_finish--;
				return _finish;
			}
			iterator cur = pos + 1;
			while (cur != this->end())
			{
				*(cur-1) = *(cur++);
			}
			_finish--;
			return pos; 
		}

	private:
		iterator _start;
		iterator _finish;
		iterator _end_of_storage;
	};
}


namespace XLJ
{
	void test_vector_1()
	{
		my_vector::vector<int> a;
		a.push_back(1);
		a.push_back(2);
		a.push_back(3);
		a.push_back(4);

		auto p = find(a.begin(), a.end(), 3);
		if (p != a.end())
		{ 
			p = a.erase(p);
			cout << *p << endl;//此时的p是失效的，因为已经完成了扩容，原空间被抹掉了
		}
		/*p = a.erase(a.begin());
		cout << *p << endl;*/
		//a.insert(a.begin()+3, 9);
		//cout<<*(a.erase(a.begin()+3))<<endl;
		my_vector::vector<int>::const_iterator it = a.begin();
		while (it != a.end())
		{
			cout << (*it) << " ";
			it++;
		}
		//for (auto it : a)//支持了迭代器，就支持范围for（替换） 
		//{
		//	cout << it << " ";
		//}
		/*for (size_t i = 0; i < a.size(); i++)
		{
			cout << a[i] << " ";
		}*/
	}

	void test_vector_2()
	{
		my_vector::vector<int> a;
		//std::vector<int> a;
		a.push_back(1);
		a.push_back(2);
		a.push_back(2);
		a.push_back(3);
		a.push_back(4);
		//a.push_back(5);

		my_vector::vector<int>::iterator ie = a.begin();
		while (ie != a.end())
		{
			if (*ie % 2 == 0)
			{
				ie = a.erase(ie);//返回被删除数据的下一个迭代器
				continue;
			}
			ie++;
		}


		//my_vector::vector<int>::iterator ie = a.begin();
		//while (ie != a.end())
		//{
		//	if (*ie % 2 == 0)
		//	{
		//		ie = a.insert(ie,(*ie)*2);//若插入成功返回当前插入迭代器
		//		ie += 2;//跳过插入迭代器位置与偶数位置，前往下一个位置
		//		continue;//跳过
		//	}
		//	ie++;//若非偶数，则正常前进  
		//}

		my_vector::vector<int>::const_iterator it = a.begin();
		while (it != a.end())
		{
			cout << (*it) << " ";
			it++;
		}
	}

	void test_vector_3()
	{
		my_vector::vector<int>a;
		a.push_back(2);
		a.push_back(2);
		a.push_back(3);
		a.push_back(4);
		my_vector::vector<int>b;
		//b.resize(10,0);
		//b.reserve(10);
		b.push_back(1);
		b.push_back(1);
		b.push_back(1);
		b.push_back(1);
		b.push_back(1);
		b.resize(20, 6);
		/*cout << b.front() << endl;
		cout << b.back() << endl;*/
		/*b.push_back(4);
		b.push_back(4);
		b = a;

		b[2] = 99;*/
		my_vector::vector<int>::iterator it = b.begin();
		while (it != b.end())
		{
			//(*it)++;
			cout << (*it) << " ";
			it++;
		}
		//cout << endl;
		//cout << b.capecity() << endl;

	}

}

 