#pragma once
#include<iostream>
#include<assert.h>
using namespace std;

namespace XLJ
{
	class String
	{
	public:

		//迭代器
		typedef char* iterator;
		//const迭代器
		typedef const char* const_iterator;

		//beging()
		iterator begin()
		{
			return _str;
		}

		//end()
		iterator end()
		{
			return _str + _size;
		}

		//const beging()
		const_iterator begin()const
		{
			return _str;
		}

		//const end()
		const_iterator end()const
		{
			return _str + _size;
		}

		////构造函数的主要使命是为了初始化对象中的成员变量
		// String(const char* str = "")
		//	 :_str(new char[strlen(str)+1])//string类中尾部存在一个“/0”的标识符
		//	 ,_size(strlen(str))//标识符“/0”不是有效字符
		//	 ,_capecity(strlen(str))
		// {
		//	 if (strlen(str))
		//	 {
		//		 strcpy(_str, str);
		//	 }
		// }

		//构造
		String(const char* str = "")
		{
			_size = strlen(str);
			_capecity = _size;
			_str = new char[_capecity + 1];
			if (_size)
			{
				strcpy(_str, str);
			}
		}

		 //析构函数
		 ~String()
		 {
			 delete[] _str;
			 _str = nullptr;
			 _capecity = 0;
			 _size = 0;
		 }

		 //拷贝构造
		 String(const String& str)
		 {
			 String cp(str._str);//作用：开出空间、调换载体
			 swap(cp);
		 }

		 //赋值
		 String& operator=(const String& str)
		 {
			 String cp(str._str);
			 swap(cp);
			 return *this;
		 }

		 //String类中的swap函数
		 void swap(String& str)
		 {
			 ::swap(_str, str._str);//表示调用全局的swap函数（模板函数）
			 ::swap(_capecity, str._capecity);
			 ::swap(_size, str._size);
		 }

		 //保留空间reserve
		 void reserve(size_t n)
		 {
			 if (n > _capecity)
			 {
				 char* str = new char[n+1];
				 strcpy(str, _str);
				 delete[] _str;
				 _str = str;
				 _capecity = n;
			 }
		 }

		 //尾插
		 void push_back(const char x)
		 {
			 if (_size == _capecity)
			 {
				 reserve(_capecity == 0 ? 4 : 2 * _capecity);
			 }
			 _str[_size++] = x;
			 _str[_size] = '\0';
		 }

		 //+= char
		 String& operator+=(const char x)
		 {
			 push_back(x);
			 return *this;
		 }

		 //+= char*
		 String& operator+=(const char* str)
		 {
			 append(str);
			 return *this;
		 }

		 //append
		 void append(const char* str)
		 {
			 size_t len = strlen(str);
			 if (_size + len > _capecity)
			 {
				 reserve(_size + len);
			 }
			 /*for (size_t i = 0; i < len; i++)
			 {
				 push_back(str[i]);
			 }*/
			 strcpy(_str + _size, str);
			 _size += len;
		 }

		 //insert在某点插入一个字符
		 String& insert(size_t pos, char x)
		 {
			 assert(pos <= _size);
			 if (_size == _capecity)
			 {
				 reserve(_capecity == 0 ? 4 : _capecity * 2);
			 }
			 int end = _size;
			 int fast = _size - 1;
			 while (fast >= pos && fast >= 0)
			 {
				 _str[end--] = _str[fast--];
			 }
			 _str[pos] = x;
			 _size++;
			 _str[_size] = '\0';
			 return *this;
		 }

		 //insert 在指定下标插入字符串
		 String& insert(size_t pos, const char* str)
		 {
			 assert(pos <= _size);
			 size_t len = strlen(str);
			 if (_size + len > _capecity)
			 {
				 reserve(_size + len);
			 }
			 int end = (_size - 1) + len;
			 int fast = _size - 1;
			 while (fast >= pos && fast >= 0)
			 {
				 _str[end--] = _str[fast--];
			 }
			 strncpy(_str + pos, str, len);
			 _size += len;
			 _str[_size] = '\0';
			 return *this;
		 }

		 //erase 在指定下标向后删除n个字符
		 void erase(size_t pos = 0, size_t len = string::npos)
		 {
			 assert(pos < _size);
		 }
		 

		 //c_str将string对象转化成常量字符串
		 const char* c_str()const
		 {
			 return _str;
		 }

		  //返回_size
		 size_t size()const
		 {
			 return _size;
		 }

		 //返回_capecity
		 size_t capecity()const
		 {
			 return _capecity;
		 }

		 //方括号
		 char& operator[](size_t pos)
		 {
			 assert(pos < _size);
			 return _str[pos];
		 }

		 //方括号（常对象调用）
		 const char& operator[](size_t pos)const
		 {
			 assert(pos < _size);
			 return _str[pos];
		 }

		 //打印
		 inline void Print()const
		 {
			 cout << _str << endl;
			 cout << _size << endl;
			 cout << _capecity << endl;
		 }
	private:
		char* _str;
		size_t _size;
		size_t _capecity;
	};
}

void test_String_1()
{
	XLJ::String a("hello");
	for (size_t i = 0; i < a.size(); i++)
	{
		a[i]++;
	}
	for (size_t i = 0; i < a.size(); i++)
	{
		cout << a[i] << " ";
	}
	cout << endl;
	//cout << a[1] << endl;
	cout << a.size() << endl;
	cout << a.capecity() << endl;
	//cout << a.c_str() << endl;
	//a.Print();
}

void test_String_2()
{
	XLJ::String a("hello");
	XLJ::String::iterator op = a.begin();
	while (op != a.end())
	{
		cout << *op << " ";
		op++;
	}
	cout << endl;
}

void test_String_3()
{
	XLJ::String a("hello");
	XLJ::String b("world");
	//a.append(" world!");
	a += " world!";
	a.Print();
	/*a.push_back('p');
	a.push_back('p');
	a.push_back('p');
	a.push_back('p');
	a.push_back('p');
	a.push_back('p');
	a.push_back('p');
	a += '0';*/
	/*string l("11");
	string p("22");
	l.swap(p);*/
	/*a[0] = 'f';
	b[0] = 't';
	a = a;
	a.Print();
	b.Print();*/
}

void test_String_4()
{
	XLJ::String a("hello");
	a.insert(0, "world ");
	a.Print();
}
