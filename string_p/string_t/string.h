#pragma once
#include<iostream>
#include<assert.h>
using namespace std;

namespace XLJ
{
	class String
	{
	public:

		//迭代器
		typedef char* iterator;
		//const迭代器
		typedef const char* const_iterator;

		//beging()
		iterator begin()
		{
			return _str;
		}

		//end()
		iterator end()
		{
			return _str + _size;
		}

		//const beging()
		const_iterator begin()const
		{
			return _str;
		}

		//const end()
		const_iterator end()const
		{
			return _str + _size;
		}

		////构造函数的主要使命是为了初始化对象中的成员变量
		// String(const char* str = "")
		//	 :_str(new char[strlen(str)+1])//string类中尾部存在一个“/0”的标识符
		//	 ,_size(strlen(str))//标识符“/0”不是有效字符
		//	 ,_capecity(strlen(str))
		// {
		//	 if (strlen(str))
		//	 {
		//		 strcpy(_str, str);
		//	 }
		// }

		//构造
		String(const char* str = "")
		{
			_size = strlen(str);
			_capecity = _size;
			_str = new char[_capecity + 1];
			if (_size)
			{
				strcpy(_str, str);
			}
		}

		 //析构函数
		 ~String()
		 {
			 delete[] _str;
			 _str = nullptr;
			 _capecity = 0;
			 _size = 0;
		 }

		 //拷贝构造
		 String(const String& str)
		 {
			 String cp(str._str);//作用：开出空间、调换载体
			 swap(cp);
		 }

		 //赋值
		 String& operator=(const String& str)
		 {
			 String cp(str._str);
			 swap(cp);
			 return *this;
		 }

		 //String类中的swap函数
		 void swap(String& str)
		 {
			 ::swap(_str, str._str);//表示调用全局的swap函数（模板函数）
			 ::swap(_capecity, str._capecity);
			 ::swap(_size, str._size);
		 }

		 //保留空间reserve
		 void reserve(size_t n)
		 {
			 if (n > _capecity)
			 {
				 char* str = new char[n+1];
				 strcpy(str, _str);
				 delete[] _str;
				 _str = str;
				 _capecity = n;
			 }
		 }

		 //控制空间resize
		 void resize(size_t n, char ch = '\0')
		 {
			 if (n > _size)
			 {
				 reserve(n);
				 memset(_str + _size, ch, n-_size);
				 _size = n;
				 _str[_size] = '\0';
			 }
			 else
			 {
				 _str[n] = '\0';
				 _size = n;
			 }
		 }

		 //尾插
		 void push_back(const char x)
		 {
			 if (_size == _capecity)
			 {
				 reserve(_capecity == 0 ? 4 : 2 * _capecity);
			 }
			 _str[_size++] = x;
			 _str[_size] = '\0';
		 }

		 //+= char
		 String& operator+=(const char x)
		 {
			 push_back(x);
			 return *this;
		 }

		 //+= char*
		 String& operator+=(const char* str)
		 {
			 append(str);
			 return *this;
		 }

		 //append
		 void append(const char* str)
		 {
			 size_t len = strlen(str);
			 if (_size + len > _capecity)
			 {
				 reserve(_size + len);
			 }
			 /*for (size_t i = 0; i < len; i++)
			 {
				 push_back(str[i]);
			 }*/
			 strcpy(_str + _size, str);
			 _size += len;
		 }

		 //insert在某点插入一个字符
		 String& insert(size_t pos, char x)
		 {
			 assert(pos <= _size);
			 if (_size == _capecity)
			 {
				 reserve(_capecity == 0 ? 4 : _capecity * 2);
			 }
			 int end = _size;
			 int fast = _size - 1;
			 while (fast >= pos && fast >= 0)
			 {
				 _str[end--] = _str[fast--];
			 }
			 _str[pos] = x;
			 _size++;
			 _str[_size] = '\0';
			 return *this;
		 }

		 //insert 在指定下标插入字符串
		 String& insert(size_t pos, const char* str)
		 {
			 assert(pos <= _size);
			 size_t len = strlen(str);
			 if (_size + len > _capecity)
			 {
				 reserve(_size + len);
			 }
			 int end = (_size - 1) + len;
			 int fast = _size - 1;
			 while (fast >= pos && fast >= 0)
			 {
				 _str[end--] = _str[fast--];
			 }
			 strncpy(_str + pos, str, len);
			 _size += len;
			 _str[_size] = '\0';
			 return *this;
		 }

		 //erase 在指定下标向后删除n个字符
		 void erase(size_t pos = 0, size_t len = string::npos)
		 {
			 assert(pos < _size);
			 if (len > _size - pos)
			 {
				 _str[pos] = '\0';
				 _size = pos;
			 }
			 else
			 {
				 strcpy(_str + pos, _str + pos + len);
				 _size -= len;
			 }
		 }
		 
		 //find 从指定下标查找字符 若找到返回第一个元素下标 失败返回npos
		 size_t find(const char x, size_t pos = 0)
		 {
			 assert(pos < _size);
			 for (size_t i = pos; i < _size; i++)
			 {
				 if (_str[i] == x)
				 {
					 return i;
				 }
			 }
			 return string::npos;
		 }

		 //find 从指定下标查找字符串 若找到返回第一个元素下标 失败返回npos
		 size_t find(const char* str, size_t pos = 0)
		 {
			 assert(pos < _size);
			 const char* p = strstr(_str + pos, str);//若找到包含的子串，则返回该子串首元素的下标
			 if (p == nullptr)
			 {
				 return string::npos;
			 }
			 return p - _str;//指针-指针返的是二者的距离
		 }

		 bool operator>(const String& s)const
		 {
			 return strcmp(_str, s._str) > 0;//大于0为真，小于0为假，等于0相等
		 }
		 bool operator==(const String& s)const
		 {
			 return strcmp(_str, s._str) == 0;
		 }
		 bool operator!=(const String& s)const
		 {
			 return !(*this == s);
		 }
		 bool operator>=(const String& s)const
		 {
			 return *this > s || *this == s;
		 }
		 bool operator<=(const String& s)const
		 {
			 return !(*this > s);
		 }
		 bool operator<(const String& s)const
		 {
			 return strcmp(_str, s._str) < 0;;
		 }

		 //c_str将string对象转化成常量字符串
		 const char* c_str()const
		 {
			 return _str;
		 }

		  //返回_size
		 size_t size()const
		 {
			 return _size;
		 }

		 //返回_capecity
		 size_t capecity()const
		 {
			 return _capecity;
		 }

		 //方括号
		 char& operator[](size_t pos)
		 {
			 assert(pos < _size);
			 return _str[pos];
		 }

		 //方括号（常对象调用）
		 const char& operator[](size_t pos)const
		 {
			 assert(pos < _size);
			 return _str[pos];
		 }

		 //打印
		 inline void Print()const
		 {
			 cout << _str << endl;
			 cout << _size << endl;
			 cout << _capecity << endl;
		 }

		 //数据清空
		 void clear()
		 {
			 _str[0] = '\0';
			 _size = 0;
		 }

		 //substr
		 String substr(size_t pos = 0, size_t len = string::npos)const
		 {
			 assert(pos < _size);
			 String out;
			 if (len > _size - pos)
			 {
				 strcpy(out._str, _str + pos);
			 }
			 else
			 {
				 for (size_t i = pos; i < _size - (_size - pos); i++)
				 {
					 out += _str[i];
				 }
			 }
			 return out;
		 }

		 friend inline istream& operator>>(istream& put, String& s);
		 friend inline ostream& operator<<(ostream& out, const String& s);
	private:
		char* _str;
		size_t _size;
		size_t _capecity;
	};

	//流输入
	inline istream& operator>>(istream& put, String& s)
	{
		s.clear();
		const size_t N = 128;
		char ch[N] = { '\0' };
		for (size_t i = 0; i < N; i++)
		{
			ch[i] = put.get();
			if (ch[i] == ' ' || ch[i] == '\n')
			{
				ch[i] = '\0';//当遇到结束符号时进入，将结束符号替换成\0
				s += ch;//尾插字符串，遇到\0停止
				break;
			}
			if (i == N - 2)//留出最后一位\0
			{
				s += ch;//最后有个\0
				i = -1;
			}
		}
		return put;
	}

	//流输出
	inline ostream& operator<<(ostream& out, const String& s)
	{
		for (size_t i = 0; i < s._size; i++)
		{
			out << s[i];
		}
		return out;
	}
}

void test_String_1()
{
	XLJ::String a("hello");
	for (size_t i = 0; i < a.size(); i++)
	{
		a[i]++;
	}
	for (size_t i = 0; i < a.size(); i++)
	{
		cout << a[i] << " ";
	}
	cout << endl;
	//cout << a[1] << endl;
	cout << a.size() << endl;
	cout << a.capecity() << endl;
	//cout << a.c_str() << endl;
	//a.Print();
}

void test_String_2()
{
	XLJ::String a("hello");
	XLJ::String::iterator op = a.begin();
	while (op != a.end())
	{
		cout << *op << " ";
		op++;
	}
	cout << endl;
}

void test_String_3()
{
	XLJ::String a("hello");
	XLJ::String b("world");
	//a.append(" world!");
	a += " world!";
	a.Print();
	/*a.push_back('p');
	a.push_back('p');
	a.push_back('p');
	a.push_back('p');
	a.push_back('p');
	a.push_back('p');
	a.push_back('p');
	a += '0';*/
	/*string l("11");
	string p("22");
	l.swap(p);*/
	/*a[0] = 'f';
	b[0] = 't';
	a = a;
	a.Print();
	b.Print();*/
}

void test_String_4()
{
	XLJ::String a("hello");
	//cout << a.find("lo");
	//cout<<a.find('o');
	//a.erase(1,2);
	//a.insert(0, "world ");
	//a.Print();
}

void test_String_5()
{
	XLJ::String a("hello");
	XLJ::String b(a);
	a.resize(10,'x');
	a.Print();
	//cout << (a == b) << endl;
	//cout<<a.substr(1);
	//b[0] = 'p';
	//cout<<a.substr(1,1);
	//cout << res;
	//cout<<a.find("ell");
	/*cin >> a >> b;
	a.Print();
	b.Print();*/
	//a += ' ';
	//cout << a << endl;
	//cout << b<<endl;
	//cout << (a > b) << endl;
}

