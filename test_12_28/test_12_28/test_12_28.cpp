#define  _CRT_SECURE_NO_WARNINGS 1

//572. 另一棵树的子树
//class Solution {
//public:
//    bool isSubtre(TreeNode* root, TreeNode* subRoot)
//    {
//        if (!root && !subRoot) return true;
//        if (!root || !subRoot) return false;
//        if (root->val != subRoot->val) return false;
//        return isSubtre(root->left, subRoot->left) && isSubtre(root->right, subRoot->right);
//    }
//
//    bool isSubtree(TreeNode* root, TreeNode* subRoot) //对每一颗子树进行对比
//    {
//        if (!root) return false;//root为空代表找不到这一棵子树
//        if (isSubtre(root, subRoot)) return true;
//        bool res = isSubtree(root->left, subRoot);
//        bool ret = isSubtree(root->right, subRoot);
//        return res || ret;//左右子树任意一个匹配就为真
//    }
//};
//
// 
// 

//二叉树遍历
//#include<iostream>
//#include<string>
//using namespace std;
//
//int i = 0;
//struct Tnode
//{
//    char val;
//    Tnode* left;
//    Tnode* right;
//};
//
//Tnode* set_node(char x)
//{
//    Tnode* root = (Tnode*)malloc(sizeof(Tnode));
//    root->left = nullptr;
//    root->right = nullptr;
//    root->val = x;
//    return root;
//}
//
//Tnode* fi(string str)
//{
//    if (str[i] == '#')
//    {
//        i++;
//        return nullptr;
//    }
//    Tnode* root = set_node(str[i++]);
//    root->left = fi(str);
//    root->right = fi(str);
//    return root;
//}
//
//void z(Tnode* root)
//{
//    if (!root) return;
//    z(root->left);
//    cout << root->val << " ";
//    z(root->right);
//}
//
//int main()
//{
//    string str;
//    cin >> str;
//    Tnode* root = fi(str);
//    z(root);
//    return 0;
//}



//剑指 Offer II 052. 展平二叉搜索树
//class Solution {
//public:
//    void z(TreeNode* root, vector<int>& out)//中序遍历收集数据
//    {
//        if (!root) return;
//        z(root->left, out);
//        out.push_back(root->val);
//        z(root->right, out);
//    }
//
//    TreeNode* set_node(int x)//建立节点
//    {
//        TreeNode* node = new TreeNode(x);
//        return node;
//    }
//
//    TreeNode* tree(vector<int>& out, int* pi)//重组
//    {
//        if (*pi == out.size())
//        {
//            return nullptr;
//        }
//        TreeNode* root = set_node(out[(*pi)++]);
//        root->right = tree(out, pi);
//        return root;
//    }
//
//    TreeNode* increasingBST(TreeNode* root)
//    {
//        vector<int>out;
//        z(root, out);
//        int i = 0;
//        return tree(out, &i);
//    }
//};