#define  _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<vector>
using namespace std;

//int main()
//{
//	ostream& a = cout;
//	a << 1;
//	return 0;
//} 

//int main()
//{
//	vector<int>vate;
//	vate.push_back(1);
//	vate.push_back(1);
//	vate.push_back(1);
//	vate.push_back(1);
//	vate.push_back(1);
//	vate.push_back(1);
//	vate.push_back(1);
//	vate.push_back(1);
//	vate.push_back(1);
//	vate.push_back(1);
//	for (auto a : vate)
//	{
//		cout << a << " ";
//	}
//	cout << endl;
//	cout << vate.capacity() << endl;
//	cout << vate.size() << endl;
//	return 0;
//}


//并不是在类外定义的函数能访问私有的场景，仅仅是定义与声明分离。
//本质上还是在类内定义
//class a
//{
//public:
//	void ap(a& t);
//private:
//	int m = 1;
//};
//
//void a::ap(a& t)
//{
//	cout << t.m;
//}
//
//int main()
//{
//	a o;
//	o.ap(o);
//	return 0;
//}

void show(int a = 5);
void show(int a)
{
	cout << a;
}
int main()
{
	show();
	return 0;
}