#define  _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<vector>
#include<algorithm>
using namespace std;

//合并k个有序链表组成新链表
//class Solution {
//public:
//
//    ListNode* set_Node(int x)//建立结点
//    {
//        ListNode* new_Node = new ListNode(x);
//        new_Node->next = nullptr;
//        return new_Node;
//    }
//
//    ListNode* mergeKLists(vector<ListNode*>& lists)
//    {
//        vector<int>out;//准备好一个存放各个链表结点的容器
//        for (size_t i = 0; i < lists.size(); i++)//遍历拿到每一个结点的数据
//        {
//            ListNode* cur = lists[i];
//            while (cur != nullptr)
//            {
//                out.push_back(cur->val);
//                cur = cur->next;
//            }
//        }
//        sort(out.begin(), out.end());//将各个有序链表的结点进行再排序（此时out容器中的数据只是局部有序，并不是整体有序）
//        ListNode* head = new ListNode(0);//建立一个头节点
//        ListNode* record = head;//保存头节点的位置
//        for (size_t i = 0; i < out.size(); i++)//遍历out容器，将有序数据构造成一个有序链表
//        {
//            head->next = set_Node(out[i]);//新建结点
//            head = head->next;
//        }
//        return record->next;
//    }
//};


//判断密码安全性
//#include <iostream>
//#include<string>
//using namespace std;
//
//bool judge_lower(const string& str)//是否存在小写
//{
//    for (size_t i = 0; i < str.size(); i++)
//    {
//        if (str[i] >= 'a' && str[i] <= 'z')
//        {
//            return true;
//        }
//    }
//    return false;
//}
//
//bool judge_super(const string& str)//是否存在大写
//{
//    for (size_t i = 0; i < str.size(); i++)
//    {
//        if (str[i] >= 'A' && str[i] <= 'Z')
//        {
//            return true;
//        }
//    }
//    return false;
//}
//
//bool judge_num(const string& str)//是否存在数字
//{
//    for (size_t i = 0; i < str.size(); i++)
//    {
//        if (str[i] >= '0' && str[i] <= '9')
//        {
//            return true;
//        }
//    }
//    return false;
//}
//
//bool judge_outher(const string& str)//是否存在其他字符
//{
//    for (size_t i = 0; i < str.size(); i++)
//    {
//        if (str[i] <= 64)
//        {
//            return true;
//        }
//    }
//    return false;
//}
//
//bool judge_strstr(string& str)//判断是否为子集
//{
//    for (size_t i = 0; i < str.size(); i++)
//    {
//        string cur = str.substr(i, 3);//每次截取三个（保证>2）
//        str.erase(i, 3);//删掉原字符串的这三个字符
//        if (cur.size() == 3 && str.find(cur) != string::npos)//若长度为3的部分字符串再源字符串中找到相似，则说明是子串
//        {
//            return false;//错误
//        }
//        str.insert(i, cur);//将截取出的部分字符串再填进去
//    }
//    return true;//正确
//}
//
//int main()
//{
//    string out;
//    while (cin >> out)
//    {
//        if (out.size() >= 9 && judge_strstr(out))//若长度满足并且非子串
//        {
//            if (judge_lower(out) + judge_super(out) + judge_num(out) + judge_outher(out) >= 3)//若至少满足三个条件，则是正确
//            {
//                cout << "OK" << endl;
//                out.clear();
//            }
//        }
//        else//否则错误
//        {
//            cout << "NG" << endl;
//            out.clear();
//        }
//    }
//    return 0;
//}


//int minSubArrayLen(int target, vector<int>& nums)
//{
//    sort(nums.begin(), nums.end());
//    int left = 0, right = nums.size() - 1;
//    while (left <= right)
//    {
//        int mid = (left + right) / 2;
//        if (target == nums[mid])//是否存在相等
//        {
//            return 1;
//        }
//        if (nums[mid] < target)
//        {
//            left = mid + 1;
//        }
//        if (nums[mid] > target)
//        {
//            right = mid - 1;
//        }
//    }
//
//    if (nums[left - 1] > target)//是否target小于它
//    {
//        return 0;
//    }
//
//    int count = 1;//记数组最大值一个
//    int sum = nums[left - 1];//取数组最大值，准备叠加求和
//    nums.resize(left - 1);
//
//    for (int i = nums.size() - 1; i >= 0; i--)
//    {
//        if (sum + nums[i] <= target)
//        {
//            count++;
//            sum += nums[i];
//            if (sum == target)
//            {
//                return count;
//            }
//        }
//        if (sum + nums[i] > target)
//        {
//            count++;
//            sum += nums[i];
//            break;
//        }
//    }
//    if (sum < target)
//    {
//        return 0;
//    }
//    return count;
//}
//
//int main()
//{
//    vector<int>nums({12,28,83,4,25,26,25,2,25,25,25,12});
//    int num = 213;
//    cout<<minSubArrayLen(num, nums);
//    return 0;
//}


//长度最小的子数组
//class Solution {
//public:
//    int minSubArrayLen(int target, vector<int>& nums)
//    {
//        int reslen = INT_MAX;//设一个整形最大值，对比取最短长度
//        int star = 0;//滑动窗口尾部动（前进）
//        int sum = 0;//和
//        int sublen = 0;//局部子数组长度（大于target的那一段长度）
//        for (size_t i = 0; i < nums.size(); i++)//遍历
//        {
//            sum += nums[i];//累加
//            while (sum >= target)//当大于target时
//            {
//                sublen = (i - star) + 1;//窗口前-窗口后，加-转为长度
//                reslen = reslen > sublen ? sublen : reslen;//取最短距离
//                sum -= nums[star++];//尾部窗口前进，使sum<target，继续使窗口前进
//            }
//        }
//        return reslen == INT_MAX ? 0 : reslen;//若reslen没有变化，则说明数组中的和小于target
//    }
//};

//int totalFruit(vector<int>& fruits)
//{
//    if (fruits.size() <= 1)
//    {
//        return fruits.size();
//    }
//    int fruits_Num = 0;
//    int star = 0;
//    int sublen = 0;
//    int count = 1;
//    int cur = 0;
//    int val_1 = fruits[0];
//
//    for (size_t i = 0; i < fruits.size(); i++)
//    {
//        if (val_1 != fruits[i])
//        {
//            count++;
//            if (count == 2)
//            {
//                cur = i;
//            }
//        }
//        if (count == 3 || i == fruits.size() - 1)
//        {
//            sublen = (i - star) + 1;
//            fruits_Num = sublen > fruits_Num ? sublen : fruits_Num;
//            star = cur;
//            val_1 = fruits[star];
//            count = 1;
//        }
//    }
//    return fruits_Num;
//}
//
//int main()
//{
//    vector<int>out({ 0,1,2,2 });
//    cout << totalFruit(out);
//    return 0;
//}


//int main()
//{
//	cout << 1%1000000007 << endl;
//	return 0;
//}


//简单密码
//#include <iostream>
//#include<string>
//using namespace std;
//
//int judge_Let(char x)
//{
//    if (x == 'a' || x == 'b' || x == 'c') { return 2; }
//    if (x == 'd' || x == 'e' || x == 'f') { return 3; }
//    if (x == 'g' || x == 'h' || x == 'i') { return 4; }
//    if (x == 'j' || x == 'k' || x == 'l') { return 5; }
//    if (x == 'm' || x == 'n' || x == 'o') { return 6; }
//    if (x == 'p' || x == 'q' || x == 'r' || x == 's') { return 7; }
//    if (x == 't' || x == 'u' || x == 'v') { return 8; }
//    if (x == 'w' || x == 'x' || x == 'y' || x == 'z') { return 9; }
//    return 0;
//}
//
//int main()
//{
//    string out;
//    cin >> out;
//    for (size_t i = 0; i < out.size(); i++)
//    {
//        if (out[i] >= 'a' && out[i] <= 'z')
//        {
//            out[i] = judge_Let(out[i]) + '0';
//        }
//        else if (out[i] >= 'A' && out[i] <= 'Z')
//        {
//            if (out[i] == 'Z')
//            {
//                out[i] = 'a';
//                continue;
//            }
//            out[i] += 33;
//        }
//    }
//    cout << out << endl;
//    return 0;
//}


////删除字符串中出现次数最少的字符
//#include <iostream>
//#include<vector>
//#include<string>
//using namespace std;
//
//int main()
//{
//    int mini = 1000;
//    string out;
//    cin >> out;
//    vector<int>has_(256);
//    for (size_t i = 0; i < out.size(); i++)
//    {
//        has_[out[i]]++;
//    }
//    for (size_t i = 0; i < out.size(); i++)
//    {
//        if (mini > has_[out[i]] && has_[out[i]] > 0)
//        {
//            mini = has_[out[i]];
//        }
//    }
//    for (size_t i = 0; i < out.size(); i++)
//    {
//        if (has_[out[i]] != mini)
//        {
//            cout << out[i];
//        }
//    }
//    return 0;
//}


#include <iostream>
#include<vector>
using namespace std;

int main()
{
    int target = 0;
    int count = 0;
    vector<int>out(1000, -1);
    for (size_t i = 0; i < 1000; i++)
    {
        cin >> out[i];
    }
    cin >> target;
    int left = 0, right = out.size() - 1;
    while (left <= right)
    {
        int mid = (left + right) / 2;
        if (out[mid] > target)
        {
            right = mid - 1;
        }
        else if (out[mid] < target)
        {
            left = mid + 1;
        }
        else
        {
            count++;
            out[mid] = -1;
            left = mid + 1;
        }
    }
    cout << count;
    return 0;
}




