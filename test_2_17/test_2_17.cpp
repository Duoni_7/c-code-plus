#define  _CRT_SECURE_NO_WARNINGS 1


//四数相加
//class Solution {
//public:
//    int fourSumCount(vector<int>& nums1, vector<int>& nums2, vector<int>& nums3, vector<int>& nums4)
//    {
//        unordered_map<int, int>mp;
//        for (auto& i : nums1)
//        {
//            for (auto& j : nums2)
//            {
//                mp[i + j]++;
//            }
//        }
//        int count = 0;
//        for (auto& i : nums3)
//        {
//            for (auto& j : nums4)
//            {
//                if (mp.find(0 - (i + j)) != mp.end())
//                {
//                    count += mp[0 - (i + j)];
//                }
//            }
//        }
//        return count;
//    }
//};


//逆置单词
//class Solution {
//public:
//    string reverseWords(string s)
//    {
//        int flag = 0;
//        string out;
//        for (auto& e : s)//去括号
//        {
//            if (e != ' ')
//            {
//                out += e;
//                if (!flag)
//                {
//                    flag = 1;
//                }
//            }
//            if (e == ' ' && flag == 1)
//            {
//                out += ' ';
//                flag = 0;
//            }
//        }
//        if (out[out.size() - 1] != ' ')//准备插入
//        {
//            out += ' ';
//        }
//        vector<string>op;
//        int post = 0;
//        for (int i = 0; i < out.size(); i++)
//        {
//            if (out[i] == ' ')
//            {
//                op.push_back(out.substr(post, i - post));//截取字符段
//                post = i + 1;
//            }
//        }
//        reverse(op.begin(), op.end());//逆置
//        s.clear();
//        int count = op.size() - 1;//空格数
//        for (auto& e : op)
//        {
//            s += e;
//            if (count--)
//            {
//                s += ' ';
//            }
//        }
//        return s;
//    }
//};



//组合
//class Solution {
//public:
//    vector<int>cur;
//    vector<vector<int>>out;
//    void backracking(int len, int brock, int index)
//    {
//        if (cur.size() == brock)//当cur达到块大小时，保存结果
//        {
//            out.push_back(cur);
//            return;
//        }
//        for (int i = index; i <= len; i++)//范围遍历
//        {
//            cur.push_back(i);//处理
//            backracking(len, brock, i + 1);//下一位处理
//            cur.pop_back();//回溯，踢出上次的结果
//        }
//    }
//    vector<vector<int>> combine(int n, int k)
//    {
//        backracking(n, k, 1);//给数值范围，块大小、起点
//        return out;
//    }
//};


//剑指 Offer II 082. 含有重复元素集合的组合
//class Solution {
//public:
//    vector<int>cur;
//    vector<vector<int>>out;
//    void backracking(vector<int>& vate, int target, int sum, int index)
//    {
//        if (sum > target) return;
//        if (sum == target)
//        {
//            out.push_back(cur);
//            return;
//        }
//        for (int i = index; i < vate.size() && sum + vate[i] <= target; i++)
//        {
//            if (i > index)
//            {
//                if (vate[i] == vate[i - 1])
//                {
//                    continue;
//                }
//            }
//            cur.push_back(vate[i]);
//            int op = 0;
//            for (size_t j = 0; j < cur.size(); j++)
//            {
//                op += cur[j];
//            }
//            backracking(vate, target, op, i + 1);
//            cur.pop_back();
//            sum -= vate[i];
//        }
//    }
//    vector<vector<int>> combinationSum2(vector<int>& candidates, int target)
//    {
//        sort(candidates.begin(), candidates.end());
//        backracking(candidates, target, 0, 0);
//        return out;
//    }
//};

//电话号码组合
//class Solution {
//public:
//    vector<string>out;
//    vector<string>nums = { "0","0","abc","def","ghi","jkl","mno","pqrs","tuv","wxyz" };
//    void backracking(const string& cur, int index, const string& op)
//    {
//        if (op.size() == cur.size())
//        {
//            out.push_back(op);
//            return;
//        }
//        int count = cur[index] - '0';
//        string ot = nums[count];
//        for (int i = 0; i < ot.size(); i++)
//        {
//            backracking(cur, index + 1, op + ot[i]);
//        }
//    }
//    vector<string> letterCombinations(string digits)
//    {
//        if (!digits.size()) return {};
//        backracking(digits, 0, "");
//        return out;
//    }
//};


//输出倒数第K位
//#include <iostream>
//#include <vector>
//#include <algorithm>
//using namespace std;
//
//struct ListNode
//{
//    ListNode(int _m_nKey = 0, ListNode* _m_pnode = nullptr)
//        :m_nKey(_m_nKey)
//        , m_pNext(_m_pnode)
//    {}
//    int m_nKey;
//    ListNode* m_pNext;
//};
//
//int main()
//{
//    int count = 0;
//    while (cin >> count)
//    {
//        ListNode* head = new ListNode(0);
//        ListNode* cur = head;
//        vector<int>out(count);
//        for (size_t i = 0; i < count; i++)
//        {
//            cin >> out[i];
//        }
//        int post = 0;
//        cin >> post;
//        for (size_t i = 0; i < count; i++)
//        {
//            cur->m_pNext = new ListNode(out[i]);
//            cur = cur->m_pNext;
//        }
//        post = count - post;
//        head = head->m_pNext;
//        while (post--)
//        {
//            head = head->m_pNext;
//        }
//        cout << head->m_nKey << endl;
//    }
//    return 0;
//}

//376. 摆动序列
//class Solution {
//public:
//    int wiggleMaxLength(vector<int>& nums)
//    {
//        int slow = 0;
//        int first = 1;
//        int cur = 0;
//        vector<int>out(nums.size() - 1);
//        while (first < nums.size())
//        {
//            out[cur++] = nums[first++] - nums[slow++];
//        }
//        cur = 0;
//        int flag = 1;
//        for (auto& e : out)
//        {
//            if (cur == 0 && e > 0)//最为核心：需要判断起始的状态是峰顶或封底
//            {
//                flag = 1;
//            }
//            if (cur == 0 && e < 0)
//            {
//                flag = 0;
//            }
//            if (flag == 1 && e > 0)
//            {
//                flag = 0;
//                cur++;
//            }
//            if (flag == 0 && e < 0)
//            {
//                flag = 1;
//                cur++;
//            }
//        }
//        return cur + 1;
//    }
//};