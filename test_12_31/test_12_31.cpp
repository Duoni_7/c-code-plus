#define  _CRT_SECURE_NO_WARNINGS 1

//二叉树最左元素
//class Solution {
//public:
//    int findBottomLeftValue(TreeNode* root)
//    {
//        TreeNode* out = nullptr;
//        queue<TreeNode*>qu;
//        qu.push(root);
//        while (!qu.empty())
//        {
//            int size = qu.size();
//            out = qu.front();
//            while (size--)
//            {
//                TreeNode* cur = qu.front();
//                qu.pop();
//                if (cur->left) qu.push(cur->left);
//                if (cur->right) qu.push(cur->right);
//            }
//        }
//        return out->val;
//    }
//};

//层序遍历
//class Solution {
//public:
//    vector<vector<int>> levelOrder(TreeNode* root)
//    {
//        if (!root) return {};
//        queue<TreeNode*>qu;
//        vector<vector<int>>out;
//        qu.push(root);
//        while (!qu.empty())
//        {
//            int size = qu.size();
//            vector<int>cur;
//            while (size--)
//            {
//                TreeNode* pre = qu.front();
//                qu.pop();
//                cur.push_back(pre->val);
//                if (pre->left) qu.push(pre->left);
//                if (pre->right) qu.push(pre->right);
//            }
//            out.push_back(cur);
//        }
//        return out;
//    }
//};

//class Solution {
//public:
//    vector<vector<int>> levelOrderBottom(TreeNode* root)
//    {
//        if (!root) return {};//若根节点为空，返回空
//        vector<vector<int>>out;//建立二维数组
//        queue<TreeNode*>q;//队列，用于分层
//        q.push(root);//插入头节点
//        while (!q.empty())//当队列为空时停止循环
//        {
//            vector<int> cur;//保存层节点数据
//            int size = q.size();//记录单层数据个数
//            while (size--)
//            {
//                TreeNode* node = q.front();
//                q.pop();
//                cur.push_back(node->val);//保存数据
//                if (node->left) q.push(node->left);//保存节点左右孩子
//                if (node->right) q.push(node->right);
//            }
//            out.push_back(cur);//插入单层的数据
//        }
//        reverse(out.begin(), out.end());
//        return out;
//    }
//};

//class Solution {
//public:
//    vector<int> inorderTraversal(TreeNode* root)
//    {
//        if (!root) return {};
//        stack<TreeNode*>st;
//        vector<int>out;
//        while (root || !st.empty())
//        {
//            if (root != nullptr)
//            {
//                st.push(root);
//                root = root->left;
//            }
//            else
//            {
//                root = st.top();
//                st.pop();
//                out.push_back(root->val);
//                root = root->right;
//            }
//        }
//        return out;
//    }
//};


//二叉搜索树与双向链表
//class Solution {
//public:
//	void get_num(TreeNode* root, vector<int>& out)
//	{
//		if (!root) return;
//		get_num(root->left, out);
//		out.push_back(root->val);
//		get_num(root->right, out);
//	}
//	void set_node(TreeNode*& root, int val)
//	{
//		if (!root)
//		{
//			root = new TreeNode(val);
//		}
//		else
//		{
//			TreeNode* cur = root;
//			while (cur->right)
//			{
//				cur = cur->right;
//			}
//			TreeNode* node = new TreeNode(val);
//			cur->right = node;
//			node->left = cur;
//		}
//	}
//	TreeNode* Convert(TreeNode* pRootOfTree)
//	{
//		vector<int>out;
//		TreeNode* head = nullptr;
//		get_num(pRootOfTree, out);
//		for (size_t i = 0; i < out.size(); i++)
//		{
//			set_node(head, out[i]);
//		}
//		return head;
//	}
//};