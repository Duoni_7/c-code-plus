#define  _CRT_SECURE_NO_WARNINGS 1


//二叉树的层序遍历
//class Solution {
//public:
//    /**
//     *
//     * @param root TreeNode类
//     * @return int整型vector<vector<>>
//     */
//    vector<vector<int> > levelOrder(TreeNode* root)
//    {
//        if (!root) return {};
//        vector<vector<int>>out;
//        queue<TreeNode*>q;
//        q.push(root);
//        while (!q.empty())
//        {
//            vector<int>cur;
//            int sz = q.size();
//            while (sz--)
//            {
//                TreeNode* node = q.front();
//                q.pop();
//                cur.push_back(node->val);
//                if (node->left) q.push(node->left);
//                if (node->right) q.push(node->right);
//            }
//            out.push_back(cur);
//        }
//        return out;
//    }
//};


//按之字形打印二叉树
//class Solution {
//public:
//    vector<vector<int> > Print(TreeNode* pRoot)
//    {
//        if (!pRoot)  return {};
//        vector<vector<int>>out;
//        queue<TreeNode*>q;
//        int flag = 0;
//        q.push(pRoot);
//        while (!q.empty())
//        {
//            int sz = q.size();
//            vector<int>cur;
//            flag++;//记录层数
//            while (sz--)
//            {
//                TreeNode* node = q.front();
//                q.pop();
//                cur.push_back(node->val);
//                if (node->left) q.push(node->left);
//                if (node->right) q.push(node->right);
//            }
//            if (flag % 2 == 0)//偶数层数据逆置
//            {
//                reverse(cur.begin(), cur.end());
//            }
//            out.push_back(cur);
//        }
//        return out;
//    }
//
//};


//二叉树的最大深度
//int maxDepth(TreeNode* root)
//{
//    if (!root) return 0;
//    queue<TreeNode*>q;
//    int death = 0;
//    q.push(root);
//    while (!q.empty())
//    {
//        death++;//记录最大深度
//        int sz = q.size();
//        while (sz--)
//        {
//            TreeNode* node = q.front();
//            q.pop();
//            if (node->left) q.push(node->left);
//            if (node->right) q.push(node->right);
//        }
//    }
//    return death;
//}
//};