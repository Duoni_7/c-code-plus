#define  _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<stdlib.h>
#include<sys/types.h>
#include<sys/wait.h>
#include<unistd.h>
using namespace std;

#define NUM 16

int main()
{
	// while(true)                        
	//{                                  
	       pid_t id = fork();
	       if (id == 0)
           {
				cout << "子进程开始运行 子进程pid:" << getpid() << endl;
		        sleep(3);
		     // execl("/usr/bin/ls","ls","-a","-l",NULL);                                    
                char* const _argv[NUM] = { (char*)"ls",(char*)"-a",(char*)"-l",NULL };
		     // execv("/usr/bin/ls",_argv);
			 // execlp("ls","ls","-a","-l",NULL);
                execvp("ls", _argv);
	            exit(1);
           }
           else
           {                                                                                          
			   cout << "父进程开始运行 父进程pid:" << getpid() << endl;
               int status = 0;
               pid_t id = waitpid(-1, &status, 0);
               if (id > 0)
               {
				   cout << "wait success, exit code:" << WEXITSTATUS(status) << endl;
               }
	       }
    // }                                  
		   return 0;
}
