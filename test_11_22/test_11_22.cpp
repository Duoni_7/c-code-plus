#define  _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<string>
#include<vector>
using namespace std;

//不同字母的排序
//const char* let[10] = { "","","abc","def","ghi","jkl","mno","pqrs","tuv","wxyz" };
//void combine(string digits, int dit, vector<string>& v, string vstr)
//{
//    if (dit == digits.size())
//    {
//        v.push_back(vstr);
//        return;
//    }

//    int num = digits[dit] - '0';
//    string str = let[num];
//    for (auto ch : str)
//    {
//        combine(digits, dit + 1, v, vstr + ch);
//    }
//}

//vector<string> letterCombinations(string digits)
//{
//    vector<string>v;
//    if (digits.size() == 0)
//    {
//        return v;
//    }

//    string str;
//    combine(digits, 0, v, str);
//    return v;
//}

//int main()
//{
//    string str("23");
//    for (auto it : letterCombinations(str))
//    {
//        cout << it << " ";
//    }
//    return 0;
//}

//前序遍历
// class Solution {
//public:
//    void Before_tree(vector<int>& out, TreeNode* root)
//    {
//        if (root == nullptr)//如果结点遍历结束，停止递归
//        {
//            return;
//        }
//        out.push_back(root->val);//记录根节点
//        Before_tree(out, root->left);//访问左节点
//        Before_tree(out, root->right);//访问右节点
//    }
//
//    vector<int> preorderTraversal(TreeNode* root)
//    {
//        if (root == nullptr)//若是空树，则返回空容器
//        {
//            return {};
//        }
//        vector<int>out;
//        Before_tree(out, root);
//        return out;
//    }
//};


//中序遍历
//class Solution {
//public:
//
//    void Before_tree(TreeNode* root, vector<int>& out)
//    {
//        if (root == nullptr)//若树遍历完毕，则退出
//        {
//            return;
//        }
//        Before_tree(root->left, out);//进入左子树
//        out.push_back(root->val);//记录节点值
//        Before_tree(root->right, out);//记录右节点
//    }
//
//    vector<int> inorderTraversal(TreeNode* root)
//    {
//        if (root == nullptr)//若是空树则返回空
//        {
//            return {};
//        }
//        vector<int>out;
//        Before_tree(root, out);
//        return out;
//    }
//};

//后序遍历
//class Solution {
//public:
//
//    void Before_tree(TreeNode* root, vector<int>& out)
//    {
//        if (root == nullptr)
//        {
//            return;
//        }
//        Before_tree(root->left, out);
//        Before_tree(root->right, out);
//        out.push_back(root->val);
//    }
//
//    vector<int> postorderTraversal(TreeNode* root)
//    {
//        if (root == nullptr)
//        {
//            return {};
//        }
//        vector<int>out;
//        Before_tree(root, out);
//        return out;
//    }
//};

//int find_num(vector<int>& out, int target)
//{
//	static int cur = 0;
//	if (out[cur] == target)
//	{
//		return cur;
//	}
//	cur++;
//	if(cur == out.size())
//	{
//		return 0;
//	}
//	find_num(out, target);
//}
//
//int main()
//{
//	int target = 9;
//	vector<int>out({ 1,3,4,8,6,9 });
//	int res = find_num(out, target);
//	cout << res;
//	return 0;
//}

