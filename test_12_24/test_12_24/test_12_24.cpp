#define  _CRT_SECURE_NO_WARNINGS 1


//矩阵中战斗力最弱的 K 行
//class Solution {
//public:
//    vector<int> kWeakestRows(vector<vector<int>>& mat, int k)
//    {
//        vector<int>coun;//用于保存每列中军人的个数
//        vector<int>out;//用于保存返回下标点
//        for (size_t i = 0; i < mat.size(); i++)
//        {
//            coun.push_back(count(mat[i].begin(), mat[i].end(), 1));//统计每列军人的个数
//        }
//        vector<int>cp(coun);//建立映射关系
//        sort(cp.begin(), cp.end());//排序出每列的强弱关系
//        for (size_t i = 0; i < k; i++)
//        {
//            auto pos = find(coun.begin(), coun.end(), cp[i]);//获取coun中cp[i]对印的下标值
//            out.push_back(pos - coun.begin());
//            *pos = -1;//获取后，将coun中的对应值置为无效
//        }
//        return out;
//    }
//};


//分割字符串
//class Solution {
//public:
//    int balancedStringSplit(string s)
//    {
//        int bal_num = 0, count = 0;
//        for (size_t i = 0; i < s.size(); i++)
//        {
//            if (s[i] == 'L')
//            {
//                bal_num--;
//            }
//            else
//            {
//                bal_num++;
//            }
//            if (bal_num == 0)
//            {
//                count++;
//            }
//        }
//        return count;
//    }
//};


//查找共用字符
//class Solution {
//public:
//    vector<string> commonChars(vector<string>& words)
//    {
//        vector<string>out;
//        //公共字符就意味着是：每一个字符串中都包含的字符，所以只要哪其中一个字符串切入就可以
//        for (size_t i = 0; i < words[0].size(); i++)//遍历单字符串
//        {
//            int flag = 1;
//            for (size_t j = 1; j < words.size(); j++)//遍历二维数组中的其他字符串
//            {
//                //查找该字符是否都在其他字符串中出现过
//                auto pos = find(words[j].begin(), words[j].end(), words[0][i]);
//                if (words[j].end() == pos)
//                {
//                    flag = 0;//若没有出现，直接退出
//                    break;
//                }
//                *pos = '*';
//            }
//            if (flag)//若都出现了，则保存
//            {
//                string cur;
//                cur = words[0][i];
//                out.push_back(cur);
//            }
//        }
//        return out;
//    }
//};

//字符串压缩
//class Solution {
//public:
//    string compressString(string S)
//    {
//        int count_num = 1;//对于相同字符的计数，默认值：1
//        int cur = 1;//快慢法，cur游标为快方，i为慢方
//        string out_str;//新建一个字符串对象
//        for (int i = 0; i < S.size(); i++)//遍历计数
//        {
//            //如果计数完一个相同字符的区间后，快方与慢方元素不同，则处理
//            if (S[i] != S[cur])
//            {
//                //将慢方字符尾插进对象
//                out_str += S[i];
//                //计数转换为字符串后尾插进对象
//                out_str += to_string(count_num);
//                //计数清0
//                count_num = 0;
//            }
//            ++count_num;//计数++
//            ++cur;///快方++
//        }
//        if (out_str.size() < S.size())
//            //如果缩减后字符串长度少于源字符串，则返回缩减后字符串
//        {
//            return out_str;
//        }
//        return S;
//    }
//};



//二叉树的锯齿形层序遍历
//class Solution {
//public:
//    vector<vector<int>> zigzagLevelOrder(TreeNode* root)
//    {
//        if (!root) return {};
//        int cur = 0;
//        queue<TreeNode*>qu;
//        vector<vector<int>>out;
//        qu.push(root);
//        while (!qu.empty())
//        {
//            int size = qu.size();
//            vector<int>vate;
//            cur++;
//            while (size--)
//            {
//                TreeNode* node = qu.front();
//                qu.pop();
//                vate.push_back(node->val);
//                if (node->left) qu.push(node->left);
//                if (node->right) qu.push(node->right);
//            }
//            if (cur % 2 == 0)
//            {
//                reverse(vate.begin(), vate.end());
//            }
//            out.push_back(vate);
//        }
//        return out;
//    }
//};