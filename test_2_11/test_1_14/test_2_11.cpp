#define  _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
using namespace std;

//884. 两句话中的不常见单词
//class Solution {
//public:
//    vector<string> uncommonFromSentences(string s1, string s2)
//    {
//        vector<string>out;
//        s1 += ' ' + s2 + ' ';
//        map<string, int>ms;
//        string cur;
//        for (auto& e : s1)
//        {
//            if (e != ' ')
//            {
//                cur += e;
//            }
//            else
//            {
//                ms[cur]++;
//                cur.clear();
//            }
//        }
//        for (auto& e : ms)
//        {
//            if (e.second == 1)
//            {
//                out.push_back(e.first);
//            }
//        }
//        return out;
//    }
//};

//350. 两个数组的交集 II
//class Solution {
//public:
//    vector<int> intersect(vector<int>& nums1, vector<int>& nums2)
//    {
//        vector<int>out;
//        sort(nums1.begin(), nums1.end());
//        sort(nums2.begin(), nums2.end());
//        int pos1 = 0;
//        int pos2 = 0;
//        while (pos1 < nums1.size() && pos2 < nums2.size())
//        {
//            if (nums1[pos1] == nums2[pos2])
//            {
//                out.push_back(nums1[pos1]);
//                pos1++;
//                pos2++;
//            }
//            else if (nums1[pos1] < nums2[pos2])
//            {
//                pos1++;
//            }
//            else
//            {
//                pos2++;
//            }
//        }
//        return out;
//    }
//};

//217. 存在重复元素
//class Solution {
//public:
//    bool containsDuplicate(vector<int>& nums)
//    {
//        map<int, int>op;
//        for (auto e : nums)
//        {
//            op[e]++;
//        }
//        for (auto& e : op)
//        {
//            if (e.second > 1)
//            {
//                return true;
//            }
//        }
//        return false;
//    }
//};

int main()
{
	int* p = new int;
	*p = 9;
	cout << p << " " << *p << endl;
	delete p;
	return 0;
}