#pragma once
#include<iostream>
#include<assert.h>
using namespace std;

namespace XLJ
{
	class String
	{
	public:

		//迭代器
		typedef char* iterator;
		//const迭代器
		typedef const char* const_iterator;

		//beging()
		iterator begin()
		{
			return _str;
		}

		//end()
		iterator end()
		{
			return _str + _size;
		}

		//const beging()
		const_iterator begin()const
		{
			return _str;
		}

		//const end()
		const_iterator end()const
		{
			return _str + _size;
		}

		////构造函数的主要使命是为了初始化对象中的成员变量
		// String(const char* str = "")
		//	 :_str(new char[strlen(str)+1])//string类中尾部存在一个“/0”的标识符
		//	 ,_size(strlen(str))//标识符“/0”不是有效字符
		//	 ,_capecity(strlen(str))
		// {
		//	 if (strlen(str))
		//	 {
		//		 strcpy(_str, str);
		//	 }
		// }

		//构造
		String(const char* str = "")
		{
			_size = strlen(str);
			_capecity = _size;
			_str = new char[_capecity + 1];
			if (_size)
			{
				strcpy(_str, str);
			}
		}

		 //析构函数
		 ~String()
		 {
			 delete[] _str;
			 _capecity = 0;
			 _size = 0;
		 }

		 //c_str将string对象转化成常量字符串
		 const char* c_str()const
		 {
			 return _str;
		 }

		  //返回_size
		 size_t size()const
		 {
			 return _size;
		 }

		 //返回_capecity
		 size_t capecity()const
		 {
			 return _capecity;
		 }

		 //方括号
		 char& operator[](size_t pos)
		 {
			 assert(pos < _size);
			 return _str[pos];
		 }

		 //方括号（常对象调用）
		 const char& operator[](size_t pos)const
		 {
			 assert(pos < _size);
			 return _str[pos];
		 }

		 //打印
		 inline void Print()const
		 {
			 cout << _str << endl;
			 cout << _size << endl;
			 cout << _capecity << endl;
		 }
	private:
		char* _str;
		size_t _size;
		size_t _capecity;
	};
}

void test_String_1()
{
	XLJ::String a("hello");
	for (size_t i = 0; i < a.size(); i++)
	{
		a[i]++;
	}
	for (size_t i = 0; i < a.size(); i++)
	{
		cout << a[i] << " ";
	}
	cout << endl;
	//cout << a[1] << endl;
	cout << a.size() << endl;
	cout << a.capecity() << endl;
	//cout << a.c_str() << endl;
	//a.Print();
}

void test_String_2()
{
	XLJ::String a("hello");
	XLJ::String::iterator op = a.begin();
	while (op != a.end())
	{
		cout << *op << " ";
		op++;
	}
	cout << endl;
}