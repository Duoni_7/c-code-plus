#include<iostream>
using namespace std;


//淘宝网店
//static int year_num[13] = { 0,62,28,31,60,31,60,31,62,60,62,30,62 };
//static int sigle_num[13] = { 0,2,1,1,2,1,2,1,2,2,2,1,2 };
//
//bool leap_year(int year)
//{
//    if (year % 4 == 0 && year % 100 != 0 || year % 400 == 0)
//    {
//        return true;
//    }
//    return false;
//}
//
//int main()
//{
//    int money_num = 0;
//    int year_1 = 0, month_1 = 0, day_1 = 0;
//    int year_2 = 0, month_2 = 0, day_2 = 0;
//    while (cin >> year_1 >> month_1 >> day_1 >> year_2 >> month_2 >> day_2)
//    {
//        //如果年份相当，计算月份差数
//        if (year_1 == year_2)
//        {
//            //如果月份相等，计算天差数
//            if (month_1 == month_2)
//            {
//                int cur = ((day_2 - (day_1 - 1))) * (year_num[month_1] > 31 ? 2 : 1);
//                money_num += cur;
//            }
//            //如果月份小于，补齐月与天数差数
//            else
//            {
//                int cur = month_1;
//                while (cur < month_2)
//                {
//                    money_num += year_num[cur++];
//                    if (leap_year(year_1) && cur == 2)
//                    {
//                        money_num += 1;
//                    }
//                }
//                if (day_1 < day_2)
//                {
//                    int cur = (day_2 - (day_1 - 1)) * sigle_num[month_2];
//                    money_num += cur;
//                    if (leap_year(year_1) && month_2 == 2)
//                    {
//                        money_num += 1;
//                    }
//                }
//                else
//                {
//                    int cur = (day_1 - (day_2 - 1)) * sigle_num[month_2];
//                    money_num -= cur;
//                    if (leap_year(year_1) && month_2 == 2)
//                    {
//                        money_num -= 1;
//                    }
//                }
//            }
//        }
//        //如果年份不相等，计算年份、月份、天数差数
//        else
//        {
//            int cur_year = year_1;
//            while (cur_year != year_2)
//            {
//                int cur_month = month_1;
//                while (true)
//                {
//                    if (leap_year(cur_year) && cur_month == 2)
//                    {
//                        money_num += 1;
//                    }
//                    money_num += year_num[cur_month++];
//                    if (cur_month > 12)
//                    {
//                        cur_year++;
//                        cur_month = 1;
//                    }
//                    if (cur_year == year_2)
//                    {
//                        break;
//                    }
//                }
//            }
//            //如果月份相等，计算天差数
//            if (month_1 == month_2)
//            {
//                int cur = ((day_2 - (day_1 - 1))) * (year_num[month_1] > 31 ? 2 : 1);
//                money_num += cur;
//            }
//            //如果月份小于，补齐月与天数差数
//            else
//            {
//                int cur = month_1;
//                while (cur < month_2)
//                {
//                    money_num += year_num[cur++];
//                    if (leap_year(year_1) && cur == 2)
//                    {
//                        money_num += 1;
//                    }
//                }
//                if (day_1 < day_2)
//                {
//                    int cur = (day_2 - (day_1 - 1)) * sigle_num[month_2];
//                    money_num += cur;
//                    if (leap_year(year_1) && month_2 == 2)
//                    {
//                        money_num += 1;
//                    }
//                }
//                else
//                {
//                    int cur = (day_1 - (day_2 - 1)) * sigle_num[month_2];
//                    money_num -= cur;
//                    if (leap_year(year_1) && month_2 == 2)
//                    {
//                        money_num -= 1;
//                    }
//                }
//            }
//        }
//        if (year_2 == 2999)
//        {
//            cout << money_num - 1 << endl;
//            continue;
//        }
//        cout << money_num << endl;
//        money_num = 0;
//    }
//    return 0;
//}

//斐波那契凤尾
//#include<iostream>
//using namespace std;
//
//int main()
//{
//    int n = 0;
//    long long out[100000];
//    out[0] = 1;
//    out[1] = 1;
//    for (int i = 2; i <= 100000; i++)
//    {
//        out[i] = out[i - 1] + out[i - 2];
//        out[i] = out[i] % 1000000;
//    }
//    while (cin >> n)
//    {
//        if (n < 29)
//            printf("%d\n", out[n]);
//        else
//            printf("%06d\n", out[n]);
//    }
//    return 0;
//}