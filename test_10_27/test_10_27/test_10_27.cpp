#define  _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<string>
using namespace std;

//删除链表重复结点
/**
 * struct ListNode {
 *	int val;
 *	struct ListNode *next;
 * };
 */

//class Solution {
//public:
    /**
     *
     * @param head ListNode类
     * @return ListNode类
     */
//    ListNode* set_node(int x)
//    {
//        ListNode* new_node = new ListNode(x);
//        new_node->next = nullptr;
//        return new_node;
//    }
//    ListNode* deleteDuplicates(ListNode* head)
//    {
//        if (head == nullptr || head->next == nullptr)
//        {
//            return head;
//        }
//        ListNode* slow = head;
//        ListNode* fast = head->next;
//        ListNode* new_node = new ListNode(-1);
//        new_node->next = nullptr;
//        ListNode* hea_ = new_node;
//        while (fast != nullptr)
//        {
//            if (slow->val != fast->val)
//            {
//                new_node->next = set_node(slow->val);
//                new_node = new_node->next;
//            }
//            fast = fast->next;
//            slow = slow->next;
//        }
//        new_node->next = set_node(slow->val);
//        return hea_->next;
//    }
//};

    //int main()
    //{
    //    string s("This is a sample");
    //    string out;
    //    int res = 0;
    //    for (size_t i = 0; i < s.size(); i++)
    //    {
    //        if (s[i] >= 'a' && s[i] <= 'z')
    //        {
    //            out += s[i] - 32;

    //        }
    //        else if (s[i] >= 'A' && s[i] <= 'Z')
    //        {
    //            out += s[i] + 32;

    //        }
    //        else
    //        {
    //            out += s[i];
    //        }
    //    }
    //    reverse(out.begin(), out.end());
    //    for (size_t j = 0; j < out.size(); j++)
    //    {
    //        if (out[j] == ' ')
    //        {
    //            reverse(out.begin() + res, out.begin() + j);
    //            res = j + 1;
    //        }
    //    }
    //    reverse(out.begin() + res, out.end());
    //    cout << out;
    //}


//左旋字符串
//字符串截取函数substr(开始位置，向后截取几个)
//class Solution {
//public:
//    string LeftRotateString(string str, int n)
//    {
//        if (str.size() == 0)
//        {
//            return str;
//        }
//        string out;
//        if (n < str.size())
//        {
//            out += str.substr(n, str.size() - n);
//            out += str.substr(0, n);
//        }
//        else if (n > str.size())
//        {
//            out += str.substr(n % str.size(), str.size() - n % str.size());
//            out += str.substr(0, n % str.size());
//        }
//        else
//        {
//            return str;
//        }
//        return out;
//    }
//};

//class Stack
//{
//public:
//    Stack(int capecity = 4)
//    {
//        _vate = new int[capecity];
//        _capecity = capecity;
//        _top = 0;
//    }
//
//    Stack(const Stack& stack)
//    {
//        memccpy(_vate, stack._vate, sizeof(_vate), sizeof(int));
//        _capecity = stack._capecity;
//        _top = stack._top;
//    }
//
//private:
//    int* _vate;
//    int _capecity;
//    int _top;
//};

class Date
{
public:
    Date(int year = 1, int month = 1, int day = 1)
    {
        _year = year;
        _month = month;
        _day = day;
    }
    inline bool operator==(const Date& ou)
    {
        return _year == ou._year
            &&_month == ou._month
            &&_day == ou._day;
    }
private:
    int _year;
    int _month;
    int _day;
};

int main()
{
    Date res;
    Date ret(1, 2, 2);
    cout << (res == ret) << endl;//用户视角
    // cout << operator==(res, ret) << endl;//编译器视角
    Date rwe(res);
    return 0;
}
