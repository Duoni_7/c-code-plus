#include"BlockQueue.hpp"
#include"Task.hpp"
#include<time.h>

int myadd(int x, int y)
{
    return x+y;
}

void* consumer(void* args)//消费者
{
    BlockQueue<Task>* bqueue = (BlockQueue<Task>*)args;
    while(true)//消费过程
    {
        //获取任务
        Task t;
        bqueue->Pop(&t);
        cout<<pthread_self()<<"... consumer: "<<t.x_<<" + "<<t.y_<<" = "<<t()<<endl;
        
        // cout<<"消费一个数据： "<<a<<endl;
    }
    return nullptr;
}

void* productor(void* args)//生产者
{
    BlockQueue<Task>* bqueue = (BlockQueue<Task>*)args;
    // int a = 1; 
    while(true)//生产过程
    {
        int x = rand()%10+1;
        usleep(1000);
        int y = rand()%5+1;

        // int x,y;
        // cout<<"Please Enter x&&y: ";
        // cin>>x>>y;

        Task t(x,y,myadd);

        bqueue->Push(t);

        cout<<pthread_self()<<"... productor: "<<t.x_<<" + "<<t.y_<<" = ?"<<endl;
        sleep(1);
    }
    return nullptr;
}

int main()
{
    srand((uint64_t)time(nullptr)^getpid()^133333);//时间戳种子
    BlockQueue<Task>* bqueue = new BlockQueue<Task>();

    pthread_t c[2],p[2 ];
    pthread_create(c,nullptr,consumer,bqueue);//消费者
    pthread_create(c+1,nullptr,consumer,bqueue);//消费者
    pthread_create(p,nullptr,productor,bqueue);//生产者
    pthread_create(p+1,nullptr,productor,bqueue);//生产者

    pthread_join(*c,nullptr);
    pthread_join(c[1],nullptr);
    pthread_join(*p,nullptr);
    pthread_join(p[1],nullptr);

    delete bqueue;
    return 0;
}