#pragma once

#include<iostream>
#include<functional>
using namespace std;

typedef function<int(int,int)> func_t;//函数包装器

class Task
{
public:
    Task()
    {}
 
    Task(int x, int y, func_t func)
    :x_(x)
    ,y_(y)
    ,func_(func)
    {}

    //执行仿函数
    int operator()()//仿函数
    {
        return func_(x_,y_);
    }
    
    int x_;
    int y_;
    func_t func_;
};
