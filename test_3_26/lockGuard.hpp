#pragma once
#include<iostream>
#include<pthread.h>
using namespace std;

class Mutex
{
public:
    Mutex(pthread_mutex_t *mtx)
    :pmtx_(mtx)
    {}
    void lock()//加锁
    {
        cout<<"进行了加锁..."<<endl;
        pthread_mutex_lock(pmtx_);
    }
    void unlock()//解锁
    {
        cout<<"进行了解锁..."<<endl;
        pthread_mutex_unlock(pmtx_);
    }
    ~Mutex()
    {}

private:
    pthread_mutex_t *pmtx_;
};

class lockGuard
{
public:
    lockGuard(pthread_mutex_t *mtx)
    :mtx_(mtx)
    {
        mtx_.lock();
    }
    ~lockGuard()
    {
        mtx_.unlock();
    }
private:
    Mutex mtx_;
};