#pragma once
#include<iostream>
#include<assert.h>
using namespace std;


namespace my_vector
{
	template<class T>
	class vector
	{
	public:
		typedef T* iterator;//迭代器的声明放在公有域
		typedef const T* const_iterator;

		//const迭代器_供const对象调用
		const_iterator begin()const
		{
			return _start;
		}

		const_iterator end()const
		{
			return _finish;
		}

		//begin
		iterator begin()//注意重载不能依据返回参数
		{
			return _start;
		}

		//end
		iterator end()
		{
			return _finish;
		}

		vector()//构造函数
			:_start(nullptr)
			, _finish(nullptr)
			,_end_of_storage(nullptr)
		{}

		~vector()//析构函数
		{
			delete[]_start;
			_start = _finish = _end_of_storage = nullptr;
		}

		//返回容量
		size_t capecity()const
		{
			return _end_of_storage - _start;//指针间相减，得出的是二者之间的距离
		}

		//返回有效数位
		size_t size()const
		{
			return _finish - _start;
		}

		//尾插
		void push_back(const T& x)
		{
			if (_finish == _end_of_storage)
			{
				reserve(capecity() == 0 ? 4 : 2 * capecity());
			}
			*_finish = x;
			_finish++;
		}

		//方括号 
		T& operator[](size_t pos)const
		{
			assert(pos < size());
			return _start[pos];
		}
		
		//扩容
		void reserve(size_t len)
		{
			if (len > capecity())
			{
				size_t sz = size();//记录未扩容前finish在start中的位置
				T* new_start = new T[len];
				if (_start != nullptr)
				{
					memcpy(new_start, _start, sizeof(T)*sz);//按字节拷贝内存
					delete[] _start;
				}
				_start = new_start;//头指针
				_finish = _start + sz;//有效位指针
				_end_of_storage = _start + len;//容器容量指针
			}
		}

		//pop_back
		void pop_back()
		{
			assert(_finish > _start);
			_finish--;
		}

		//insert与erase的迭代器失效问题

		//insert 返回当前插入点的迭代器
		//解决insert时发生的pos点失效（迭代器失效问题）
		//先记录pos到原本_start的距离，等到发生扩容时，将这一段距离与新空间的begin相加，就得到了新空间的pos点 
		iterator insert(iterator pos, const T& x)
		{
			assert(pos >= _start && pos <= _finish);//支持尾插 、头插
			if (_finish == _end_of_storage)
			{
				size_t sz = pos - _start;//记录就空间中begin到pos的距离
				reserve(capecity() == 0 ? 4 : capecity() * 2);
				pos = _start + sz;//更新在新空间中pos点的位置
			}
			iterator cur = this->end()-1;
			iterator end = this->end();
			while (cur >= pos)
			{
				*(end--) = *(cur--);//按需把控优先级
			}
			*pos = x;
			_finish++;
			return pos;
		}

		//erase 删除指定迭代器后，返回被删元素的下一个迭代器
		iterator erase(iterator pos)
		{
			assert(pos >= _start && pos < _finish);
			if (pos == this->end() - 1)
			{
				_finish--;
				return _finish;
			}
			iterator cur = pos + 1;
			while (cur != this->end())
			{
				*(cur-1) = *(cur++);
			}
			_finish--;
			return pos; 
		}

	private:
		iterator _start;
		iterator _finish;
		iterator _end_of_storage;
	};
}


namespace XLJ
{
	void test_vector_1()
	{
		my_vector::vector<int> a;
		a.push_back(1);
		a.push_back(2);
		a.push_back(3);
		a.push_back(4);

		auto p = find(a.begin(), a.end(), 3);
		if (p != a.end())
		{ 
			p = a.erase(p);
			cout << *p << endl;//此时的p是失效的，因为已经完成了扩容，原空间被抹掉了
		}
		/*p = a.erase(a.begin());
		cout << *p << endl;*/
		//a.insert(a.begin()+3, 9);
		//cout<<*(a.erase(a.begin()+3))<<endl;
		my_vector::vector<int>::const_iterator it = a.begin();
		while (it != a.end())
		{
			cout << (*it) << " ";
			it++;
		}
		//for (auto it : a)//支持了迭代器，就支持范围for（替换） 
		//{
		//	cout << it << " ";
		//}
		/*for (size_t i = 0; i < a.size(); i++)
		{
			cout << a[i] << " ";
		}*/
	}

	void test_vector_2()
	{
		my_vector::vector<int> a;
		a.push_back(1);
		a.push_back(2);
		//a.push_back(4);
		a.push_back(3);
		a.push_back(4);
		//a.push_back(5);

		my_vector::vector<int>::iterator ie = a.begin();
		while (ie != a.end())
		{
			if (*ie % 2 == 0)
			{
				ie=  a.erase(ie);//返回被删除数据的下一个迭代器
				continue;
			}
			ie++;
		}

		my_vector::vector<int>::const_iterator it = a.begin();
		while (it != a.end())
		{
			cout << (*it) << " ";
			it++;
		}
	}
}

 