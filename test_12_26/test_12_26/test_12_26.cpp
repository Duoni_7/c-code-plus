#define  _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<assert.h>
#include<stdlib.h>
#include<iostream>
using namespace std;
//1441. 用栈操作构建数组
//class Solution {
//public:
//    vector<string> buildArray(vector<int>& target, int n)
//    {
//        vector<int>num(n);
//        vector<string>out;
//        for (size_t i = 0; i < n; i++)
//        {
//            num[i] = i + 1;
//        }
//        int cur = 0;
//        for (size_t i = 0; i < target.size(); i++)
//        {
//            if (target[i] == num[cur])
//            {
//                out.push_back("Push");
//                cur++;
//            }
//            else
//            {
//                int op = target[i] - num[cur];
//                int size = op;
//                while (op--)
//                {
//                    out.push_back("Push");
//                    out.push_back("Pop");
//                }
//                cur += size;
//                i--;
//            }
//        }
//        return out;
//    }
//};


//class Solution {
//public:
//    vector<vector<int>> construct2DArray(vector<int>& original, int m, int n)
//    {
//        if (original.size() > m * n || original.size() < m * n) return {};
//        vector<vector<int>>out(m, vector<int>(n));
//        int cur = 0;
//        for (size_t i = 0; i < m; i++)
//        {
//            for (size_t j = 0; j < n; j++)
//            {
//                out[i][j] = original[cur++];
//            }
//        }
//        return out;
//    }
//};


//class Solution {
//public:
//    string compressString(string S)
//    {
//        int count_num = 1;//对于相同字符的计数，默认值：1
//        int cur = 1;//快慢法，cur游标为快方，i为慢方
//        string out_str;//新建一个字符串对象
//        for (int i = 0; i < S.size(); i++)//遍历计数
//        {
//            //如果计数完一个相同字符的区间后，快方与慢方元素不同，则处理
//            if (S[i] != S[cur])
//            {
//                //将慢方字符尾插进对象
//                out_str += S[i];
//                //计数转换为字符串后尾插进对象
//                out_str += to_string(count_num);
//                //计数清0
//                count_num = 0;
//            }
//            ++count_num;//计数++
//            ++cur;///快方++
//        }
//        if (out_str.size() < S.size())
//            //如果缩减后字符串长度少于源字符串，则返回缩减后字符串
//        {
//            return out_str;
//        }
//        return S;
//    }
//};

struct heap
{
	int* vate;
	int size;
	int capecity;

	heap()
		:vate(nullptr)
		,size(0)
		,capecity(0)
	{}

	void push(struct heap& op, int x)
	{
		if (size == capecity)
		{
			int ht = op.capecity == 0 ? 4 : op.capecity * 2;
			op.vate = (int*)realloc(op.vate, ht);
			assert(op.vate);
			op.capecity = ht;
		}
		op.vate[op.size++] = x;
	}
};

int main()
{
	heap hp;
	hp.push(hp, 2);
	hp.push(hp, 2);
	hp.push(hp, 2);
	hp.push(hp, 2);
	hp.push(hp, 2);
	
	cout << hp.vate[3];
	return 0;
}