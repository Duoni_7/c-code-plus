#define  _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
using namespace std;

class A
{
public:
	explicit A(int a = 0, int b = 0)//自定义类必须写构造函数
		:_a(a)
		,_b(b)
	{}
private:
	int _a;
	int _b;
};

class B
{
public:
	explicit B(int x = 0)//const常变量 引用 没有构造函数的自定义类型 必须通过初始化列表初始化成员变量
	:_name(x)
	,cp(x,x)
	{}
private:
	int _name;
	A cp;
};

int main()
{
	B at = 5;
	return 0;
}