#define  _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<vector>
#include<algorithm>
using namespace std;

//oj
//vector<int> smallerNumbersThanCurrent(vector<int>& nums)
//{
//    vector<int>cur = nums;
//    vector<int>out;
//    sort(cur.begin(), cur.end());
//    for (size_t i = 0; i < nums.size(); i++)
//    {
//        auto pos = find(cur.begin(), cur.end(), nums[i]);
//        out.push_back(cur.end()-pos);
//    }
//    return out;
//}
//
////oj
//class Solution {
//public:
//    bool uniqueOccurrences(vector<int>& arr)
//    {
//        vector<int>has_(1000, -1001);
//        for (size_t i = 0; i < arr.size(); i++)
//        {
//            if (arr[i] >= 0)
//            {
//                has_[arr[i]]++;
//            }
//            //哈希负数处理
//            else
//            {
//                has_[arr[i] + 1000]++;
//            }
//        }
//
//        arr.resize(0);
//        for (size_t i = 0; i < has_.size(); i++)
//        {
//            if (has_[i] != -1001)
//            {
//                arr.push_back(has_[i]);
//            }
//        }
//
//        for (size_t i = 0; i < arr.size(); i++)
//        {
//            if (2 <= count(arr.begin(), arr.end(), arr[i]))
//            {
//                return false;
//            }
//        }
//
//        return true;
//    }
//};

//派生类作基类
//class Person
//{
//protected:
//	int _a = 20;
//	int _b = 30;
//};
//
//class cp_Per:public Person
//{
//protected:
//	int p = 99;
//	int o = 999;
//};
//
//class cp_next :public cp_Per
//{
//public:
//	void print()
//	{
//		cout << cp_Per::p << endl;
//		cout << cp_Per::o << endl;
//		cout << Person::_a << endl;
//		cout << Person::_b << endl;
//	}
//};
//
//int main()
//{
//	cp_next op;
//	op.print();
//	return 0;
//}

//class Person
//{
//public:
//	int _a = 99;
//	int _b = 999;
//};
//
//class student :public Person
//{
//public:
//	void priant()
//	{
//		cout << Person::_a << endl;
//		cout << Person::_b << endl;
//	}
//protected:
//	int _one = 1;
//};
//
//int main()
//{
//	Person a;
//	student b;
//	Person* op = &a;
//	student* ot = &b;
//	op->_a = 6;
//	op->_b = 66;
//	ot = (student*)op;
//	ot->priant();
//	/*ot._a = 10;
//	ot._b = 100;
//	op = ot;*/
//	//ot.priant();
//	return 0;
//}

//class Person
//{
//public:
//	Person(const char* p = "peter")
//		:_name(p)
//	{
//		cout << "Person()" << endl;
//	}
//
//	~Person()
//	{
//		cout << "~Person()" << endl;
//	}
//
//	Person(const Person& cp)
//	{
//		_name = cp._name;
//		cout << "Person(const Person& cp)" << endl;
//	}
//
//	Person& operator=(const Person& p)
//	{
//		cout << "Person& operator=(const Person& p)" << endl;
//		if (this != &p)
//		{
//			_name = p._name;
//		}
//		return *this;
//	}
//
//	string _name;
//};
//
//class cp_Person :public Person
//{
//public:
//	cp_Person(int x = 0,const char* p = "peter")
//		:_num(x)
//		,Person(p)
//	{
//		cout << "cp_Person()" << endl;
//	}
//
//	~cp_Person()
//	{
//		// Person::~Person();
//		cout << "~cp_Person()" << endl;
//	}
//
//	cp_Person(const cp_Person& s)
//		:Person(s)
//		,_num(s._num)
//	{
//		cout << "cp_Person(const Person& s)" << endl;
//	}
//
//	cp_Person& operator=(const cp_Person& s)
//	{
//		cout << "cp_Person& operator=(const cp_Person& s)" << endl;
//		if (this != &s)
//		{
//			Person::operator=(s);
//			_num = s._num;
//		} 
//		return *this;
//	}
//
//protected:
//	int _num;
//};
//
//int main()
//{
//	cp_Person op(99, "l");
//	cp_Person ot(op);
//	cp_Person ou(99, "l");
//	op = ou;
//	return 0;
//}