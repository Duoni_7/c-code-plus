#pragma once
#include<iostream>
#include<assert.h>
#include<string>
#include<algorithm>
#include<vector>
using namespace std;

namespace XLJ
{
	//节点类
	template<class T>
	class list_node
	{
	public:
		list_node(const T& x = T())
			:_date(x)
			,_next(nullptr)
			,_prev(nullptr)
		{}
		T _date;
		list_node<T>* _next;
		list_node<T>* _prev;
	};

	//迭代器类
	template<class T, class Ref, class Ptr>//通过模板实例化来确定返回值
	//struct的权限是共有的，迭代器是公用的属性，所以必须使用struct构造迭代器类
	struct __list_iterator
	{
		//typedef __list_iterator<T,const T&,const T*> const_iterator;//重点！！！
		
		//后期了解：迭代器类型
		typedef bidirectional_iterator_tag iterator_category;
		typedef T value_type;
		typedef Ptr pointer;
		typedef Ref reference;
		typedef ptrdiff_t difference_type;

		typedef __list_iterator<T,Ref,Ptr> iterator;//自己默认匹配返回类型
		typedef list_node<T> Node; 
		Node* _node;//只是借用节点，并非开辟，不需要析构

		//使用节点来构造迭代器位置
		__list_iterator(Node* node)//构造
			:_node(node)
		{}

		//->重载
		Ptr operator->()
		{
			return &(operator*());//operator*()返回的是一个对象，对对象取地址后在进行返回，然后再由另一个隐藏的->对其进行成员变量访问 
		}

		//*解引用
		Ref operator*()
		{
			return _node->_date;
		}

		//!=重载
		bool operator!=(const iterator& it)const
		{
			return _node != it._node;
		}

		//==重载
		bool operator==(const iterator& it)const
		{
			return _node == it._node;
		}

		//前置++
		iterator& operator++()
		{
			_node = _node->_next;
			return *this;
		}

		//后置++
		iterator& operator++(int)
		{
			iterator cp = _node;
			_node = _node->_next;
			return cp;
		}

		//前置--
		iterator& operator--()
		{
			_node = _node->_prev;
			return *this;
		}

		//后置--解引用
		iterator& operator--(int)
		{
			iterator cp = _node;
			_node = _node->_prev;
			return cp;
		}

	};

	template<class T>
	class list 
	{
		typedef list_node<T> Node;
	public:

		//类模板 
		typedef __list_iterator<T, const T&, const T*> const_iterator;//可读不可写迭代器（权限与否由：对象的权限决定）
		typedef __list_iterator<T, T&, T*> iterator;//可读可写迭代器

		//为什么要构造迭代器：因为list的迭代器已不再是原生指针
		//const begin
		const_iterator begin()const
		{
			return const_iterator(_head->_next); //调用迭代器构造匿名对象
		}

		//const end
		const_iterator end()const
		{
			return const_iterator(_head);
		}

		//begin
		iterator begin()
		{
			return iterator(_head->_next); //调用迭代器构造匿名对象
		}


		//end
		iterator end()
		{
			return iterator(_head);
		}

		list()//构造函数(构造头节点）
		{
			_head = new Node;
			_head->_prev = _head;
			_head->_next = _head;
		}

		void push_back(const T& x)//尾插
		{
			//Node* end = _head->_prev;//第一步：找尾
			//Node* node = new Node(x);//第二步：开辟节点//自定义类型：new一个节点会自动调用构造函数
			//end->_next = node;//尾节点指针指向新节点
			//node->_prev = end;//新节点的指针指向前方的尾节点
			//node->_next = _head;//新节点的尾指针指向头指针
			//_head->_prev = node;//头指针的前一位指向新尾节点

			insert(end(),x);
		}

		//insert
		iterator insert(iterator pos, const T& val)
		{
			Node* cur = pos._node;
			Node* fast = cur->_prev;
			Node* node = new Node(val);
			fast->_next = node;
			node->_prev = fast;
			node->_next = cur;
			cur->_prev = node;
			return iterator(node);
		}

		//push_front
		void push_front(const T& val)
		{
			insert(begin(), val);
		}
	
		//erase
		iterator erase(iterator pos)
		{
			assert(pos != end()); 
			Node* fast = pos._node->_prev;
			Node* end = pos._node->_next;
			fast->_next = end;
			end->_prev = fast;
			delete pos._node;
			return iterator(end);
		}

		//pop_back
		void pop_back()
		{
			erase(--end());
		}

		//pop_front
		void pop_front()
		{
			erase(begin());
		}

	//private:
		Node* _head;//自定义类
	};
}


void tp(const XLJ::list<int>& a)
{
	XLJ::list<int>::const_iterator it = a.begin();
	while (it != a.end())
	{
		//*it=10;
		cout << *it << " ";
		it++;
	}
}
void test_list_1()
{
	XLJ::list<int>a;
	a.push_back(8);
	a.push_back(7);
	a.push_back(9);
	a.push_back(1);
	a.push_back(3);
	a.push_back(5);
	//XLJ::list<int>* b = &a;
	//cout << b->begin()._node->_date << endl;
	//cout << b->_head->_next->_date<<endl;
	//XLJ::list<int>::const_iterator it = a.begin();
	//while (it != a.end())
	//{
	//	cout << *it << " ";
	//	it++;
	//}
	for (auto& e : a)
	{ 
		cout << e << " ";
	}
	//cout << a.begin()._node->_date;
	//tp(a);
}

void test_list_2()
{
	XLJ::list<int>a;
	a.push_back(8);
	a.push_back(7);
	a.push_back(9);
	a.push_back(1);
	a.push_back(3);
	a.push_back(5);
	a.push_front(1);
	a.push_front(1);
	a.push_front(1);
	a.push_front(1);
	a.push_back(66);
	a.pop_back();
	a.pop_front();
	auto it = find(a.begin(), a.end(), 9);
	if(it != a.end())
	{
		a.erase(it);
	}
	for (auto& e : a)
	{
		cout << e << " ";
	}

}

