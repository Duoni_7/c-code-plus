#define  _CRT_SECURE_NO_WARNINGS 1
#include"string.h"

int main()
{
	//使用new需要有捕获异常动作，如果有异常，立即跳到catch报错
	try
	{
		Xlj_String::test_string();//调用命名空间
	}
	catch (const exception& e)//捕获错误信息
	{
		cout << e.what() << endl;//打印错误信息
	}
	return 0;
}