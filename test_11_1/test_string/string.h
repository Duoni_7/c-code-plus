#define  _CRT_SECURE_NO_WARNINGS 1
#pragma once
#include<iostream>
#include<assert.h>

using namespace std;

namespace Xlj_String//自定义命名空间
{
	class My_String
	{
	public:
		//构造函数
		My_String(const char* vate="\0")//给缺省值，在没有传参情况下使用缺省值，完成构造函数
			//:_vate(new char[strlen(vate)+1])//实例化空间
			////开辟一个vate大小+1的空间给成员变量，多开一个空间是为\0所预留的。因为在接下来的拷贝中，将只拷贝\0前的字符，而string对象中默认的\0也需要空间存放，所以要额外开辟一个
			//,_size(strlen(vate))//实际大小为vate字符长度
			//,_capecity(_size)//总容量也为vate大小
		{
			//函数体内初始化定义不受成员变量声明顺序影响
			_size = strlen(vate);//对以上的优化：只调用一次strlen函数
			_capecity = _size;
			_vate = new char[_size + 1];
			strcpy(_vate, vate);//目标字符串与源字符串（浅拷贝）
		}

		//析构函数
		~My_String()
		{
			delete[]_vate;//方括号new与delete要统一匹配
			_vate = nullptr;
			_size = _capecity = 0;
		}

		//拷贝构造（传统写法）
		//My_String(const My_String& str)
		//{
		//	//深拷贝：给字符串类额外开辟一块空间（大小等同于目标字符串大小+1），另外+1是留给\0的
		//	_vate = new char[str._size + 1];//地址与目标对象字符串地址不同
		//	_size = str._size;//大小等于目标大小
		//	_capecity = str._capecity;//容量等于目标容量
		//	strcpy(_vate, str._vate);//将目标字符串拷贝到源字符串中，
		//}

		//拷贝构造（现代写法）
		My_String(const My_String& str)
			:_vate(nullptr)//保证交换后不会产生内存泄露//保证交换后的新对象能够被析构释放
			,_size(0)
			,_capecity(0)
		{
			//为什么要调用构造函数后进行交换？
			//因为如果直接使用参数进行调换，会产生浅拷贝的析构错误
			My_String ap(str._vate);//使用对象str中的字符串调用构造函数
			swap(_vate, ap._vate);//与新对象中的字符串交换
			swap(_size, ap._size);//有效位交换
			swap(_capecity, ap._capecity);//容量交换
		}

		//赋值重载（此版本若遇到自己赋值自己，就会遇到问题，释放后，字符串在拷贝时，用到的是随机值）
		//My_String& operator=(const My_String& str)
		//{
		//		delete[]_vate;
		//		_vate = new char[str._capecity+1];
		//		_size = str._size;
		//		_capecity = str._capecity;
		//		strcpy(_vate, str._vate);
		//		return *this;
		//}

		//new空间会失败的两种原因：
		//1、申请的空间太大
		//2、系统没有足够的空间  

		//赋值重载（传统写法）
		//My_String& operator=(const My_String& str)
		//{
		//	if (this != &str)//如果this指针不等于源参数指针，则能确定不是自身给自身赋值，才能进入
		//	{
		//		//char* tep = new char[str._capecity + 1];//先开空间，避免new失败
		//		//strcpy(tep, str._vate);//拷贝
		//		//delete[]_vate;//销毁原空间
		//		//_vate = tep;//指针赋值

		//		delete[]_vate;//不能扩容，但要想能容纳参数字符串，就要释放掉
		//		_vate = new char[str._capecity + 1];//扩容一个 与参数字符串大小的空间给this中字符串的空间
		//		_size = str._size;//有效位更新
		//		_capecity = str._capecity;//容量更新
		//		strcpy(_vate, str._vate);//万事俱备，拷贝内容成功，且不会发生浅拷贝
		//	}
		//	return *this;//返回对象
		//}

		//赋值重载（现代写法）
		My_String& operator=(const My_String& str)
		{
			//此期间的对象都是自行开辟的，字符串地址不一样，析构不存在重复问题
			if (this != &str)//如果不是自身则进入赋值
			{
				My_String op(str._vate);//用源对象的字符串成员初始化构造一个新对象
				//此swap是命名空间中的交换函数，可交换所有类型
				swap(_vate, op._vate);//字符串交换
				swap(_size, op._size);//有效位交换
				swap(_capecity, op._capecity);//容量交换
				//string类中的swap是用于类对象之间的交换，只能用于字符串类型
				//可以理解为是全局swap的一层封装
				//交换类内的所有成员变量
			}
			return *this;
		}



		//打印
		void str_prin()const
		{
			cout << _vate<<" "<<_size<<" "<<_capecity << endl;//只有类内成员函数才能访问到私人域：成员变量
		}

		//返回字符串成员变量
		const char* c_str()const//添加const修饰this指针，更具有容纳性，修饰this指针不被修改
		{
			return _vate;//返回字符串变量，不可修改
		}

		//返回size
		size_t size()const//只读，不能对this指针进行改动，const修饰this指针成为只可读
		{
			return _size;//返回有效位
		}

		//返回pos点的字符元素(可被修改）
		char& operator[](size_t pos)//使用别名返回，输出型返回值可被修改
		{
			assert(pos < _size);//pos要小于_size
			return _vate[pos];//返回vate中第pos下标的元素
		}

		//返回pos点的字符元素(不可被修改）
		const char& operator[](size_t pos)const//使用别名返回，输出型返回值不可被修改
		{
			assert(pos < _size);
			return _vate[pos];
		}

		//迭代器的实现
		typedef char* iterator;//（还未确定是否为指针，但作用很相似）

		//字符串首元素返回
		iterator begin()
		{
			return _vate;//返回字符串头指针
		}

		//字符串尾指针返回
		iterator end()
		{
			return _vate + _size;//字符串末位的后一位地址
		}

		//尾插
		void  str_push_back(char ret)
		{
			if (_size == _capecity)//空间是否满了
			{
				str_reserve(_capecity == 0 ? 4 : _capecity * 2);//若满了则进行扩容
			}

			_vate[_size++] = ret;//插入
			_vate[_size] = '\0';//因为\0被覆盖，所以要在元素后补上
		 }
		
		//扩容
		void str_reserve(size_t num)
		{
			//reserve特性：若值大于容器值，则进行扩容；若小于则不扩容
			//resize特性：：若值大于有效位，则不处理；若值小于有效位，则进行有效位裁剪
			if (num > _capecity)
			{
				char* tep = new char[num + 1];//扩容max+1位
				strcpy(tep, _vate);//拷贝（深）

				delete[]_vate;//拷贝完成后，清理vate的空间
				_vate = tep;//指向新空间
				_capecity = num;//更新容量
			}
		}

		//+=尾插
		My_String& operator+=(const char ret)
		{
			str_push_back(ret);//复用尾插
			return *this;
		}

		//返回容量
		size_t ret_capecity()
		{
			return _capecity;
		}

		//插入字符_串
		void str_append(const char* ret)
		{
			size_t len = strlen(ret);//检查字符串长度
			if (_size + len > _capecity)//若插入字符串长度与有效位长度相加大于容量
			{
				//会经历扩容迁移，所开空间与两个数相加相同，不会浪费一个字节
				str_reserve(_size + len);//则进行扩容，扩容大小：可以是容器大小+长度，也可以有效位加长度，最省空间优先
			}
			strcpy(_vate + _size, ret);//拷贝到vate中的尾部，遇\0结束拷贝
			//strcat需要找到尾部的\0，O(N)效率太低，效率不高，慎用 
			_size += len;//有效位更新
		}

		//+=字符串（底层调用）apeend
		My_String operator+=(const char* ret)
		{
			str_append(ret);//复用append
			return *this;
		}

		//指定位置插入字符
		//My_String& str_insert(size_t num, char ret)
		//{
		//	assert(num <= _size);//指定插入位置必须要小于有效位大小
		//	if (_size == _capecity)//检查扩容
		//	{
		//		str_reserve(_capecity == 0 ? 4 : _capecity*2);
		//	}
		//	size_t end = _size + 1;//有效位后一位
		//	while (end > num)//将num到_size之间的数位全部后移一位
		//	{
		//		_vate[end] = _vate[end - 1];//后移
		//		end--;//向前移动
		//	}
		//	_vate[num] = ret;//空位插入
		//	_size++; //有效位+1
		//	return *this;//返回
		//}

		//指定位置插入字符串
		My_String& str_insert(size_t num, const char* ret)
		{
			assert(num <= _size);
			size_t len = strlen(ret);
			if (_size + len > _capecity)//检查扩容
			{
				str_reserve(_size + len);
			}
			size_t end = _size + len;
			while (end >= num+len)
			{
				_vate[end] = _vate[end - len];//后移
				end--;//向前移动
			}
			strncpy(_vate + num, ret, len);
			_size+=len;  
			return *this;//返回
		} 

		//删除指定位置
		void str_delete(size_t num, size_t len = npos)
		{
			assert(num < _size);
			if (len == npos || num + len >= _size)
			{
				_vate[num] = '\0';
				_size = num;
			}
			else
			{
				strcpy(_vate + num, _vate + num + len);
				_size -= len; 
			}
		}

		void str_clear()//清空
		{
			_vate[0] = '\0';
			_size = 0;
		}

		//查找字符并返回下标
		size_t str_find(char ret, size_t pos = 0)const
		{
			assert(pos < _size);
			for (size_t i = pos; i < _size; i++)//遍历,从pos点开始
			{
				if (ret == _vate[i])//若找到
				{
					return i;//返回下标
				}
			}
			return npos;//若找不到，返回错误码
		}

		//查找子串，返回起始地址
		size_t str_find(char* ret, size_t pos = 0)const
		{
			assert(ret);
			assert(pos < _size);
			const char* p = strstr(_vate + pos, ret);//strstr函数
			if (p == nullptr)//检查是否找到子串
			{
				return npos;//若找不到则返回错
			}
			else
			{
				return p - _vate;//指针相减，所得出的是二者相差的距离。大-小
			}
		}

		//拼组（将起始点与向后的指定长度裁剪置新字符串类型中）
		My_String str_substr(size_t pos, size_t len = npos)const
		{
			assert(pos < _size);//pos点不得超过或等于实际长度
			size_t reallen = len;//备用
			if (len == npos || pos + len > _size)//如果指定长度等于npos或起点+指定长度大于实际长度
			{
				reallen = _size - pos;//那么备用长度就等于，起点到终点的长度
				//从起点的位置，裁剪到字符串结束的位置
			}
			My_String sub;//声明新字符串
			for (size_t i = 0; i < reallen; i++)//将指定长度的字符尾插进新字符串
			{
				sub += _vate[pos + i];//从pos点开始尾插指定长度中的数据
			}
			return sub;
		}

		//比较字符串是否大于
		bool  operator>(const My_String& str)const
		{
			return strcmp(_vate, str._vate)>0;//库函数：==0相等 >0为真 <0为假 
		}

		//比较字符串小于
		bool  operator<(const My_String& str)const
		{
			return !(*this >= str);
		}

		//比较字符串大于等于
		bool  operator>=(const My_String& str)const
		{
			return *this > str || *this == str;
		}

		//比较字符串小于等于
		bool  operator<=(const My_String& str)const
		{
			return !(*this < str);
		}

		//比较字符串等于
		bool  operator==(const My_String& str)const
		{
			return strcmp(_vate, str._vate) == 0;
		}

		//比较字符串不等于
		bool  operator!=(const My_String& str)const
		{
			return !(*this == str);
		}

		//resize模拟
		void str_resize(size_t n, char ch = '\0')//数位和初始值
		{
			if (n > _size)
			{
				str_reserve(n);//大于有效位检查容量，不足则扩容
				for (size_t i = _size; i < n; i++)//在已有的基础上扩容
				{
					_vate[i] = ch;//resize的随机值是0
				}
				_vate[n] = '\0';
				_size = n;
			}
			else//小于有效位直接截取
			{
				_vate[n] = '\0';
				_size = n;
			}
		}

	private:
		//进行构造函数时，执行顺序是按照成员变量的声明进行的（在初始化列表内如此）
		char* _vate;//动态增长的字符串数组
		size_t _size;//有效位计数//无符号整形，保证不是负数
		size_t _capecity;//容量

		//static int test;//静态变量初始化必须要类外定义
		const static size_t npos = -1;//静态常变量可以类内定义//C++新特性
	};

	//流输出
	ostream& operator<<(ostream& out, const My_String& str)//类外定义，但在同一命名空间内
	{
		for (size_t i = 0; i < str.size(); i++)
		{
			out << str[i];//读取字符到缓冲区
		}
		return out;//返回缓冲区
	}

	////流提取//输入(减少扩容损耗）   
	istream& operator>>(istream& in, My_String& str)
	{
		str.str_clear();//清空
		char ch;//声明一个字符变量，用于接收输入字符
		const size_t N = 32;//定义一个常变量，定义数组大小
		char ret[N];//定义一个零时存储数组
		size_t t = 0;//数组的游标
		ch = in.get();//用is库中的get成员函数接收一个符号（可以接收空格）
		//cin流不接收空格与换行符号
		//get接收
		while (ch != '\0' && ch != '\n')//同时具备\0结束标志与\n换行标志
		{
			ret[t++] = ch;//将接收到的字符填入临时数组
			if (t == N-1)//如果有效位达到31位，则进入条件
			{
				ret[t] = '\0';//将下标31位置为\0
				str += ret;//尾插到字符串类中
				t = 0;//游标置0
			}
			ch = in.get();//提取字符
		}
		ret[t] = '\0';//遇到结束符，则直接将t位置\0
		str += ret;//整体尾插
		return in;
	}

	//int My_String::test = 1;

	void test_string()
	{
		My_String pr("hello world!");
		pr.str_prin();
		const char* vate = pr.c_str();//返回值是const修饰的字符指针，注意权限的放大问题
		for (size_t i = 0; i < pr.size(); i++)
		{
			cout << vate[i] << " ";
		}
		cout << endl;
		//迭代器的实现
		My_String::iterator it = pr.begin();//去类中的自定义名：iterator(char*)赋予头指针
		while (it != pr.end())//头指针u不等于尾指针的后一位（完成遍历）
		{
			cout << *it << " ";//每个指针都要解引用读取打印
			it++;//指针前进
		}
		//其实string类中的迭代器更像是指针，取头取尾（后一位）然后O(N)遍历
		cout << endl;
		cout << pr.size() << endl;
		cout << pr.operator[](6) << endl;
		//My_String pt;//即使是空对象，也具有一个元素：'\0'
		//pt.str_prin();
		//拷贝构造
		My_String pt(pr);
		pt = pr;//赋值：可能会造成浅拷贝后：1、析构两次 2、修改问题  //深拷贝解决这两个问题，让两个字符串地址成为唯一
		My_String tr("yes i do! ");
		tr.str_push_back('h');
		tr += '5';
		tr.str_append("chk8888888888888888888888eep");
		tr += " who?";
		//tr.str_insert(8, '1');
		tr.str_insert(9,"99");
		tr.str_delete(5);//删除
		for (size_t i = 0; i < tr.size(); i++)
		{
			cout << tr[i];
		}
		cout << endl;
		cout << tr.ret_capecity() << " " << tr.size()  << endl;

		My_String op("hello");
		My_String ot;
		/*op += '\0';
		op += "world!";*/
		cin >> op>>ot;
		cout << op << "-------"<< ot << endl;//流输出
		//cout << op.c_str() << endl;//C语言中字符串类型是依靠\0来判断停止的
		//而C++中字符串类型是依靠类内的size有效位决定是否停止的


	}
}