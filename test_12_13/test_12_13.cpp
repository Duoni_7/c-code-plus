#define  _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
using namespace std;

//class b;
//class a
//{
//public:
//	friend void print(const a& p, const b& q);
////protected:
//	string _name = "perte";
//	static int ok;
//};
//int a::ok = 99;
//
//class b :public a
//{
//	friend void print(const a& p, const b& q);
//protected:
//	int _num = 99;
//};
//
//void print(const a& p, const b& q)
//{
//	/*cout << p._name << endl;
//	cout << q._num << endl;*/
//	cout << &p.ok << endl;
//	cout << &q.a::ok << endl;
//	cout << &p._name << endl;
//	cout << &q.a::_name << endl;
//}
//
//int main()
//{
//	a u;
//	b l;
//	print(u, l);
//	//cout << u.ok << endl;
//	return 0;
//}

//oj
//class Solution {
//public:
//    void rotate(vector<int>& nums, int k)
//    {
//        if (k < nums.size())//若k小于数组长度
//        {
//            vector<int>cur;
//            for (size_t i = nums.size() - k % nums.size(); i < nums.size(); i++)
//            {
//                cur.push_back(nums[i]);//抓取后k位
//            }
//            for (size_t i = 0; i < nums.size() - k; i++)
//            {
//                cur.push_back(nums[i]);//再尾插前nums.size()-k位
//            }
//            nums = cur;//赋值给原数组
//        }
//        else//若k>=nums.size()
//        {
//            vector<int>cur;
//            for (size_t i = nums.size() - k % nums.size(); i < nums.size(); i++)
//            {
//                cur.push_back(nums[i]);//抓取后nums.size()-k%nums.size()到nums.size()-1位
//            }
//            for (size_t i = 0; i < nums.size() - k % nums.size(); i++)
//            {
//                cur.push_back(nums[i]);//尾插0到nums.size()-k%nums.size()-1位
//            }
//            nums = cur;
//        }
//    }
//};

////菱形继承
//class Person
//{
//public:
//	string _name; // 姓名
//};
//
//class Student : virtual public Person
//{
//protected:
//	int _num; //学号
//};
//
//class Teacher : virtual public Person
//{
//protected:
//	int _id; // 职工编号
//};
//
//class Assistant : public Student, public Teacher
//{
//protected:
//	string _majorCourse; // 主修课程
//};
//
//int main()
//{
//	Assistant a;
//	a._name = "赵五";
//	a.Student::_name = "张三";
//	a.Teacher::_name = "肖老师";
//	return 0;
//}

class A
{
public:
	int _a;
};

// class B : public A
class B : public A
{
public:
	int _b;
};

// class C : public A
class C : public A
{
public:
	int _c;
};

class D : public B, public C
{  
public:
	int _d;
};

