#define  _CRT_SECURE_NO_WARNINGS 1
#include"list.h"


int main()
{
	test_list_3();
	return 0;
}



//STL_list容器使用
//int main()
//{
//	list<int>ls;
//	ls.push_back(5);
//	ls.push_back(5);
//	ls.push_back(5);
//	ls.push_back(5);
//	ls.push_back(5);
//
//	ls.push_back(1);
//	ls.push_back(1);
//	ls.push_back(1);
//	ls.push_back(4);
//	ls.push_back(5);
//	ls.push_back(6);
//	ls.push_back(7);
//	ls.sort();
////	ls.remove(1);
////	/*ls.push_front(999);
////	ls.pop_back();
////	ls.insert(ls.begin(), 66);
////	auto o = find(ls.begin(), ls.end(), 66);
////	ls.erase(o);
////	ls.erase(o);*/
////	//ls.insert(o, 88);
//	list<int>::iterator it = ls.begin();//虽然内部不有序，但迭代器会使之有序化
//	while (it != ls.end())
//	{
//		(*it)++;
//		cout << *it << " ";
//		it++;
//	}
//	/*for (auto e : ls)
//	{
//		cout << e << " ";
//	}*/
//	
//	return 0;
//}

//去重算法：必须数据有序
//int main()
//{
//	vector<int>a({ 1,2,2,3,4,4,6,7,7,7,9,9,9,9,9,9,10,666,999,44,666 });
//	int slow = 0, fast = 0;
//	while (fast < a.size())
//	{
//		if (a[slow] == a[fast])
//		{
//			fast++;
//		}
//		else
//		{
//			a[++slow] = a[fast++];
//		}
//	}
//	a.resize(slow + 1);
//	for (auto it : a)
//	{
//		cout << it << " ";
//	}
//	return 0;
//}