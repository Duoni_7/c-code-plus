#define  _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
using namespace std;

class Date
{
public:
	//构造函数
	Date(int year = 1900, int month = 1, int day = 1)
	{
		_year = year;
		_month = month;
		_day = day;
	}

	// 拷贝构造函数
	Date(const Date& d)
	{
		_year = d._year;
		_month = d._month;
		_day = d._day;
	}

	// 析构函数
	~Date()
	{
		_year = 0;
		_month = 0;
		_day = 0;
	}

	// ==运算符重载
	bool operator==(const Date& d)
	{
		return _year == d._year
			&& _month == d._month
			&& _day == d._day;
	}

	// >运算符重载
	bool operator>(const Date& d)
	{
		return !(*this < d) && !(*this == d);
	}

	// >=运算符重载
	bool operator >= (const Date& d)
	{
		return (*this == d) || (*this > d);
	}

	// <运算符重载false
	bool operator < (const Date& d)
	{
		if (_year < d._year || _year == d._year)//若年<=d年，则进入判断
		{
			return _year < d._year//如果年小于d年
				|| _year == d._year && _month < d._month//年等于d年，月小于d月
				|| _year == d._year && _month == d._month && _day < d._day;//若年等于d年，月等于d月，天小于d天
		}
		else//若年大于d年，返回假
		{
			return false;
		}
	}

	// <=运算符重载
	bool operator <= (const Date& d)
	{
		return (*this == d) || (*this < d);
	}

	// !=运算符重载
	bool operator != (const Date& d)
	{
		return !(*this == d);
	}

	// 获取某年某月的天数
	int GetMonthDay(int year, int month)
	{
		static int yearNum[] = { 0,31,28,31,30,31,30,31,31,30,31,30,31 };
		int flag = 0;
		if (year % 4 == 0 && year % 100 != 0 || year % 400 == 0)
		{
			flag = 1;
		}
		if (month == 2)
		{
			return yearNum[month] + flag;
		}
		return yearNum[month];
	}

	// 赋值运算符重载
	Date& operator=(const Date& d)
	{
		if (this != &d)//用地址判断，防止自己给自己赋值
		{
			_year = d._year;
			_month = d._month;
			_day = d._day;
		}
		return *this;
	}

	//日期+=天数
	Date& operator+=(int day)
	{
		/**this = *this + day;//附用
		return *this;*/

		_day += day;//天数+=
		int res = GetMonthDay(_year, _month);//当月天数
		if (_day > res)//若+=后，大于当月天数，则需要进位，-天数
		{
			while (_day > res)//当日期天数小于等于当月份天数时停止
			{
				_day -= res;//*当月天数
				_month += 1;//月份+1
				if (_month > 12)//若月份大于12，则年+1
				{
					_year += 1;
					_month = 1;
				}
				res = GetMonthDay(_year, _month);//继续查找下一月份天数
			}
		}
		return *this;
	}



	//日期+天数
	Date operator+(int day)
	{
		Date cp(*this);//拷贝日期对象
		//return cp += day;//附用

		cp._day += day;//天数+=
		int res = GetMonthDay(cp._year, cp._month);//当月天数
		if(cp._day > res)//若+=后，大于当月天数，则需要进位，-天数
		{
			while (cp._day > res)//当日期天数小于等于当月份天数时停止
			{
				cp._day -= res;//*当月天数
				cp._month += 1;//月份+1
				if (cp._month > 12)//若月份大于12，则年+1
				{
					cp._year += 1;
					cp._month = 1;
				}
				res = GetMonthDay(cp._year, cp._month);//继续查找下一月份天数
			}
		}
		return cp;
	}

	//日期-天数
	Date operator-(int day)
	{
		Date cp(*this);//返回拷贝日期对象

		/*cp -= day;//附用
		return cp;*/

		cp._day -= day;//-指定天数
		if (cp._day <= 0)//若指定天数大于该日期天数，那么此时日期天数为负数，要补到>0
		{
			while (cp._day <= 0)//日期天数大于0时停止
			{
				cp._month -= 1;//月份--
				if (cp._month == 0)//若月份为0
				{
					cp._year -= 1;//年--
					cp._month = 12;//月份为12
				}
				int res = GetMonthDay(cp._year, cp._month);//取上一月份的天数补齐
				cp._day += res;//+=
			}
		}
		return cp;//返回
	}

	// 日期-=天数
	Date& operator-=(int day)
	{
		_day -= day;//-指定天数
		if (_day <= 0)//若指定天数大于该日期天数，那么此时日期天数为负数，要补到>0
		{
			while (_day <= 0)//日期天数大于0时停止
			{
				_month -= 1;//月份--
				if (_month == 0)//若月份为0
				{
					_year -= 1;//年--
					_month = 12;//月份为12
				}
				int res = GetMonthDay(_year, _month);//取上一月份的天数补齐
				_day += res;//+=
			}
		}
		return *this;//返回
	}

	//计算年的总天数  
	int get_YearNum(int year)
	{
		static int year_[] = { 365,366 };
		int sum = 0;
		while (year)
		{
			year--;
			if (year % 4 == 0 && year % 100 != 0 || year % 400 == 0)
			{
				sum += year_[1];
			}
			else
			{
				sum += year_[0];
			}
		}
		return sum;
	}

	//日期-日期 返回天数
	int operator-(const Date& d)
	{
		int t_sum = get_YearNum(_year);
		int d_sum = get_YearNum(d._year);
		int t_month = _month, d_month = d._month;
		while (t_month || d_month)
		{
			if (t_month != 0)
			{
				t_month--;
				t_sum += GetMonthDay(_year, t_month);
			}
			if (d_month != 0)
			{
				d_month--;
				d_sum += GetMonthDay(d._year, d_month);
			}
		}
		t_sum += _day;
		d_sum += d._day;
		return t_sum - d_sum;
	}

	// 前置++
	Date& operator++()
	{
		//return *this += 1;附用

		int numDay = GetMonthDay(_year, _month);//当月天数
		if (_day == numDay)//如果此时天数等于当月天数
		{
			_month += 1;//月份++
			_day = 1;//天数等于1
			if (_month > 12)//如果此时月份大于12
			{
				_year += 1;//年份加1
				_month = 1;//月份等于1
			}
			return *this;//返回修改前的对象
		}
		_day += 1;//若天数不等于当月天数，直接++
		return *this;
	}

	//后置++
	Date operator++(int)
	{
		//Date cp(*this);
		// *this + 1;
		//return cp;附用

		Date cp(*this);//拷贝++前的对象
		int numDay = GetMonthDay(_year, _month);//当月天数
		if (_day == numDay)//如果此时天数等于当月天数
		{
			_month += 1;//月份++
			_day = 1;//天数等于1
			if (_month > 12)//如果此时月份大于12
			{
				_year += 1;//年份加1
				_month = 1;//月份等于1
			}
			return cp;//返回修改前的对象
		}
		_day += 1;//若天数不等于当月天数，直接++
		return cp;//返回改变前的对象
	}

	//后置--
	Date operator--(int)
	{
		Date cp(*this);

		/*this -= 1;附用
		return cp;*/

		if (_day == 1)//如果当月只剩一天
		{
			_day -= 1;//天--
			_month -= 1;//月
			if (_month < 1)//若月份小于1
			{
				_year -= 1;//年--
				_month = 12;//月等于12
			}
			_day = GetMonthDay(_year, _month);//获取天数
			return cp;//返回
		}
		_day -= 1;//否则正常--
		return cp;
	}

	// 前置--
	Date& operator--()
	{
		//return *this -= 1;附用

		if (_day == 1)//如果当月只剩一天
		{
			_day-=1;//天--
			_month-=1;//月
			if (_month < 1)//若月份小于1
			{
				_year-=1;//年--
				_month = 12;//月等于12
			}
			_day = GetMonthDay(_year, _month);//获取天数
			return *this;//返回
		}
		_day-=1;//否则正常--
		return *this;
	}

	//打印
	void printf_Date()
	{
		cout << _year << " " << _month << " " << _day << endl;
	}

private:
	int _year;
	int _month;
	int _day;
};

int main()
{
	Date vate(2000,2,15);
	Date pere(2075,7,25);
	cout << (vate == pere) << endl;//等于重载
	cout << (vate < pere) << endl;//小于重载
	cout << (vate <= pere) << endl;//小于等于重载
	cout << (vate != pere) << endl;//不等于重载
	cout << (vate > pere) << endl;//大于重载
	cout << (vate >= pere) << endl;//大于等于重载
	--vate;//前置--
	Date cpVate = vate--;//后置--，返回未--前的日期
	cpVate.printf_Date();//未--前日期
	vate.printf_Date();//--后的日期
	//Date cpVate = vate++;//后置++
	//cpVate.printf_Date();//未++前的日期
	//vate.printf_Date();//++后的日期
	//++vate;
	//vate.printf_Date();//++后的日期
	//pere = vate;//赋值运算符重载
	//pere.printf_Date();
	//Date cpVate = vate - 62;//减号重载
	//cpVate.printf_Date();
	//vate.printf_Date();
	//vate -= 30;//-=重载
	//vate.printf_Date();
	//Date cpVate = vate + 40;//+重载
	//cpVate.printf_Date();
	//vate += 55;//+=重载6
	//vate.printf_Date();
	//(vate + 4).printf_Date();
	//(vate + 40).printf_Date();
	//(vate + 400).printf_Date();
	//(vate + 40000).printf_Date();
	//(vate - 4).printf_Date();//日期-天数
	//(vate - 40).printf_Date();
	//(vate - 400).printf_Date();
	//(vate - 4000).printf_Date();
	//(vate - 40000).printf_Date();
	cout << vate.get_YearNum(3) << endl;//测试求年天数和
	cout << (vate - pere) << endl;//日期-日期
	cout << (pere - vate) << endl;//日期-日期
	return 0; 
}